# ipeer-web
Web Application for iPeer project. This application serve as
 - Admin module
 - Web Service
 
## Installation
 - Download source code using `git clone https://bitbucket.org/kowsercse/ipeer.git`
 - Go to project directory `cd ipeer/ipeer-web`
 - Copy `.ipeer` to the `~/` directory
 - Edit deployment configuration file in the `~/.ipeer/config` directory  
    - `development.yml` for development
    - `production.yml` for production
    - `test.yml` for test
 - Cd to `ipeer/ipeer-web/src/main/webapp`
 - Use bower to install JavaScript libraries: `bower install`
 - Import database tables using the `~/.ipeer/db/tables.sql` script

## Run Application (update)

User Gradle deploy, it is more stable.

- cd to `~/downloads/ipeer/ipeer-web`

- execute `./gradlew clean build` first

- `./gradlew -Dgrails.env=prod bootRun` to run as production environment


## Run Application
Since its a `grails 3`, there are different option to deploy this application using `gradle`, `grails` and `tomcat` is 
possible. All of these options are listed below.

- JAR Deployment
    - Execute `nohup java -jar ipeer-web-0.1.war &` to deploy as a standalone Java Application
- Grails Deployment
    - Execute `grails prod run-app` for production environment
    - For more on [deployment using Grails 3](http://docs.grails.org/3.0.17/guide/deployment.html)
- Gradle Deployment
    - `./gradlew -Dgrails.env=prod bootRun` to run as production environment
- Tomcat Deployment
    - Create war file using `./gradlew clean build` and the war will be created in the `build/libs` directory
    - Copy the war file and deploy it using standard tomcat deployment
