#!/usr/bin/env bash

shutdown.sh
shutdown.sh

./gradlew clean build assemble

unzip build/libs/ipeer-web-0.1.war -d build/ipeer-web

startup.sh
