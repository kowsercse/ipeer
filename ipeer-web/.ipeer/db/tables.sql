SET FOREIGN_KEY_CHECKS  = FALSE;

--
-- Table structure for table `acl_class`
--
DROP TABLE IF EXISTS `acl_class`;
CREATE TABLE `acl_class` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `class` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_iy7ua5fso3il3u3ymoc4uf35w` (`class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `acl_entry`
--
DROP TABLE IF EXISTS `acl_entry`;
CREATE TABLE `acl_entry` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ace_order` int(11) NOT NULL,
  `acl_object_identity` bigint(20) NOT NULL,
  `audit_failure` bit(1) NOT NULL,
  `audit_success` bit(1) NOT NULL,
  `granting` bit(1) NOT NULL,
  `mask` int(11) NOT NULL,
  `sid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_ace_order` (`acl_object_identity`,`ace_order`),
  KEY `FK_i6xyfccd4y3wlwhgwpo4a9rm1` (`sid`),
  CONSTRAINT `FK_fhuoesmjef3mrv0gpja4shvcr` FOREIGN KEY (`acl_object_identity`) REFERENCES `acl_object_identity` (`id`),
  CONSTRAINT `FK_i6xyfccd4y3wlwhgwpo4a9rm1` FOREIGN KEY (`sid`) REFERENCES `acl_sid` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `acl_object_identity`
--
DROP TABLE IF EXISTS `acl_object_identity`;
CREATE TABLE `acl_object_identity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `object_id_class` bigint(20) NOT NULL,
  `entries_inheriting` bit(1) NOT NULL,
  `object_id_identity` bigint(20) NOT NULL,
  `owner_sid` bigint(20) DEFAULT NULL,
  `parent_object` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_object_id_identity` (`object_id_class`,`object_id_identity`),
  KEY `FK_nxv5we2ion9fwedbkge7syoc3` (`owner_sid`),
  KEY `FK_6oap2k8q5bl33yq3yffrwedhf` (`parent_object`),
  CONSTRAINT `FK_6c3ugmk053uy27bk2sred31lf` FOREIGN KEY (`object_id_class`) REFERENCES `acl_class` (`id`),
  CONSTRAINT `FK_6oap2k8q5bl33yq3yffrwedhf` FOREIGN KEY (`parent_object`) REFERENCES `acl_object_identity` (`id`),
  CONSTRAINT `FK_nxv5we2ion9fwedbkge7syoc3` FOREIGN KEY (`owner_sid`) REFERENCES `acl_sid` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `acl_sid`
--
DROP TABLE IF EXISTS `acl_sid`;
CREATE TABLE `acl_sid` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `principal` bit(1) NOT NULL,
  `sid` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_principal` (`sid`,`principal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `choice`
--
DROP TABLE IF EXISTS `choice`;
CREATE TABLE `choice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `issue`
--
DROP TABLE IF EXISTS `issue`;
CREATE TABLE `issue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `auto_reported` bit(1) NOT NULL,
  `created_by_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL,
  `detail` varchar(255) NOT NULL,
  `error_log` varchar(255) DEFAULT NULL,
  `last_updated` datetime NOT NULL,
  `summary` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `K_issue_created_by_id` (`created_by_id`),
  CONSTRAINT `FK_issue_created_by` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `mentor`
--
DROP TABLE IF EXISTS `mentor`;
CREATE TABLE `mentor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_mentor_user_id` (`user_id`),
  CONSTRAINT `FK_mentor_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `question`
--
DROP TABLE IF EXISTS `question`;
CREATE TABLE `question` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `question_choices`
--

DROP TABLE IF EXISTS `question_choices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_choices` (
  `question_id` bigint(20) NOT NULL,
  `choice_id` bigint(20) NOT NULL,
  PRIMARY KEY (`question_id`,`choice_id`),
  KEY `K_question_choices_id` (`choice_id`),
  CONSTRAINT `FK_question_choices_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`),
  CONSTRAINT `FK_question_choices_choice` FOREIGN KEY (`choice_id`) REFERENCES `choice` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `authority` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_role_auhtority` (`authority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `schedule`
--
DROP TABLE IF EXISTS `schedule`;
CREATE TABLE `schedule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `end` datetime NOT NULL,
  `start` datetime NOT NULL,
  `survey_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_schedule_survey_id` (`survey_id`),
  CONSTRAINT `FK_schedule_survey` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `submission_summary`
--
DROP TABLE IF EXISTS `submission_summary`;
CREATE TABLE `submission_summary` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `progress_direction` int(11) NOT NULL,
  `schedule_id` bigint(20) NOT NULL,
  `score` int(11) NOT NULL,
  `submission_time` datetime NOT NULL,
  `submitted_by_id` bigint(20) DEFAULT NULL,
  `veteran_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_schedule_id` (`veteran_id`,`schedule_id`),
  KEY `K_submission_summary_schedule_id` (`schedule_id`),
  KEY `K_submission_summary_submitted_by_id` (`submitted_by_id`),
  CONSTRAINT `FK_submission_summary_schedule` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`id`),
  CONSTRAINT `FK_submission_summary_mentor` FOREIGN KEY (`submitted_by_id`) REFERENCES `mentor` (`id`),
  CONSTRAINT `FK_submission_summary_veteran` FOREIGN KEY (`veteran_id`) REFERENCES `veteran` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `submitted_answer`
--
DROP TABLE IF EXISTS `submitted_answer`;
CREATE TABLE `submitted_answer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `choice_id` bigint(20) NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `submission_summary_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_submitted_answer` (`question_id`,`submission_summary_id`),
  KEY `K_submitted_answer_choice_id` (`choice_id`),
  KEY `K_submitted_answer_submisssion_summary_id` (`submission_summary_id`),
  CONSTRAINT `FK_submitted_answer_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`),
  CONSTRAINT `FK_submitted_answer_submission_summary` FOREIGN KEY (`submission_summary_id`) REFERENCES `submission_summary` (`id`),
  CONSTRAINT `FK_submitted_answer_choice` FOREIGN KEY (`choice_id`) REFERENCES `choice` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `survey`
--
DROP TABLE IF EXISTS `survey`;
CREATE TABLE `survey` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `survey_questions`
--
DROP TABLE IF EXISTS `survey_questions`;
CREATE TABLE `survey_questions` (
  `question_id` bigint(20) NOT NULL,
  `survey_id` bigint(20) NOT NULL,
  PRIMARY KEY (`survey_id`,`question_id`),
  KEY `K_survey_questions` (`question_id`),
  CONSTRAINT `FK_survey_questions_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`),
  CONSTRAINT `FK_survey_questions_survey` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `user`
--
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_expired` bit(1) NOT NULL,
  `account_locked` bit(1) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_expired` bit(1) NOT NULL,
  `reset_token` varchar(255) DEFAULT NULL,
  `token_generation_time` datetime DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_user_username` (`username`),
  UNIQUE KEY `UK_user_reset_token` (`reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `user_role`
--
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `role_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `K_user_role` (`user_id`),
  CONSTRAINT `FK_user_role_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_user_role_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `version`
--
DROP TABLE IF EXISTS `version`;
CREATE TABLE `version` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `build_number` varchar(128) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `major` varchar(128) NOT NULL,
  `minor` varchar(128) NOT NULL,
  `release_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_version_build_number` (`build_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `veteran`
--
DROP TABLE IF EXISTS `veteran`;
CREATE TABLE `veteran` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mentor_id` bigint(20) DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `red_cap_user_id` VARCHAR(10),
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_veteran_user_id` (`user_id`),
  UNIQUE KEY `UK_veteran_red_cap_user_id` (`red_cap_user_id`),
  KEY `K_veteran_mentor_id` (`mentor_id`),
  CONSTRAINT `FK_veteran_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_veteran_mentor` FOREIGN KEY (`mentor_id`) REFERENCES `mentor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `authentication_token`
--
DROP TABLE IF EXISTS `authentication_token`;
CREATE TABLE `authentication_token` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `token_value` VARCHAR(255) NOT NULL,
  `username` VARCHAR(255) NOT NULL,
  PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `authentication_token`
--
DROP TABLE IF EXISTS `authentication_token`;
CREATE TABLE `password_refresh_token` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `date_created` DATETIME NOT NULL,
  `token` VARCHAR(255) NOT NULL,
  `user_id` BIGINT(20) NOT NULL,
  PRIMARY KEY(`id`),
  UNIQUE KEY `UK_refresh_token_token` (`token`),
  CONSTRAINT `FK_refresh_token_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `password_reset_token`
--
DROP TABLE IF EXISTS `password_reset_token`;
CREATE TABLE `password_reset_token` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `date_created` DATETIME NOT NULL,
  `token` VARCHAR(255) NOT NULL,
  `user_id` BIGINT(20) NOT NULL,
  PRIMARY KEY(`id`),
  UNIQUE KEY `UK_reset_token_token` (`token`),
  CONSTRAINT `FK_reset_token_user_id` FOREIGN KEY (`user_id`) REFERENCES user (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

SET FOREIGN_KEY_CHECKS  = TRUE;
