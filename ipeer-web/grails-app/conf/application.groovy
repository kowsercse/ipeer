

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'ipeer.entity.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'ipeer.entity.UserRole'
grails.plugin.springsecurity.authority.className = 'ipeer.entity.Role'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',               access: ['permitAll']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/static/**',      access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/css/**',      access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/**/favicon.ico', access: ['permitAll']]
]

grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/css/**',      filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/**',             filters: 'JOINED_FILTERS']
]

grails.plugin.springsecurity.roleHierarchy = '''
   ROLE_SUPER_ADMIN > ROLE_MENTOR
   ROLE_SUPER_ADMIN > ROLE_VETERAN
'''

grails.plugin.springsecurity.useBasicAuth = true
grails.plugin.springsecurity.basic.realmName = "Webservice"
grails.plugin.springsecurity.rest.login.active=true
grails.plugin.springsecurity.rest.login.failureStatusCode=401
grails.plugin.springsecurity.rest.login.useJsonCredentials=true
grails.plugin.springsecurity.rest.login.usernamePropertyName='username'
grails.plugin.springsecurity.rest.login.passwordPropertyName='password'
//grails.plugin.springsecurity.rest.login.endpointUrl='/webservice/login'
//grails.plugin.springsecurity.rest.logout.endpointUrl='/webservice/logout'
grails.plugin.springsecurity.rest.token.validation.headerName = "Authorization"
grails.plugin.springsecurity.rest.token.storage.useGorm = true
grails.plugin.springsecurity.rest.token.storage.gorm.tokenDomainClassName="ipeer.entity.AuthenticationToken"
grails.plugin.springsecurity.rest.token.generation.useSecureRandom = true
grails.plugin.springsecurity.securityConfigType = "Annotation"

grails.plugin.springsecurity.filterChain.chainMap = [
    [
        pattern: '/webservice/**',
        filters: 'JOINED_FILTERS,-exceptionTranslationFilter,-authenticationProcessingFilter,-securityContextPersistenceFilter,-rememberMeAuthenticationFilter'
    ],
    [
        pattern: '/**',
        filters: 'JOINED_FILTERS,-restTokenValidationFilter,-restExceptionTranslationFilter,-basicAuthenticationFilter,-basicExceptionTranslationFilter'
    ]
]

grails.gorm.default.mapping = {
	version false
	autoTimestamp false
}

quartz {
  autoStartup = true
  jdbcStore = false
  waitForJobsToCompleteOnShutdown = true
  exposeSchedulerInRepository = false

  props {
    scheduler.skipUpdateCheck = true
  }
}
