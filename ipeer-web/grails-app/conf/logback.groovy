import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.rolling.RollingFileAppender
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy
import grails.util.Environment

import static ch.qos.logback.classic.Level.DEBUG
import static ch.qos.logback.classic.Level.WARN

// See http://logback.qos.ch/manual/groovy.html for details on configuration
appender('STDOUT', ConsoleAppender) {
  encoder(PatternLayoutEncoder) {
    pattern = "%level %logger - %msg%n"
  }
}

appender("FILE", RollingFileAppender) {
  def userHome = System.properties.getProperty('user.home');
  file = "${userHome}/.ipeer/log/${Environment.current.name}.log"
  rollingPolicy(TimeBasedRollingPolicy) {
    fileNamePattern = "%d{yyyy-MM-dd}.log"
    maxHistory = 30
//    totalSizeCap = "1GB"
  }
  encoder(PatternLayoutEncoder) {
    pattern = "%-4relative [%thread] %-5level %logger{35} - %msg%n"
  }
}

root(WARN, ['STDOUT', 'FILE'])
if (Environment.isDevelopmentMode()) {
//  root(DEBUG, ['STDOUT', 'FILE'])
}
