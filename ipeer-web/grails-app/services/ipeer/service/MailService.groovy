package ipeer.service

import com.sun.mail.smtp.SMTPTransport
import groovy.util.logging.Log

import javax.annotation.PostConstruct
import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.Session
import javax.mail.internet.AddressException
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

class MailService {

  private Session session;
  private InternetAddress fromAddress;

  Log log
  def grailsApplication

  @PostConstruct
  void init() throws AddressException {
    def mailConfig = grailsApplication.config.mail
    Properties properties = new Properties();
    properties.put("mail.smtp.host", mailConfig.host);
    properties.put("mail.smtp.user", mailConfig.username);
    properties.put("mail.smtp.password", mailConfig.password);
    properties.put("mail.smtp.port", mailConfig.port);
    session = Session.getInstance(properties, null);
    fromAddress = new InternetAddress(mailConfig.from);
  }

  /**
   * Send email using GMail SMTP server.
   *
   * @param recipientEmail TO recipient
   * @param title title of the message
   * @param message message to be sent
   * @throws javax.mail.internet.AddressException if the email address parse failed
   * @throws javax.mail.MessagingException if the connection is dead or not in the connected state or if the message is not a MimeMessage
   */
  void send(String recipientEmail, String title, String message) throws MessagingException {
//    log.info "Sending email to: ${recipientEmail} with Subject: ${title}"

    final MimeMessage msg = new MimeMessage(session);
    msg.setFrom(fromAddress);
    msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail, false));

    msg.setSubject(title);
    msg.setText(message, "utf-8");
    msg.setSentDate(new Date());

    SMTPTransport smtpTransport = (SMTPTransport) session.getTransport("smtp");

    def mailConfig = grailsApplication.config.mail
    smtpTransport.connect(mailConfig.host, mailConfig.username, mailConfig.password);
    smtpTransport.sendMessage(msg, msg.getAllRecipients());
    smtpTransport.close();
  }

}
