package ipeer.service

import ipeer.entity.Survey

class SurveyService {

  Survey getLatestSurvey() {
    def surveys = Survey.where {}.list(sort: "id", max: 1)
    surveys ? surveys[0] : null
  }

}
