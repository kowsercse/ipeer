package ipeer.service

import grails.plugin.springsecurity.rest.token.generation.TokenGenerator
import grails.plugin.springsecurity.rest.token.storage.TokenStorageService
import grails.transaction.Transactional
import ipeer.entity.AuthenticationToken
import ipeer.entity.PasswordRefreshToken
import ipeer.entity.PasswordResetToken
import ipeer.entity.User
import ipeer.utils.DateUtils

import java.time.LocalDateTime

/**
 * @author kowsercse@gmail.com
 */
class AuthenticationTokenService {

  TokenGenerator tokenGenerator
  TokenStorageService tokenStorageService

  User findUserByPasswordResetToken(final String token) {
    def expirationTime = LocalDateTime.now().minusDays(1)
    def resetToken = PasswordResetToken.findByToken(token)
    if (resetToken == null || DateUtils.toLocalDateTime(resetToken.dateCreated).isBefore(expirationTime)) {
      return null;
    }

    resetToken.user
  }

  User findUserByPasswordRefreshToken(final String token) {
    def expirationTime = LocalDateTime.now().minusDays(1)
    def refreshToken = PasswordRefreshToken.findByToken(token)
    if(refreshToken == null || DateUtils.toLocalDateTime(refreshToken.dateCreated).isBefore(expirationTime)) {
      return null;
    }

    refreshToken.user
  }

  @Transactional
  def getPasswordResetToken(User user) {
    def expirationTime = LocalDateTime.now().minusDays(1)
    def resetToken = user.passwordResetToken

    if(resetToken == null || DateUtils.toLocalDateTime(resetToken.dateCreated).isBefore(expirationTime)) {
      resetToken = new PasswordResetToken()
      resetToken.user = user;
      resetToken.token = UUID.randomUUID().toString()

      resetToken.save flush: true
    }

    resetToken.token
  }

  AuthenticationToken retrieveAuthenticationToken(User user) {
    def authenticationToken = AuthenticationToken.findByUsername(user.username)
    if (authenticationToken == null) {
      def userDetails = new org.springframework.security.core.userdetails.User(user.username, "", Collections.emptyList())
      def accessToken = tokenGenerator.generateAccessToken(userDetails);
      tokenStorageService.storeToken(accessToken.accessToken, userDetails)

      authenticationToken = AuthenticationToken.findByUsername(user.username)
    }

    return authenticationToken
  }

  PasswordRefreshToken retrieveRefreshToken(User user) {
    def refreshToken = PasswordRefreshToken.findByUser(user)
    if (refreshToken != null) {
      refreshToken.delete()
    }

    refreshToken = new PasswordRefreshToken(user: user, token: UUID.randomUUID().toString())
    refreshToken.save flush: true
    refreshToken
  }

}
