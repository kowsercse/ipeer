<%@ page import="ipeer.entity.User" %>




<div class="mdl-grid ${hasErrors(bean: user, field: 'username', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="username">
    <g:message code="user.username.label" default="Username"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:textField name="username" required="" value="${user?.username}"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: user, field: 'veteran', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="veteran">
    <g:message code="user.veteran.label" default="Veteran"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:select id="veteran" name="veteran.id" from="${ipeer.entity.Veteran.list()}" optionKey="id" value="${user?.veteran?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: user, field: 'mentor', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="mentor">
    <g:message code="user.mentor.label" default="Mentor"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:select id="mentor" name="mentor.id" from="${ipeer.entity.Mentor.list()}" optionKey="id" value="${user?.mentor?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: user, field: 'accountExpired', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="accountExpired">
    <g:message code="user.accountExpired.label" default="Account Expired"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:checkBox name="accountExpired" value="${user?.accountExpired}" />
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: user, field: 'accountLocked', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="accountLocked">
    <g:message code="user.accountLocked.label" default="Account Locked"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:checkBox name="accountLocked" value="${user?.accountLocked}" />
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: user, field: 'enabled', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="enabled">
    <g:message code="user.enabled.label" default="Enabled"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:checkBox name="enabled" value="${user?.enabled}" />
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: user, field: 'passwordExpired', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="passwordExpired">
    <g:message code="user.passwordExpired.label" default="Password Expired"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:checkBox name="passwordExpired" value="${user?.passwordExpired}" />
</div>
</div>

