<html>
<head>
  <meta name='layout' content='dashboard'/>
  <title><g:message code="springSecurity.login.title"/></title>
</head>

<body>

<form action='${postUrl}' method='POST' autocomplete='off'
      class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--8-col mdl-cell--4-col-phone mdl-cell--4-col-tablet mdl-cell--8-col-desktop">

  <div class="mdl-card__title mdl-shadow--2dp">
    <g:message code="springSecurity.login.header"/>
  </div>

  <div class="mdl-card__supporting-text">
    <g:if test='${flash.message}'>
      <div class='login_message'>${flash.message}</div>
    </g:if>

    <div class="mdl-textfield mdl-js-textfield">
      <input class="mdl-textfield__input" type="text" name="username" id="username"/>
      <label class="mdl-textfield__label" for="username"><g:message code="springSecurity.login.username.label"/></label>
    </div>

    <div class="mdl-textfield mdl-js-textfield">
      <input class="mdl-textfield__input" type="password" name="password" id="password"/>
      <label class="mdl-textfield__label" for="password"><g:message code="springSecurity.login.password.label"/></label>
    </div>

    <div class="mdl-textfield mdl-js-textfield">
      <label for="remember_me" class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect">
        <input type='checkbox' class='mdl-checkbox__input' name='${rememberMeParameter}' id='remember_me'
               <g:if test='${hasCookie}'>checked='checked'</g:if>/>
        <span class="mdl-checkbox__label"><g:message code="springSecurity.login.remember.me.label"/></span>
      </label>
    </div>
  </div>

  <div class="mdl-card__actions mdl-card--border"><input type='submit'
                                                         class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--green-700 mdl-color-text--white"
                                                         id="submit"
                                                         value='${message(code: "springSecurity.login.button")}'/>
  </div>
</form>

<div style="clear: both"></div>
<form action='${createLink(namespace: "front", controller: "profile", action: "sendResetToken", absolute: true)}' method='POST' autocomplete='off'
      class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--8-col mdl-cell--4-col-phone mdl-cell--4-col-tablet mdl-cell--8-col-desktop">

  <div class="mdl-card__title mdl-shadow--2dp">
    Forgot Password
  </div>

  <div class="mdl-card__supporting-text">
    <div class="mdl-textfield mdl-js-textfield">
      <input class="mdl-textfield__input" type="text" name="username" id="forgotUsername"/>
      <label class="mdl-textfield__label" for="forgotUsername"><g:message code="springSecurity.login.username.label"/></label>
    </div>
  </div>

  <div class="mdl-card__actions mdl-card--border">
    <input id="forgotPassword" type='submit' value='Forgot Password'
           class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--green-700 mdl-color-text--white"/>
  </div>

</form>

<script type='text/javascript'>
  <!--
  (function () {
    document.forms['loginForm'].elements['j_username'].focus();
  })();
  // -->
</script>
</body>
</html>
