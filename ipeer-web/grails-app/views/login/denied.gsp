<head>
  <meta name='layout' content='dashboard'/>
  <title><g:message code="springSecurity.denied.title"/></title>
</head>

<body>
<div class='body'>
  <div class='errors'><g:message code="springSecurity.denied.message"/></div>
</div>
</body>
