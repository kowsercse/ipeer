<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="dashboard"/>
  <title>IPeer</title>
</head>

<body>
<div id="controller-list" role="navigation">
  <g:if test="${version != null}">
    <h2>Download Apps</h2>
    <ul>
      <li><a class="mdl-color-text--green-700" href="${createLink(mapping: 'downloadMentor', id: version.id, absolute: true)}">mentor-${version}.apk</a></li>
      <li><a class="mdl-color-text--green-700" href="${createLink(mapping: 'downloadMentee', id: version.id, absolute: true)}">veteran-${version}.apk</a></li>
    </ul>
  </g:if>
</div>
</body>
</html>
