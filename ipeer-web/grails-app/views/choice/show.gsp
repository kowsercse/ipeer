
<%@ page import="ipeer.entity.Choice" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'choice.label', default: 'Choice')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--6-col mdl-cell--4-col-phone">
  <div class="mdl-card__title mdl-shadow--2dp">
    <g:message code="default.show.label" args="[entityName]"/>
  </div>

  <div class="mdl-card__supporting-text">
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>

    
    <g:if test="${choice?.label}">
      <div class="mdl-grid">
        <div id="label-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="choice.label.label" default="Label"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="label-label">
          
          <g:fieldValue bean="${choice}" field="label"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${choice?.value}">
      <div class="mdl-grid">
        <div id="value-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="choice.value.label" default="Value"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="value-label">
          
          <g:fieldValue bean="${choice}" field="value"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${choice?.questions}">
      <div class="mdl-grid">
        <div id="questions-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="choice.questions.label" default="Questions"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="questions-label">
          
          <g:each in="${choice.questions}" var="q">
            <g:link controller="question" namespace="admin" action="show" id="${q.id}">${q?.encodeAsHTML()}</g:link>
          </g:each>
          
        </div>
      </div>
    </g:if>
    
  </div>


  <g:form class="mdl-card__actions mdl-card--border" action="delete" resource="${choice}" namespace="admin" method="DELETE">
      <g:link action="edit" namespace="admin" resource="${choice}"
              class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
        <g:message code="default.button.edit.label" default="Edit"/>
      </g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link>
  </g:form>
</div>

</body>
</html>
