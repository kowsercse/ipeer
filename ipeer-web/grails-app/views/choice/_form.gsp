<%@ page import="ipeer.entity.Choice" %>




<div class="mdl-grid ${hasErrors(bean: choice, field: 'label', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="label">
    <g:message code="choice.label.label" default="Label"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:textField name="label" required="" value="${choice?.label}"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: choice, field: 'value', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="value">
    <g:message code="choice.value.label" default="Value"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:field name="value" type="number" value="${choice.value}" required=""/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: choice, field: 'questions', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="questions">
    <g:message code="choice.questions.label" default="Questions"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col">
</div>
</div>

