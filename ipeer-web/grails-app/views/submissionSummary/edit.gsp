<%@ page import="ipeer.entity.SubmissionSummary" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'submissionSummary.label', default: 'SubmissionSummary')}"/>
  <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>

<g:form class="mdl-card  mdl-shadow--2dp mdl-cell mdl-cell--8-col mdl-cell--4-col-phone mdl-cell--4-col-tablet mdl-cell--8-col-desktop"
        action="update" namespace="admin" resource="${submissionSummary}"
        method="PUT" >
  <g:hiddenField name="version" value="${submissionSummary?.version}"/>
  <div class="mdl-card__title mdl-shadow--2dp">
    <g:message code="default.create.label" args="[entityName]"/>
  </div>
  <div class="mdl-card__supporting-text">
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${submissionSummary}">
      <ul class="errors" role="alert">
      <g:eachError bean="${submissionSummary}" var="error">
        <li<g:if
          test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
          error="${error}"/></li>
      </g:eachError>
      </ul>
    </g:hasErrors>

    <g:render template="form"/>
  </div>
  <div class="mdl-card__actions mdl-card--border">
    <g:actionSubmit class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--green-700 mdl-color-text--white"
                    action="update" value="${message(code: 'default.button.update.label', default: 'Update')}"/>
    <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" action="index" namespace="admin">
      Cancel
    </g:link>
    <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" action="create" namespace="admin">
      <g:message code="default.new.label" args="[entityName]"/>
    </g:link>
  </div>
</g:form>

</body>
</html>
