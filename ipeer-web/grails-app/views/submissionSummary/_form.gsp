<%@ page import="ipeer.entity.SubmissionSummary" %>




<div class="mdl-grid ${hasErrors(bean: submissionSummary, field: 'schedule', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="schedule">
    <g:message code="submissionSummary.schedule.label" default="Schedule"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:select id="schedule" name="schedule.id" from="${ipeer.entity.Schedule.list()}" optionKey="id" required="" value="${submissionSummary?.schedule?.id}" class="many-to-one"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: submissionSummary, field: 'veteran', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="veteran">
    <g:message code="submissionSummary.veteran.label" default="Veteran"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:select id="veteran" name="veteran.id" from="${ipeer.entity.Veteran.list()}" optionKey="id" required="" value="${submissionSummary?.veteran?.id}" class="many-to-one"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: submissionSummary, field: 'submissionTime', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="submissionTime">
    <g:message code="submissionSummary.submissionTime.label" default="Submission Time"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:datePicker name="submissionTime" precision="day"  value="${submissionSummary?.submissionTime}"  />
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: submissionSummary, field: 'score', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="score">
    <g:message code="submissionSummary.score.label" default="Score"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:field name="score" type="number" value="${submissionSummary.score}" required=""/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: submissionSummary, field: 'progressDirection', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="progressDirection">
    <g:message code="submissionSummary.progressDirection.label" default="Progress Direction"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:field name="progressDirection" type="number" value="${submissionSummary.progressDirection}" required=""/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: submissionSummary, field: 'submittedBy', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="submittedBy">
    <g:message code="submissionSummary.submittedBy.label" default="Submitted By"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:select id="submittedBy" name="submittedBy.id" from="${ipeer.entity.Mentor.list()}" optionKey="id" value="${submissionSummary?.submittedBy?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: submissionSummary, field: 'submittedAnswers', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="submittedAnswers">
    <g:message code="submissionSummary.submittedAnswers.label" default="Submitted Answers"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col">
<ul class="one-to-many">
<g:each in="${submissionSummary?.submittedAnswers?}" var="s">
    <li><g:link controller="submittedAnswer" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="submittedAnswer" action="create" params="['submissionSummary.id': submissionSummary?.id]">${message(code: 'default.add.label', args: [message(code: 'submittedAnswer.label', default: 'SubmittedAnswer')])}</g:link>
</li>
</ul>

</div>
</div>

