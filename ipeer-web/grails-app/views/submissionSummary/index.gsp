
<%@ page import="ipeer.entity.SubmissionSummary" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'submissionSummary.label', default: 'SubmissionSummary')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-desktop  mdl-cell--8-col-tablet  mdl-cell--6-col-phone">
  <h1><g:message code="default.list.label" args="[entityName]"/></h1>
  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>

  <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
    <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="submissionSummary.id.label" default="#"/>
      </th>
      
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="submissionSummary.schedule.label" default="Schedule"/>
      </th>
      
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="submissionSummary.veteran.label" default="Veteran"/>
      </th>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="submissionTime"
                        title="${message(code: 'submissionSummary.submissionTime.label', default: 'Submission Time')}"/>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="score"
                        title="${message(code: 'submissionSummary.score.label', default: 'Score')}"/>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="progressDirection"
                        title="${message(code: 'submissionSummary.progressDirection.label', default: 'Progress Direction')}"/>
      
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="submissionSummary.submittedBy.label" default="Submitted By"/>
      </th>
      
      <th class="mdl-data-table__cell--non-numeric">Action</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${submissionSummaryList}" status="i" var="submissionSummary">
      <tr>
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${submissionSummary.id}">${submissionSummary.id}</g:link>
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
            
            <g:link namespace="admin" controller="schedule" action="show" id="${submissionSummary.schedule.id}">${submissionSummary.schedule}</g:link>
            
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
            
            <g:link namespace="admin" controller="veteran" action="show" id="${submissionSummary.veteran.id}">${submissionSummary.veteran}</g:link>
            
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          <g:formatDate date="${submissionSummary.submissionTime}"/>
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          ${fieldValue(bean: submissionSummary, field: "score")}
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          ${fieldValue(bean: submissionSummary, field: "progressDirection")}
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">


            <g:if test="${submissionSummary.submittedBy != null}">
            <g:link namespace="admin" controller="mentor" action="show" id="${submissionSummary.submittedBy.id}">${submissionSummary.submittedBy}</g:link>
            </g:if>
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${submissionSummary.id}"><i class="mdl-color-text--grey material-icons">description</i></g:link>
        </td>
      </tr>
    </g:each>
    </tbody>
  </table>

  <div class="mdl-card__actions pagination">
    <g:link namespace="admin" controller="veteran" action="index"
            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--green-700 mdl-color-text--white">
      Veterans
    </g:link>
    <g:paginate namespace="admin" controller="submissionSummary" action="index" total="${submissionSummaryCount ?: 0}"/>
  </div>
</div>
</body>
</html>
