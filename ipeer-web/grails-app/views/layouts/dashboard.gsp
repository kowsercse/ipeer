<!doctype html>
<!--
  Material Design Lite
  Copyright 2015 Google Inc. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      https://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><g:layoutTitle default="IPeer"/></title>

  <!-- Add to homescreen for Chrome on Android -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">

  <!-- Add to homescreen for Safari on iOS -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="Material Design Lite">
  <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">

  <!-- Tile icon for Win8 (144x144 + tile color) -->
  <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
  <meta name="msapplication-TileColor" content="#3372DF">

  <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
  <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

  <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="${resource(dir: 'components/material-design-lite', file: 'material.min.css')}" type="text/css">
  <asset:stylesheet src="styles.css"/>
  <style>
  .required-indicator {
    color: #48802C;
    display: inline-block;
    font-weight: bold;
    margin-left: 0.3em;
    position: relative;
    top: 0.1em;
  }
  .error input, .error select, .error textarea {
    background: #fff3f3;
    border-color: #ffaaaa;
    color: #cc0000;
  }

  .error input:focus, .error select:focus, .error textarea:focus {
    -moz-box-shadow: 0 0 0.5em #ffaaaa;
    -webkit-box-shadow: 0 0 0.5em #ffaaaa;
    box-shadow: 0 0 0.5em #ffaaaa;
  }
  </style>
</head>
<body>
<div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
  <header class="demo-header mdl-layout__header mdl-color--white mdl-color--grey-100 mdl-color-text--grey-600">
    <div class="mdl-layout__header-row">
      <span class="mdl-layout-title">IPeer</span>
    </div>
  </header>
  <div class="demo-drawer mdl-layout__drawer mdl-color--grey-700 mdl-color-text--white">
    <header class="demo-drawer-header">
      <div class="demo-avatar-dropdown">
        <span>Welcome <sec:username/></span>
        <div class="mdl-layout-spacer"></div>
        <sec:ifLoggedIn roles="ROLE_SUPER_ADMIN">
          <button id="accbtn" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
            <i class="material-icons">arrow_drop_down</i>
          </button>
          <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="accbtn">
            <g:link class="mdl-navigation__link" namespace="front" controller="profile" action="changePassword"><button class="mdl-menu__item">Change Password</button></g:link>
            <g:link class="mdl-navigation__link" uri="/j_spring_security_logout"><button class="mdl-menu__item">Logout</button></g:link>
          </ul>
        </sec:ifLoggedIn>
      </div>
    </header>
    <nav class="demo-navigation mdl-navigation mdl-color--grey-600 mdl-color-text--white">
      <g:link class="mdl-navigation__link" controller="home" action="index"><i class="material-icons">dashboard</i>Home</g:link>
      <sec:ifAnyGranted roles="ROLE_MENTOR">
        <g:link class="mdl-navigation__link" namespace="admin" controller="veteran" action="index"><i class="material-icons">people_outline</i>My Veterans</g:link>
      </sec:ifAnyGranted>

      <sec:ifAnyGranted roles="ROLE_SUPER_ADMIN">
        <g:link class="mdl-navigation__link" namespace="admin" controller="user" action="index"><i class="material-icons">person</i>Users</g:link>
        <g:link class="mdl-navigation__link" namespace="admin" controller="mentor" action="index"><i class="material-icons">supervisor_account</i>Mentor</g:link>
        <g:link class="mdl-navigation__link" namespace="admin" controller="choice" action="index"><i class="material-icons">question_answer</i>Choice</g:link>
        <g:link class="mdl-navigation__link" namespace="admin" controller="question" action="index"><i class="material-icons">question_answer</i>Question</g:link>
        <g:link class="mdl-navigation__link" namespace="admin" controller="survey" action="index"><i class="material-icons">poll</i>Survey</g:link>
        <g:link class="mdl-navigation__link" namespace="admin" controller="schedule" action="index"><i class="material-icons">schedule</i>Schedule</g:link>
        <g:link class="mdl-navigation__link" namespace="admin" controller="report" action="surveyCompletion"><i class="material-icons">assessment</i>Reports</g:link>
        <g:link class="mdl-navigation__link" namespace="admin" controller="version" action="index"><i class="material-icons">file_download</i>Downloads</g:link>
        <g:link class="mdl-navigation__link" namespace="front" controller="issue" action="index"><i class="material-icons">feedback</i>Feedback</g:link>
      </sec:ifAnyGranted>
      <div class="mdl-layout-spacer"></div>
      <g:link class="mdl-navigation__link" controller="documentation" action="index"><i class="material-icons">help_outline</i>Help</g:link>
      <g:link class="mdl-navigation__link" namespace="front" controller="issue" action="create"><i class="material-icons">feedback</i>Feedback</g:link>
    </nav>
  </div>
  <main class="mdl-layout__content mdl-color--grey-100">
    <div class="mdl-components__page mdl-grid">
      <g:layoutBody/>
    </div>
  </main>
</div>
<script src="${resource(dir: 'components/material-design-lite', file: 'material.min.js')}"></script>
</body>
</html>
