<%@ page import="ipeer.entity.Mentor" %>




<div class="mdl-grid ${hasErrors(bean: mentor, field: 'firstName', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="firstName">
    <g:message code="mentor.firstName.label" default="First Name"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:textField name="firstName" required="" value="${mentor?.firstName}"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: mentor, field: 'lastName', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="lastName">
    <g:message code="mentor.lastName.label" default="Last Name"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:textField name="lastName" required="" value="${mentor?.lastName}"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: mentor, field: 'phone', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="phone">
    <g:message code="mentor.phone.label" default="Phone"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:textField name="phone" required="" value="${mentor?.phone}"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: mentor, field: 'email', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="email">
    <g:message code="mentor.email.label" default="Email"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:textField name="email" required="" value="${mentor?.email}"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: mentor, field: 'user', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="user">
    <g:message code="mentor.user.label" default="User"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:select id="user" name="user.id" from="${ipeer.entity.User.list()}" optionKey="id" value="${mentor?.user?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: mentor, field: 'veterans', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="veterans">
    <g:message code="mentor.veterans.label" default="Veterans"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col">
<ul class="one-to-many">
<g:each in="${mentor?.veterans?}" var="v">
    <li><g:link controller="veteran" action="show" id="${v.id}">${v?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="veteran" action="create" params="['mentor.id': mentor?.id]">${message(code: 'default.add.label', args: [message(code: 'veteran.label', default: 'Veteran')])}</g:link>
</li>
</ul>

</div>
</div>

