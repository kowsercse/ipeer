<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="dashboard">
  <title>IPeer :: Completion report</title>
</head>

<body>
<div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-desktop  mdl-cell--8-col-tablet  mdl-cell--6-col-phone">
  <h1>Survey Completions</h1>

  <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
    <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="veteran.id.label" default="#"/>
      </th>

      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="veteran.user.label" default="User"/>
      </th>

      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="veteran.mentor.label" default="Mentor"/>
      </th>
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="veteran.startDate.label" default="Start Date"/>
      </th>

      <td>Completed</td>
      <td>Remaining</td>

      <th class="mdl-data-table__cell--non-numeric">Action</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${veteranList}" status="i" var="veteran">
      <tr>
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${veteran.id}">${veteran.id}</g:link>
        </td>

        <td class="mdl-data-table__cell--non-numeric">
          <g:if test="${veteran.user != null}">
            <g:link namespace="admin" controller="user" action="show" id="${veteran.user.id}">${veteran.user}</g:link>
          </g:if>
        </td>

        <td class="mdl-data-table__cell--non-numeric">
          <g:if test="${veteran.mentor != null}">
            <g:link namespace="admin" controller="mentor" action="show" id="${veteran.mentor.id}">${veteran.mentor}</g:link>
          </g:if>
        </td>

        <td class="mdl-data-table__cell--non-numeric">
          <g:formatDate date="${veteran.startDate}"/>
        </td>

        <td>${veteran.submissionSummaries.size()}</td>
        <td>${12 - veteran.submissionSummaries.size()}</td>

        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${veteran.id}"><i class="mdl-color-text--grey material-icons">description</i></g:link>
        </td>
      </tr>
    </g:each>
    </tbody>
  </table>

  <div class="mdl-card__actions pagination">
    <g:paginate namespace="admin" controller="veteran" action="index" total="${veteranCount ?: 0}"/>
  </div>
</div>
</body>
</html>
