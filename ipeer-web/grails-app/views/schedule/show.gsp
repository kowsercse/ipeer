
<%@ page import="ipeer.entity.Schedule" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'schedule.label', default: 'Schedule')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--6-col mdl-cell--4-col-phone">
  <div class="mdl-card__title mdl-shadow--2dp">
    <g:message code="default.show.label" args="[entityName]"/>
  </div>

  <div class="mdl-card__supporting-text">
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>

    
    <g:if test="${schedule?.start}">
      <div class="mdl-grid">
        <div id="start-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="schedule.start.label" default="Start"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="start-label">
          
          <g:formatDate date="${schedule?.start}"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${schedule?.end}">
      <div class="mdl-grid">
        <div id="end-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="schedule.end.label" default="End"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="end-label">
          
          <g:formatDate date="${schedule?.end}"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${schedule?.survey}">
      <div class="mdl-grid">
        <div id="survey-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="schedule.survey.label" default="Survey"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="survey-label">
          
          <g:link controller="survey" namespace="admin" action="show" id="${schedule?.survey?.id}">${schedule?.survey?.encodeAsHTML()}</g:link>
          
        </div>
      </div>
    </g:if>
    
  </div>


  <g:form class="mdl-card__actions mdl-card--border" action="delete" resource="${schedule}" namespace="admin" method="DELETE">
      <g:link action="edit" namespace="admin" resource="${schedule}"
              class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
        <g:message code="default.button.edit.label" default="Edit"/>
      </g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link>
  </g:form>
</div>

</body>
</html>
