<%@ page import="ipeer.entity.Schedule" %>




<div class="mdl-grid ${hasErrors(bean: schedule, field: 'start', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="start">
    <g:message code="schedule.start.label" default="Start"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:datePicker name="start" precision="day"  value="${schedule?.start}"  />
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: schedule, field: 'end', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="end">
    <g:message code="schedule.end.label" default="End"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:datePicker name="end" precision="day"  value="${schedule?.end}"  />
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: schedule, field: 'survey', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="survey">
    <g:message code="schedule.survey.label" default="Survey"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:select id="survey" name="survey.id" from="${ipeer.entity.Survey.list()}" optionKey="id" required="" value="${schedule?.survey?.id}" class="many-to-one"/>
</div>
</div>

