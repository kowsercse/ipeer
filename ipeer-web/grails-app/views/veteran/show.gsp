
<%@ page import="ipeer.entity.Veteran" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'veteran.label', default: 'Veteran')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--6-col mdl-cell--4-col-phone">
  <div class="mdl-card__title mdl-shadow--2dp">
    <g:message code="default.show.label" args="[entityName]"/>
  </div>

  <div class="mdl-card__supporting-text">
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>

    
    <g:if test="${veteran?.user}">
      <div class="mdl-grid">
        <div id="user-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="veteran.user.label" default="User"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="user-label">
          
          <g:link controller="user" namespace="admin" action="show" id="${veteran?.user?.id}">${veteran?.user?.toString()?.encodeAsHTML()}</g:link>
          
        </div>
      </div>
    </g:if>

    <g:if test="${veteran?.redCapUserId}">
      <div class="mdl-grid">
        <div id="redCapUserId-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="veteran.redCapUserId.label" default="Red Cap User ID"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="redCapUserId-label">

          <g:fieldValue bean="${veteran}" field="redCapUserId"/>

        </div>
      </div>
    </g:if>

    <g:if test="${veteran?.mentor}">
      <div class="mdl-grid">
        <div id="mentor-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="veteran.mentor.label" default="Mentor"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="mentor-label">
          
          <g:link controller="mentor" namespace="admin" action="show" id="${veteran?.mentor?.id}">${veteran?.mentor?.toString()?.encodeAsHTML()}</g:link>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${veteran?.startDate}">
      <div class="mdl-grid">
        <div id="startDate-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="veteran.startDate.label" default="Start Date"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="startDate-label">
          
          <g:formatDate date="${veteran?.startDate}"/>
          
        </div>
      </div>
    </g:if>

  </div>


  <g:form class="mdl-card__actions mdl-card--border" action="delete" resource="${veteran}" namespace="admin" method="DELETE">
      <g:link action="edit" namespace="admin" resource="${veteran}"
              class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
        <g:message code="default.button.edit.label" default="Edit"/>
      </g:link>
    <g:if test="${veteran.user != null}">
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" controller="user" action="changePassword" id="${veteran.user.id}">Change Password</g:link>
    </g:if>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link>
  </g:form>
</div>

<div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-desktop  mdl-cell--8-col-tablet  mdl-cell--6-col-phone">
  <h4>Survey Submissions</h4>

  <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
    <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="submissionSummary.id.label" default="#"/>
      </th>

      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="submissionSummary.schedule.label" default="Schedule"/>
      </th>

      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="submissionSummary.veteran.label" default="Veteran"/>
      </th>

      <th class="mdl-data-table__cell--non-numeric">Submission Time</th>

      <th class="mdl-data-table__cell--non-numeric">Score</th>

      <th class="mdl-data-table__cell--non-numeric">Progress Direction</th>

      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="submissionSummary.submittedBy.label" default="Submitted By"/>
      </th>

      <th class="mdl-data-table__cell--non-numeric">Action</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${veteran?.submissionSummaries}" status="i" var="submissionSummary">
      <tr>
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${submissionSummary.id}">${submissionSummary.id}</g:link>
        </td>

        <td class="mdl-data-table__cell--non-numeric">



          <g:link namespace="admin" controller="schedule" action="show" id="${submissionSummary.schedule.id}">${submissionSummary.schedule}</g:link>


        </td>

        <td class="mdl-data-table__cell--non-numeric">



          <g:link namespace="admin" controller="veteran" action="show" id="${submissionSummary.veteran.id}">${submissionSummary.veteran}</g:link>


        </td>

        <td class="mdl-data-table__cell--non-numeric">


          <g:formatDate date="${submissionSummary.submissionTime}"/>

        </td>

        <td class="mdl-data-table__cell--non-numeric">


          ${fieldValue(bean: submissionSummary, field: "score")}

        </td>

        <td class="mdl-data-table__cell--non-numeric">


          ${fieldValue(bean: submissionSummary, field: "progressDirection")}

        </td>

        <td class="mdl-data-table__cell--non-numeric">


          <g:if test="${submissionSummary.submittedBy != null}">
            <g:link namespace="admin" controller="mentor" action="show" id="${submissionSummary.submittedBy.id}">${submissionSummary.submittedBy}</g:link>
          </g:if>

        </td>

        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" controller="submissionSummary" action="show" id="${submissionSummary.id}"><i class="mdl-color-text--grey material-icons">description</i></g:link>
        </td>
      </tr>
    </g:each>
    </tbody>
  </table>

</div>

</body>
</html>
