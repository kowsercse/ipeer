<%@ page import="ipeer.entity.Veteran" %>




<div class="mdl-grid ${hasErrors(bean: veteran, field: 'user', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="user">
    <g:message code="veteran.user.label" default="User"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:select id="user" name="user.id" from="${ipeer.entity.User.list()}" optionKey="id" value="${veteran?.user?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: veteran, field: 'mentor', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="mentor">
    <g:message code="veteran.mentor.label" default="Mentor"/>

  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:select id="mentor" name="mentor.id" from="${ipeer.entity.Mentor.list()}" optionKey="id" value="${veteran?.mentor?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: veteran, field: 'startDate', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="startDate">
    <g:message code="veteran.startDate.label" default="Start Date"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:datePicker name="startDate" precision="day"  value="${veteran?.startDate}"  />
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: veteran, field: 'redCapUserId', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="redCapUserId">
    <g:message code="veteran.redCapUserId.label" default="Red Cap User Id"/>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:textField name="redCapUserId" required="" value="${veteran?.redCapUserId}"/>
  </div>
</div>
