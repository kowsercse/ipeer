<%@ page import="ipeer.entity.Issue" %>


<div class="mdl-grid ${hasErrors(bean: issue, field: 'summary', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col"><label for="summary">
    <g:message code="issue.summary.label" default="Summary"/>
    <span class="required-indicator">*</span>
  </label>
  </div>

  <div class="mdl-cell mdl-cell--3-col"><g:textField size="50" name="summary" required=""
                                                     value="${issue?.summary}"/>
  </div>
</div>

<div class="mdl-grid ${hasErrors(bean: issue, field: 'detail', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col"><label for="detail">
    <g:message code="issue.detail.label" default="Detail"/>
    <span class="required-indicator">*</span>
  </label>
  </div>

  <div class="mdl-cell mdl-cell--3-col">
    <g:textArea name="detail" cols="50" rows="10" required="" value="${issue?.detail}"/>
  </div>
</div>


<div class="mdl-grid ${hasErrors(bean: issue, field: 'errorLog', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col"><label for="errorLog">
    <g:message code="issue.errorLog.label" default="Error Log"/>
  </label>
  </div>

  <div class="mdl-cell mdl-cell--3-col">
    <g:textArea name="errorLog" cols="50" rows="10" value="${issue?.errorLog}"/>
  </div>
</div>
