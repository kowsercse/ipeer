
<%@ page import="ipeer.entity.Issue" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'issue.label', default: 'Issue')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--6-col mdl-cell--4-col-phone">
  <div class="mdl-card__title mdl-shadow--2dp">
    <g:message code="default.show.label" args="[entityName]"/>
  </div>

  <div class="mdl-card__supporting-text">
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>

    
    <g:if test="${issue?.errorLog}">
      <div class="mdl-grid">
        <div id="errorLog-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="issue.errorLog.label" default="Error Log"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="errorLog-label">
          
          <g:fieldValue bean="${issue}" field="errorLog"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${issue?.autoReported}">
      <div class="mdl-grid">
        <div id="autoReported-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="issue.autoReported.label" default="Auto Reported"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="autoReported-label">
          
          <g:formatBoolean boolean="${issue?.autoReported}"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${issue?.createdBy}">
      <div class="mdl-grid">
        <div id="createdBy-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="issue.createdBy.label" default="Created By"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="createdBy-label">
          
          <g:link controller="user" namespace="admin" action="show" id="${issue?.createdBy?.id}">${issue?.createdBy?.encodeAsHTML()}</g:link>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${issue?.dateCreated}">
      <div class="mdl-grid">
        <div id="dateCreated-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="issue.dateCreated.label" default="Date Created"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="dateCreated-label">
          
          <g:formatDate date="${issue?.dateCreated}"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${issue?.detail}">
      <div class="mdl-grid">
        <div id="detail-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="issue.detail.label" default="Detail"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="detail-label">
          
          <g:fieldValue bean="${issue}" field="detail"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${issue?.lastUpdated}">
      <div class="mdl-grid">
        <div id="lastUpdated-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="issue.lastUpdated.label" default="Last Updated"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="lastUpdated-label">
          
          <g:formatDate date="${issue?.lastUpdated}"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${issue?.summary}">
      <div class="mdl-grid">
        <div id="summary-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="issue.summary.label" default="Summary"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="summary-label">
          
          <g:fieldValue bean="${issue}" field="summary"/>
          
        </div>
      </div>
    </g:if>
    
  </div>


  <g:form class="mdl-card__actions mdl-card--border" action="delete" resource="${issue}" namespace="front" method="DELETE">
      <g:link action="edit" namespace="front" resource="${issue}"
              class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
        <g:message code="default.button.edit.label" default="Edit"/>
      </g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="front" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="front" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link>
  </g:form>
</div>

</body>
</html>
