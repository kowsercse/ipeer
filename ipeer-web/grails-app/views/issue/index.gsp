
<%@ page import="ipeer.entity.Issue" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'issue.label', default: 'Issue')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-desktop  mdl-cell--8-col-tablet  mdl-cell--6-col-phone">
  <h1><g:message code="default.list.label" args="[entityName]"/></h1>
  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>

  <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
    <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="issue.id.label" default="#"/>
      </th>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="errorLog"
                        title="${message(code: 'issue.errorLog.label', default: 'Error Log')}"/>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="autoReported"
                        title="${message(code: 'issue.autoReported.label', default: 'Auto Reported')}"/>
      
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="issue.createdBy.label" default="Created By"/>
      </th>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="dateCreated"
                        title="${message(code: 'issue.dateCreated.label', default: 'Date Created')}"/>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="detail"
                        title="${message(code: 'issue.detail.label', default: 'Detail')}"/>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="lastUpdated"
                        title="${message(code: 'issue.lastUpdated.label', default: 'Last Updated')}"/>
      
      <th class="mdl-data-table__cell--non-numeric">Action</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${issueList}" status="i" var="issue">
      <tr>
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="front" action="show" id="${issue.id}">${issue.id}</g:link>
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          ${fieldValue(bean: issue, field: "errorLog")}
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          <g:formatBoolean boolean="${issue.autoReported}"/>
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
            
            <g:link namespace="admin" controller="user" action="show" id="${issue.createdBy.id}">${issue.createdBy}</g:link>
            
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          <g:formatDate date="${issue.dateCreated}"/>
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          ${fieldValue(bean: issue, field: "detail")}
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          <g:formatDate date="${issue.lastUpdated}"/>
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="front" action="show" id="${issue.id}"><i class="mdl-color-text--grey material-icons">description</i></g:link>
          <g:link namespace="front" action="edit" id="${issue.id}"><i class="mdl-color-text--grey material-icons">edit</i></g:link>
        </td>
      </tr>
    </g:each>
    </tbody>
  </table>

  <div class="mdl-card__actions pagination">
    <g:link action="create" namespace="front"
            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--green-700 mdl-color-text--white">
      <g:message code="default.new.label" args="[entityName]"/>
    </g:link>
    <g:paginate total="${issueCount ?: 0}"/>
  </div>
</div>
</body>
</html>
