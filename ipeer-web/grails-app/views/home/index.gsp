<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="dashboard"/>
  <title>IPeer</title>
</head>

<body>
<g:include view="veteranSurvey/currentSurvey.gsp" model="['currentSchedule': currentSchedule]"/>
<div class="mdl-cell mdl-cell--5-col">
  <h5>Download Apps</h5>
  <ul>
    <li><a class="mdl-color-text--green-700" href="${createLink(mapping: 'downloadMentee', id: latestVersion.id, absolute: true)}">veteran-${version}.apk</a></li>
  </ul>
</div>
</body>
</html>