<%@ page import="ipeer.entity.User" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <title>Reset Password</title>
</head>

<body>

<div>
  You have successfully changed your password. <sec:ifNotLoggedIn> Please <g:link style="margin: 0 2px 0 2px" controller="login" action="auth">login here</g:link></sec:ifNotLoggedIn>
</div>

</body>
</html>
