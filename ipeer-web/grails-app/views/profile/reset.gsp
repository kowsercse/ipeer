<%@ page import="ipeer.entity.User" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <title>Reset Password</title>
</head>

<body>

<g:form class="mdl-card  mdl-shadow--2dp mdl-cell mdl-cell--8-col mdl-cell--4-col-phone mdl-cell--4-col-tablet mdl-cell--8-col-desktop"
        namespace="front" controller="profile" action="resetPassword" params="[token: resetToken]" method="PUT">
  <div class="mdl-card__title mdl-shadow--2dp">Reset Password</div>
  <div class="mdl-card__supporting-text">
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ul class="errors">
      <g:each in="${errors}" var="error">
        <li>${error}</li>
      </g:each>
    </ul>

    <div class="mdl-grid required">
      <div class="mdl-cell mdl-cell--3-col">
        <label for="newPassword">New Password <span class="required-indicator">*</span></label>
      </div>
      <div class="mdl-cell mdl-cell--3-col"><g:passwordField name="newPassword" required=""/></div>
    </div>
    <div class="mdl-grid required">
      <div class="mdl-cell mdl-cell--3-col">
        <label for="confirmPassword">Confirm Password <span class="required-indicator">*</span></label>
      </div>
      <div class="mdl-cell mdl-cell--3-col"><g:passwordField name="confirmPassword" required=""/></div>
    </div>
  </div>
  <div class="mdl-card__actions mdl-card--border">
    <g:actionSubmit class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--green-700 mdl-color-text--white"
                    action="resetPassword" value="Reset Password"/>
    <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" action="index" namespace="admin">Cancel</g:link>
  </div>
</g:form>

</body>
</html>
