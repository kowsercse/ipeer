package ipeer.webservice

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import ipeer.entity.Mentor
import ipeer.entity.User
import ipeer.entity.Veteran

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class MentorController {

  static namespace = "webservice"
  static responseFormats = ['json', 'xml']
  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  def springSecurityService
  def veteranService

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond Mentor.list(params), [status: OK]
  }

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def show(Mentor mentor) {
    respond mentor
  }

  @Secured(['ROLE_MENTOR'])
  def veterans(Mentor mentor) {
    respond mentor.veterans
  }

  @Secured(['ROLE_MENTOR'])
  @Transactional
  def addVeteran() {
    User user = springSecurityService.getCurrentUser() as User
    Veteran veteran = veteranService.addVeteran(user.getMentor(), null, null, request.JSON.redCapUserId)
    respond veteran, [status: CREATED]
  }

  @Transactional
  def save(Mentor mentorInstance) {
    if (mentorInstance == null) {
      render status: NOT_FOUND
      return
    }

    mentorInstance.validate()
    if (mentorInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    mentorInstance.save flush: true
    respond mentorInstance, [status: CREATED]
  }

  @Transactional
  def update(Mentor mentorInstance) {
    if (mentorInstance == null) {
      render status: NOT_FOUND
      return
    }

    mentorInstance.validate()
    if (mentorInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    mentorInstance.save flush: true
    respond mentorInstance, [status: OK]
  }

  @Transactional
  def delete(Mentor mentorInstance) {

    if (mentorInstance == null) {
      render status: NOT_FOUND
      return
    }

    mentorInstance.delete flush: true
    render status: NO_CONTENT
  }
}
