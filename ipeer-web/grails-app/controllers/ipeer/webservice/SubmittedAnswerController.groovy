package ipeer.webservice

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import ipeer.entity.Role
import ipeer.entity.SubmissionSummary
import ipeer.entity.SubmittedAnswer
import ipeer.entity.User

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class SubmittedAnswerController {

  static namespace = "webservice"
  static responseFormats = ['json', 'xml']
  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  def springSecurityService

  def beforeInterceptor = {
    if(springSecurityService.authentication.authorities.any {it.authority == Role.ROLE_MENTOR}) {
      return true
    }

    def submittedAnswer = SubmittedAnswer.findById(params.id)
    def user = springSecurityService.currentUser as User
    if(user?.veteran != submittedAnswer.submissionSummary.veteran) {
      render(status: NOT_FOUND)
      return false
    }

    return true
  }

  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond SubmittedAnswer.list(params), [status: OK]
  }

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def show(SubmittedAnswer submittedAnswer) {
    respond submittedAnswer
  }

  @Transactional
  def save(SubmittedAnswer submittedAnswerInstance) {
    if (submittedAnswerInstance == null) {
      render status: NOT_FOUND
      return
    }

    submittedAnswerInstance.validate()
    if (submittedAnswerInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    submittedAnswerInstance.save flush: true
    respond submittedAnswerInstance, [status: CREATED]
  }

  @Transactional
  def update(SubmittedAnswer submittedAnswerInstance) {
    if (submittedAnswerInstance == null) {
      render status: NOT_FOUND
      return
    }

    submittedAnswerInstance.validate()
    if (submittedAnswerInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    submittedAnswerInstance.save flush: true
    respond submittedAnswerInstance, [status: OK]
  }

  @Transactional
  def delete(SubmittedAnswer submittedAnswerInstance) {

    if (submittedAnswerInstance == null) {
      render status: NOT_FOUND
      return
    }

    submittedAnswerInstance.delete flush: true
    render status: NO_CONTENT
  }
}
