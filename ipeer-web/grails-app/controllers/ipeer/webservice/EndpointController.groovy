package ipeer.webservice

import grails.plugin.springsecurity.annotation.Secured
import ipeer.entity.Choice
import ipeer.entity.Question
import ipeer.entity.Survey

import static org.springframework.http.HttpStatus.OK

@Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
class EndpointController {

  static namespace = "webservice"
  static responseFormats = ['json', 'xml']
  def springSecurityService

  def index() {
    render template: 'index', model: [user: springSecurityService.currentUser], contentType: 'application/json'
  }

  def surveys(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond Survey.list(params), [status: OK]
  }

  def questions(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond Question.list(params), [status: OK]
  }

  def choices(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond Choice.list(params), [status: OK]
  }

}
