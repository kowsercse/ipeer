package ipeer.webservice

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import ipeer.entity.*
import ipeer.service.AuthenticationTokenService
import ipeer.service.UserService
import ipeer.service.VeteranService
import org.grails.web.json.JSONObject

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class VeteranController {

  static namespace = "webservice"
  static responseFormats = ['json', 'xml']
  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  AuthenticationTokenService authenticationTokenService
  VeteranService veteranService
  def submissionSummaryService
  def springSecurityService

  def beforeInterceptor = {
    if(springSecurityService.authentication.authorities.any {it.authority == Role.ROLE_MENTOR}) {
      return true
    }

    def veteran = Veteran.findById(params.id)
    def user = springSecurityService.currentUser as User
    if(user?.veteran != veteran) {
      render(status: NOT_FOUND)
      return false
    }

    return true
  }

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond Veteran.list(params), [status: OK]
  }

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def show(Veteran veteran) {
    respond veteran
  }

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def schedules(Veteran veteran) {
    def schedules = veteranService.getGetSchedulesFor veteran
    respond schedules
  }

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def submissionSummaries(Veteran veteran) {
    respond veteran.submissionSummaries
  }

  @Transactional
  @Secured(['ROLE_VETERAN'])
  def submitAnswers() {
    def veteran = (springSecurityService.getCurrentUser() as User).veteran
    SubmissionSummary submissionSummary = submissionSummaryService.submitSurvey((JSONObject) request.JSON, veteran)
    return submissionSummary ? respond(submissionSummary, [status: CREATED]) : render(status: PRECONDITION_FAILED)
  }

  @Transactional
  @Secured(['ROLE_MENTOR'])
  def save(Veteran veteranInstance) {
    if (veteranInstance == null) {
      render status: NOT_FOUND
      return
    }

    veteranInstance.validate()
    if (veteranInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    veteranInstance.save flush: true
    respond veteranInstance, [status: CREATED]
  }

  @Transactional
  def update(Veteran veteranInstance) {
    if (veteranInstance == null) {
      render status: NOT_FOUND
      return
    }

    veteranInstance.validate()
    if (veteranInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    veteranInstance.save flush: true
    respond veteranInstance, [status: OK]
  }

  @Transactional
  def delete(Veteran veteranInstance) {
    if (veteranInstance == null) {
      render status: NOT_FOUND
      return
    }

    veteranInstance.delete flush: true
    render status: NO_CONTENT
  }

  @Transactional
  @Secured(['ROLE_MENTOR'])
  def resetToken(Veteran veteranInstance) {
    if (veteranInstance == null) {
      render status: NOT_FOUND
      return
    }

    def resetTokenUrl = veteranService.getPasswordResetTokenUrl(veteranInstance)
    render([contentType: "text/uri-list"], resetTokenUrl)
  }

  @Transactional
  @Secured('ROLE_VETERAN')
  def refreshToken(final Veteran veteran) {
    if (veteran == null) {
      render status: NOT_FOUND
      return
    }

    def refreshToken = authenticationTokenService.retrieveRefreshToken(veteran.user)
    respond refreshToken
  }

}
