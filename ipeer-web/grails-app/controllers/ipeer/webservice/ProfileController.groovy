package ipeer.webservice

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import ipeer.entity.Role
import ipeer.entity.User
import ipeer.service.AuthenticationTokenService
import org.grails.web.json.JSONObject

import static org.springframework.http.HttpStatus.NOT_FOUND

@Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
class ProfileController {

  static namespace = "webservice"
  static responseFormats = ['json', 'xml']

  def springSecurityService
  AuthenticationTokenService authenticationTokenService

  def beforeInterceptor = {
    if(springSecurityService.authentication.authorities.any {it.authority == Role.ROLE_MENTOR}) {
      return true
    }

    def user = User.findById(params.id)
    def currentUser = springSecurityService.currentUser as User
    if(currentUser != user) {
      render(status: NOT_FOUND)
      return false
    }

    return true
  }

  def show(User user) {
    respond user
  }

  @Transactional
  @Secured('permitAll')
  def token(String id) {
    def user = authenticationTokenService.findUserByPasswordRefreshToken(id)
    if (user == null) {
      render status: NOT_FOUND
      return
    }

    def authenticationToken = authenticationTokenService.retrieveAuthenticationToken(user)
    render([contentType: "application/json"], new JSONObject([tokenType: "Bearer", accessToken: authenticationToken.tokenValue]))
  }

}
