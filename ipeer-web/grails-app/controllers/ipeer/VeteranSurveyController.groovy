package ipeer

import grails.plugin.springsecurity.annotation.Secured
import ipeer.entity.Schedule
import ipeer.entity.SubmissionSummary
import ipeer.entity.User
import org.grails.web.json.JSONObject

class VeteranSurveyController {

  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
  static namespace = "front"

  def veteranService
  def springSecurityService
  def submissionSummaryService

  @Secured(['ROLE_VETERAN'])
  def currentSurvey() {
    def user = springSecurityService.currentUser as User
    Schedule currentSchedule = veteranService.getCurrentScheduleFor user.veteran
    render(view: 'currentSurvey', model: [currentSchedule: currentSchedule])
  }

  @Secured(['ROLE_VETERAN'])
  def submitAnswers() {
    def user = springSecurityService.getCurrentUser() as User
    Schedule schedule = veteranService.getCurrentScheduleFor user.veteran
    def json = [schedule: schedule.id, submittedAnswers: []]
    if (schedule) {
      schedule.survey.questions.forEach({
        def key = 'question-' + it.id
        if (params.containsKey(key)) {
          json.submittedAnswers.add(['question': it.id, 'choice': params[key]])
        }
      })
      SubmissionSummary submissionSummary = submissionSummaryService.submitSurvey(new JSONObject(json), user.veteran)
      if (submissionSummary) {
        return redirect(controller: 'home')
      }
    }

    render(view: 'currentSurvey', model: [currentSchedule: schedule])
  }

}
