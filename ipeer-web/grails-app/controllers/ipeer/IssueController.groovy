package ipeer

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import ipeer.entity.Issue
import ipeer.entity.Role
import ipeer.entity.User

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class IssueController {

  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
  static namespace = "front"

  def springSecurityService;

  @Secured(['ROLE_SUPER_ADMIN'])
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond Issue.list(params), model: [issueInstanceCount: Issue.count()]
  }

  @Secured(['ROLE_SUPER_ADMIN'])
  def show(Issue issueInstance) {
    respond issueInstance
  }

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def create() {
    respond new Issue(params)
  }

  @Transactional
  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def save(Issue issueInstance) {
    if (issueInstance == null) {
      notFound()
      return
    }

    issueInstance.createdBy = springSecurityService.currentUser as User
    issueInstance.autoReported = Boolean.FALSE

    if (!issueInstance.validate()) {
      respond issueInstance.errors, view: 'create'
      return
    }

    issueInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'issue.label', default: 'Issue'), issueInstance.id])
        redirect issueInstance
      }
      '*' { respond issueInstance, [status: CREATED] }
    }
  }

  @Secured(['ROLE_SUPER_ADMIN'])
  def edit(Issue issueInstance) {
    respond issueInstance
  }

  @Transactional
  @Secured(['ROLE_SUPER_ADMIN'])
  def update(Issue issueInstance) {
    if (issueInstance == null) {
      notFound()
      return
    }

    if (issueInstance.hasErrors()) {
      respond issueInstance.errors, view: 'edit'
      return
    }

    issueInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'Issue.label', default: 'Issue'), issueInstance.id])
        redirect issueInstance
      }
      '*' { respond issueInstance, [status: OK] }
    }
  }

  @Transactional
  def delete(Issue issueInstance) {

    if (issueInstance == null) {
      notFound()
      return
    }

    issueInstance.delete flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'Issue.label', default: 'Issue'), issueInstance.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NO_CONTENT }
    }
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'issue.label', default: 'Issue'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }
}
