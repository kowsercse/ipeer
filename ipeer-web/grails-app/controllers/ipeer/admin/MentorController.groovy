package ipeer.admin

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import grails.web.mapping.LinkGenerator
import groovy.text.SimpleTemplateEngine
import ipeer.entity.Mentor
import ipeer.entity.Role
import ipeer.service.AuthenticationTokenService
import ipeer.service.UserService
import org.apache.commons.lang.StringUtils

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
@Secured(['ROLE_MENTOR'])
class MentorController {

  static namespace = "admin"
  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  UserService userService
  AuthenticationTokenService authenticationTokenService
  def mailService
  LinkGenerator grailsLinkGenerator

  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond Mentor.list(params), model: [mentorCount: Mentor.count()]
  }

  def show(Mentor mentorInstance) {
    respond mentorInstance
  }

  def create() {
    respond new Mentor(params)
  }

  @Transactional
  def save(Mentor mentorInstance) {
    if (mentorInstance == null) {
      notFound()
      return
    }

    if (mentorInstance.hasErrors()) {
      respond mentorInstance.errors, view: 'create'
      return
    }

    if(!mentorInstance.user && StringUtils.isNotBlank(params.username)) {
      def user = userService.addUser(params.username, Role.findByAuthority(Role.ROLE_MENTOR))
      mentorInstance.user = user
    }
    mentorInstance.save flush: true

    if(mentorInstance.user.passwordExpired) {
      sendPasswordResetToken(mentorInstance)
    }

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'mentor.label', default: 'Mentor'), mentorInstance.id])
        redirect mentorInstance
      }
      '*' { respond mentorInstance, [status: CREATED] }
    }
  }

  def edit(Mentor mentorInstance) {
    respond mentorInstance
  }

  @Transactional
  def update(Mentor mentorInstance) {
    if (mentorInstance == null) {
      notFound()
      return
    }

    if (mentorInstance.hasErrors()) {
      respond mentorInstance.errors, view: 'edit'
      return
    }

    mentorInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'Mentor.label', default: 'Mentor'), mentorInstance.id])
        redirect mentorInstance
      }
      '*' { respond mentorInstance, [status: OK] }
    }
  }

  @Transactional
  def delete(Mentor mentorInstance) {

    if (mentorInstance == null) {
      notFound()
      return
    }

    mentorInstance.delete flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'Mentor.label', default: 'Mentor'), mentorInstance.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NO_CONTENT }
    }
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'mentor.label', default: 'Mentor'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }

  private void sendPasswordResetToken(Mentor mentorInstance) {
    def token = authenticationTokenService.getPasswordResetToken(mentorInstance.user)
    String mailBody = getEmailBody(mentorInstance, token)

    mailService.send(mentorInstance.email, "Reset your password", mailBody)
  }

  private String getEmailBody(Mentor mentorInstance, String token) {
    def text = 'Dear $firstName $lastName,\n' +
        '\n' +
        'Thank you for registering as a Mentor.\n' +
        '\n' +
        'Your username: $username.\n' +
        'Please set your password at $resetLink\n' +
        '\n' +
        'For more:\n' +
        'Homepage: $homeLink\n' +
        'Documentation: $documentationLink\n' +
        '\n' +
        '\n' +
        'Thank you\n' +
        'Administrator'
    def engine = new SimpleTemplateEngine()

    def template = engine.createTemplate(text).make([
        firstName        : mentorInstance.firstName,
        lastName         : mentorInstance.lastName,
        username         : mentorInstance.user.username,
        resetLink        : grailsLinkGenerator.link(namespace: "front", controller: "profile", action: "reset", absolute: true, params: [token: token]),
        homeLink         : grailsLinkGenerator.link(controller: "home", action: "index", absolute: true),
        documentationLink: grailsLinkGenerator.link(controller: "documentation", action: "index", absolute: true)
    ])
    template.toString()
  }
}
