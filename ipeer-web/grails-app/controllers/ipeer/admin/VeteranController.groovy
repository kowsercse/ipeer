package ipeer.admin

import grails.plugin.springsecurity.annotation.Secured
import ipeer.entity.Role
import ipeer.entity.User
import ipeer.entity.Veteran

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class VeteranController {

  static namespace = "admin"
  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
  
  def veteranService
  def springSecurityService

  @Secured(['ROLE_MENTOR'])
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    User user = springSecurityService.currentUser as User
    def veterans = user.mentor ? Veteran.where {mentor == user.mentor}.list() : Veteran.list(params);
    respond veterans, model: [veteranCount: Veteran.count()]
  }

  @Secured(['ROLE_MENTOR'])
  def show(Veteran veteranInstance) {
    respond veteranInstance
  }

  @Secured(['ROLE_MENTOR'])
  def create() {
    respond new Veteran(params)
  }

  @Transactional
  @Secured(['ROLE_MENTOR'])
  def save(Veteran veteranInstance) {
    if (veteranInstance == null) {
      notFound()
      return
    }

    if (veteranInstance.hasErrors()) {
      respond veteranInstance.errors, view: 'create'
      return
    }

    veteranInstance = veteranService.addVeteran(veteranInstance.mentor, veteranInstance.startDate, veteranInstance.user, veteranInstance.redCapUserId)

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'Veteran.label', default: 'Veteran'), veteranInstance.id])
        redirect veteranInstance
      }
      '*' { respond veteranInstance, [status: CREATED] }
    }
  }

  @Secured(['ROLE_MENTOR'])
  def edit(Veteran veteranInstance) {
    respond veteranInstance
  }

  @Transactional
  @Secured(['ROLE_MENTOR'])
  def update(Veteran veteranInstance) {
    if (veteranInstance == null) {
      notFound()
      return
    }

    if (veteranInstance.hasErrors()) {
      respond veteranInstance.errors, view: 'edit'
      return
    }

    veteranInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'Veteran.label', default: 'Veteran'), veteranInstance.id])
        redirect veteranInstance
      }
      '*' { respond veteranInstance, [status: OK] }
    }
  }

  @Transactional
  def delete(Veteran veteranInstance) {

    if (veteranInstance == null) {
      notFound()
      return
    }

    veteranInstance.delete flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'Veteran.label', default: 'Veteran'), veteranInstance.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NO_CONTENT }
    }
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'Veteran.label', default: 'Veteran'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }
}
