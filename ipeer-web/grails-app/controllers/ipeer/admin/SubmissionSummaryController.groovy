package ipeer.admin

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import ipeer.entity.SubmissionSummary

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class SubmissionSummaryController {

  static namespace = "admin"
  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  @Secured(['ROLE_SUPER_ADMIN'])
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond SubmissionSummary.list(params), model: [submissionSummaryCount: SubmissionSummary.count()]
  }

  @Secured(['ROLE_MENTOR'])
  def show(SubmissionSummary submissionSummaryInstance) {
    respond submissionSummaryInstance
  }

  def create() {
    respond new SubmissionSummary(params)
  }

  @Transactional
  def save(SubmissionSummary submissionSummaryInstance) {
    if (submissionSummaryInstance == null) {
      notFound()
      return
    }

    if (submissionSummaryInstance.hasErrors()) {
      respond submissionSummaryInstance.errors, view: 'create'
      return
    }

    submissionSummaryInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'submissionSummary.label', default: 'SubmissionSummary'), submissionSummaryInstance.id])
        redirect submissionSummaryInstance
      }
      '*' { respond submissionSummaryInstance, [status: CREATED] }
    }
  }

  def edit(SubmissionSummary submissionSummaryInstance) {
    respond submissionSummaryInstance
  }

  @Transactional
  def update(SubmissionSummary submissionSummaryInstance) {
    if (submissionSummaryInstance == null) {
      notFound()
      return
    }

    if (submissionSummaryInstance.hasErrors()) {
      respond submissionSummaryInstance.errors, view: 'edit'
      return
    }

    submissionSummaryInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'SubmissionSummary.label', default: 'SubmissionSummary'), submissionSummaryInstance.id])
        redirect submissionSummaryInstance
      }
      '*' { respond submissionSummaryInstance, [status: OK] }
    }
  }

  @Transactional
  def delete(SubmissionSummary submissionSummaryInstance) {

    if (submissionSummaryInstance == null) {
      notFound()
      return
    }

    submissionSummaryInstance.delete flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'SubmissionSummary.label', default: 'SubmissionSummary'), submissionSummaryInstance.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NO_CONTENT }
    }
  }

  def answers(SubmissionSummary submissionSummaryInstance) {

  }

    protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'submissionSummary.label', default: 'SubmissionSummary'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }
}
