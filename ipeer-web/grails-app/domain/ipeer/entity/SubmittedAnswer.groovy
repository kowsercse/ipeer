package ipeer.entity

class SubmittedAnswer {

  Question question
  Choice choice
  SubmissionSummary submissionSummary;

  static constraints = {
    question nullable: false
    choice nullable: false
    submissionSummary nullable: false, unique: 'question'
  }

}
