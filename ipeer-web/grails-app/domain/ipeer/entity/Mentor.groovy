package ipeer.entity

class Mentor {

  String firstName
  String lastName
  String email
  String phone
  User user

  static hasMany = [veterans: Veteran]

  static mapping = {
    sort 'firstName'
  }

  static constraints = {
    firstName blank: false
    lastName blank: false
    phone blank: false
    email blank: false
    user nullable: true, unique: true
  }

  @Override
  String toString() {
    lastName + ", " + firstName;
  }

}
