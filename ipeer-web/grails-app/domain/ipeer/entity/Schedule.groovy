package ipeer.entity

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode(includes='id')
class Schedule {

  Date start
  Date end
  Survey survey

  static constraints = {
    start nullable: false
    end nullable: false
    survey nullable: false
  }

  @Override
  public String toString() {
    return start.format("dd MMM, yy") + " - " + end.format("dd MMM, yy");
  }
}
