package ipeer.entity

class Version {

  String major
  String minor
  String buildNumber
  Boolean enabled
  Date releaseDate

  static constraints = {
    major maxSize: 128
    minor maxSize: 128
    buildNumber maxSize: 128, unique: true
    releaseDate nullable: false
  }

  @Override
  public String toString() {
    "${major}.${minor}.${buildNumber}"
  }

}
