package ipeer.entity

class SubmissionSummary {

  Date submissionTime
  Integer score
  Integer progressDirection
  Schedule schedule
  Veteran veteran
  Mentor submittedBy

  static hasMany = [submittedAnswers: SubmittedAnswer]

  static constraints = {
    schedule nullable: false, unique: 'veteran'
    veteran nullable: false
    submissionTime nullable: false
    score nullable: false
    progressDirection nullable: false
    submittedBy nullable: true
  }

}
