package ipeer.entity

class Question {

  String title
  static belongsTo = [Survey]
  static hasMany = [surveys:Survey, choices: Choice]

  static constraints = {
    title blank: false
  }

  @Override
  String toString() {
    title
  }

}
