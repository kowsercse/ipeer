package ipeer.entity

class Choice {

  String label
  Integer value

  static belongsTo = [Question]
  static hasMany = [questions: Question]

  static constraints = {
    label nullable: false
    value nullable: false
  }

  @Override
  String toString() {
    label
  }
}
