package ipeer.entity

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode(includes='authority')
@ToString(includes='authority', includeNames=true, includePackage=false)
class Role implements Serializable {

	static final ROLE_MENTOR = "ROLE_MENTOR"
	static final ROLE_VETERAN = "ROLE_VETERAN"
	static final ROLE_SUPER_ADMIN = "ROLE_SUPER_ADMIN"

	private static final long serialVersionUID = 1

	String authority

	Role(String authority) {
		this()
		this.authority = authority
	}

	static constraints = {
		authority blank: false, unique: true
	}

	static mapping = {
		cache true
	}
}
