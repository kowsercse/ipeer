package ipeer.entity

class PasswordRefreshToken {

  User user;
  String token
  Date dateCreated

  def beforeInsert() {
    dateCreated = new Date();
  }

  static constraints = {
    token unique: true
  }

}
