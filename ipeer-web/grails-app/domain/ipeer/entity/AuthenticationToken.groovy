package ipeer.entity

class AuthenticationToken {

  String tokenValue;
  String username;

  static constraints = {
    tokenValue nullable: false
    username nullable: false
  }

}
