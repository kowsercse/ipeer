package ipeer.entity

class Veteran {

  Mentor mentor
  Date startDate
  User user
  String redCapUserId;

  static hasMany = [submissionSummaries: SubmissionSummary]

  static mapping = {
    sort 'mentor'
  }

  static constraints = {
    user nullable: true, unique: true
    mentor nullable: true
    startDate nullable: false
    redCapUserId nullable: true, unique: true
  }

  @Override
  String toString() {
    id
  }

}
