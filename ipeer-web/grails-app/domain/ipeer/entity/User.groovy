package ipeer.entity

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode(includes='username')
class User implements Serializable {

	private static final long serialVersionUID = 1

	transient springSecurityService

	String username
	String password
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	Date tokenGenerationTime
	Date dateCreated
	Date lastUpdated

  static hasOne = [veteran: Veteran, mentor: Mentor, passwordResetToken: PasswordResetToken, passwordRefreshToken: PasswordRefreshToken]

  User(String username, String password) {
		this()
		this.username = username
		this.password = password
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this)*.role
	}

	def beforeInsert() {
    dateCreated = new Date();
    lastUpdated = new Date();
		encodePassword()
	}

	def beforeUpdate() {
    lastUpdated = new Date();
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}

	static transients = ['springSecurityService']

	static constraints = {
		password blank: false, password: true
		username blank: false, unique: true
    tokenGenerationTime nullable: true
    veteran nullable: true, unique: true
    mentor nullable: true, unique: true
    passwordResetToken nullable: true, unique: true
		passwordRefreshToken nullable: true, unique: true
	}

	static mapping = {
		password column: '`password`'
	}


	@Override
	public String toString() {
		username
	}
}
