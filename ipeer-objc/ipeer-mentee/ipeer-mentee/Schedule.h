//
//  Schedule.h
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "JsonDomain.h"

@protocol Schedule
@end

@interface Schedule : JsonDomain

@property(nonatomic) NSDate *start;
@property(nonatomic) NSDate *end;

- (bool) isActiveNow;
- (bool) isScheduledFuture;
- (bool) isScheduledPast;
- (Link *) getSurveyLink;

@end
