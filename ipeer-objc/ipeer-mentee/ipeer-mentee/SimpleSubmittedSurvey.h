//
//  SimpleSubmittedSurvey.h
//  ipeer-mentee
//
//  Created by Kowser on 10/14/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Schedule.h"
#import "SelectedAnswer.h"

@interface SimpleSubmittedSurvey : JSONModel

- (instancetype)initWithSchedule:(Schedule *) theSchedule;

@property(nonatomic) int schedule;
@property(nonatomic) NSMutableArray<SelectedAnswer> *submittedAnswers;

@end
