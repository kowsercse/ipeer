//
//  Mentor.m
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "Mentor.h"

@implementation Mentor

- (Link *)getVeteransLink
{
  return [self getLink:VETERAN];
}

@end
