//
//  SelectedAnswer.h
//  ipeer-mentee
//
//  Created by Kowser on 10/14/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Question.h"
#import "Choice.h"

@protocol SelectedAnswer
@end

@interface SelectedAnswer : JSONModel

@property(nonatomic) int question;
@property(nonatomic) int choice;

- (instancetype)initWithQuestion:(Question *) question andChoice: (Choice *) choice;

@end
