//
//  Veteran.h
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "JsonDomain.h"

@interface Veteran : JsonDomain

@property(nonatomic) NSDate *startDate;

- (Link *)getMentorLink;
- (Link *)getSchedulesLink;
- (Link *)getSubmissionSummariesLink;
- (Link *)getTokenLink;
- (Link *)getProfileLink;

@end
