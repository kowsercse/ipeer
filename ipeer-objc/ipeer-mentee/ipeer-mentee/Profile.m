//
//  Profile.m
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "Profile.h"

@implementation Profile

- (Link *)getMentorLink
{
  return [self getLink:MENTOR];
}

- (Link *)getVeteranLink
{
  return [self getLink:VETERAN];
}

@end
