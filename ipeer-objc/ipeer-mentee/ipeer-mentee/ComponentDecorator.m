//
//  ComponentDecorator.m
//  ipeer-mentee
//
//  Created by Kowser on 10/15/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "ComponentDecorator.h"

@implementation ComponentDecorator

+ (void)initialize {
  if(!olive1) {
    olive1 = [UIColor colorWithRed:(129/255.0f) green:(155/255.0f) blue:(60/255.0f) alpha:1];
  }
  if(!olive2) {
    olive2 = [UIColor colorWithRed:(86/255.0f) green:(100/255.0f) blue:(44/255.0f) alpha:1];
  }
  if(!gray1) {
    gray1 = [UIColor colorWithRed:(246/255.0f) green:(247/255.0f) blue:(248/255.0f) alpha:1];
  }
  if(!gray2) {
    gray2 = [UIColor colorWithRed:(213/255.0f) green:(213/255.0f) blue:(213/255.0f) alpha:1];
  }
  if(!red1) {
    red1 = [UIColor colorWithRed:(213/255.0f) green:(10/255.0f) blue:(10/255.0f) alpha:1];
  }
}

+ (void)decorateButton:(UIButton *)button
{
  button.layer.cornerRadius = 3;
  button.clipsToBounds = YES;
  [button setBackgroundColor:button.enabled ? olive1 : gray2];
  [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

+ (void)decorateLabel:(UIView *)label
{
  label.backgroundColor = olive1;
  label.layer.cornerRadius = 3;
  label.layer.masksToBounds = YES;
}

+ (MBProgressHUD *)createToast:(NSString *)message forError:(bool)isError withView: (UIView *)view
{
  MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];

  hud.mode = MBProgressHUDModeText;
  hud.labelText = message;
  hud.margin = 10.f;
  hud.yOffset = 150.f;
  hud.removeFromSuperViewOnHide = YES;
  hud.color = isError ? red1 : olive1;

  return hud;
}
@end
