//
//  Profile.h
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "JsonDomain.h"

@interface Profile : JsonDomain

@property(nonatomic) NSString *username;

- (Link *)getMentorLink;

- (Link *)getVeteranLink;

@end
