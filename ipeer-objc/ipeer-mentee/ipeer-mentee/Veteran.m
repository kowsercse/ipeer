//
//  Veteran.m
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "Veteran.h"

@implementation Veteran

- (Link *)getMentorLink
{
  return [self getLink:MENTOR];
}

- (Link *)getSchedulesLink
{
  return [self getLink:SCHEDULE];
}

- (Link *)getSubmissionSummariesLink
{
  return [self getLink:SUBMISSION_SUMMARY];
}

- (Link *)getTokenLink
{
  return [self getLink:TOKEN];
}

- (Link *)getProfileLink
{
  return [self getLink:PROFILE];
}

@end
