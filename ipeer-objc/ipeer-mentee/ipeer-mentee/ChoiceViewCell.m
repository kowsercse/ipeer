//
//  ChoiceViewCell.m
//  ipeer-mentee
//
//  Created by Kowser on 10/15/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "ChoiceViewCell.h"

@implementation ChoiceViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
