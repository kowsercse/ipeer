//
//  Schedule.m
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "Schedule.h"

@implementation Schedule

- (bool)isActiveNow
{
  return ![self isScheduledFuture] && ![self isScheduledPast];
}

- (bool)isScheduledFuture
{
  NSDate * now = [NSDate date];
  return [_start compare:now] != NSOrderedAscending;
}

- (bool)isScheduledPast
{
  NSDate *now = [NSDate date];
  return [_end compare:now] != NSOrderedDescending;
}

- (Link *)getSurveyLink
{
  return [self getLink:SURVEY];
}

@end
