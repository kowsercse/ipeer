//
//  QuestionAnswerHolder.h
//  ipeer-mentee
//
//  Created by Kowser on 10/14/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Question.h"
#import "Choice.h"

@protocol QuestionAnswerHolder
@end

@interface QuestionAnswerHolder : NSObject

@property(nonatomic, strong) Question *question;
@property(nonatomic, strong) NSArray <Choice> *choices;
@property(nonatomic, strong) Choice *selectedChoice;

@end
