//
//  Endpoint.m
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "Endpoint.h"

@implementation Endpoint

- (Link *)getProfileLink
{
  return [self getLink:PROFILE];
}

- (Link *)getSurveysLink
{
  return [self getLink:SURVEY];
}

- (Link *)getQuestionsLink
{
  return [self getLink:QUESTION];
}

- (Link *)getChoicesLink
{
  return [self getLink:CHOICE];
}

- (Link *)getVersionLink
{
  return nil;
}

@end
