//
//  ScheduleTableViewCell.h
//  ipeer-mentee
//
//  Created by Kowser on 10/8/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *weekDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIButton *checkInButton;

@end
