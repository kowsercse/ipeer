//
//  Question.m
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "Question.h"

@implementation Question

- (Link *)getChoicesLink
{
  return [self getLink:CHOICE];
}

@end
