//
//  Mentor.h
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "JsonDomain.h"

@interface Mentor : JsonDomain

@property(nonatomic) NSString *firstName;
@property(nonatomic) NSString *lastName;
@property(nonatomic) NSString *phone;
@property(nonatomic) NSString *email;

- (Link *)getVeteransLink;

@end
