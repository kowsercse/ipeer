//
//  Client.h
//  ipeer-mentee
//
//  Created by Kowser on 10/3/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Link.h"
#import "Endpoint.h"
#import "Profile.h"
#import "Mentor.h"
#import "Veteran.h"
#import "Schedule.h"
#import "Survey.h"
#import "Question.h"
#import "Choice.h"
#import "SubmissionSummary.h"
#import "SimpleSubmittedSurvey.h"

@interface Client : NSObject

- (void)setAuthenticationHeaderWithUsername:(NSString *)username password:(NSString *)password;

- (void)getEndpoint:(NSURL *)endpointUri onSuccess:(void (^)(Endpoint *))success onFailure:(void (^)(NSError *))failure;

- (void)getProfile:(Link*) profileLink onSuccess:(void (^)(Profile *))success;

- (void)getMentor:(Link*) mentorLink onSuccess:(void (^)(Mentor *))success;

- (void)getVeteran:(Link*) veteranLink onSuccess:(void (^)(Veteran *))success;

- (void)getSurveys:(Link*) surveysLink onSuccess:(void (^)(NSArray<Survey> *))success;

- (void)getQuestions:(Link*) questionsLink onSuccess:(void (^)(NSArray<Question> *))success;

- (void)getChoices:(Link*) choicesLink onSuccess:(void (^)(NSArray<Choice> *))success;

- (void)getSchedules:(Link*) schedulesLink onSuccess:(void (^)(NSArray<Schedule> *))success;

- (void)getSubmissionSummaries:(Link*) submissionSummariesLink onSuccess:(void (^)(NSArray<SubmissionSummary> *))success onFailure:(void (^)(NSError *))failure;

- (void)postSubmittedSurvey:(Link *) submissionSummariesLink withSubmittedSurvey:(SimpleSubmittedSurvey *) submittedSurvey onSuccess:(void (^)(SubmissionSummary *))SubmissionSummary onFailure:(void (^)(NSError *))failure;

@end
