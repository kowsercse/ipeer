//
//  Client.m
//  ipeer-mentee
//
//  Created by Kowser on 10/3/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "Client.h"
#import "AFHTTPRequestOperationManager.h"

@implementation Client {
  AFHTTPRequestOperationManager *_manager;
  void (^_defaultFailure)(NSError *);
}

- (id)init
{
  self = [super init];
  if (self) {
    _manager = [AFHTTPRequestOperationManager manager];
    _defaultFailure = ^(NSError *error) {
        NSLog(@"result %@", error);
    };
  }
  return self;
}

- (void)setAuthenticationHeaderWithUsername:(NSString *)username password:(NSString *)password
{
  _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
  [_manager.requestSerializer setAuthorizationHeaderFieldWithUsername:username password:password];
}

- (void)getEndpoint:(NSURL *)endpointUri onSuccess:(void (^)(Endpoint *))success onFailure:(void (^)(NSError *))failure
{
  void (^onSuccess)(id) = ^(id responseObject) {
      NSError *error = nil;
      Endpoint *endpoint = [[Endpoint alloc] initWithDictionary:responseObject error:&error];
      if (error == nil) {
        success(endpoint);
      }
      else {
        failure(error);
      }
  };
  [self getFromUri:endpointUri.absoluteString onSuccess:onSuccess onFailure:failure];
}

- (void)getProfile:(Link *)profileLink onSuccess:(void (^)(Profile *))success
{
  void (^onSuccess)(id) = ^(id responseObject) {
      NSError *error = nil;
      Profile *profile = [[Profile alloc] initWithDictionary:responseObject error:&error];
      if (error == nil) {
        success(profile);
      }
      else {
        _defaultFailure(error);
      }
  };
  [self getFromLink:profileLink onSuccess:onSuccess onFailure:_defaultFailure];
}

- (void)getMentor:(Link *)mentorLink onSuccess:(void (^)(Mentor *))success
{
  void (^onSuccess)(id) = ^(id responseObject) {
      NSError *error = nil;
      Mentor *mentor = [[Mentor alloc] initWithDictionary:responseObject error:&error];
      if (error == nil) {
        success(mentor);
      }
      else {
        _defaultFailure(error);
      }
  };
  [self getFromLink:mentorLink onSuccess:onSuccess onFailure:_defaultFailure];
}

- (void)getVeteran:(Link *)veteranLink onSuccess:(void (^)(Veteran *))success
{
  void (^onSuccess)(id) = ^(id responseObject) {
      NSError *error = nil;
      Veteran *veteran = [[Veteran alloc] initWithDictionary:responseObject error:&error];
      if (error == nil) {
        success(veteran);
      }
      else {
        _defaultFailure(error);
      }
  };
  [self getFromLink:veteranLink onSuccess:onSuccess onFailure:_defaultFailure];
}

- (void)getSurveys:(Link *)surveysLink onSuccess:(void (^)(NSArray <Survey> *))success
{
  void (^onSuccess)(id) = ^(id responseObject) {
      NSError *error = nil;
      NSArray<Survey> *array = (id) [Survey arrayOfModelsFromDictionaries:responseObject error:&error];
      if (error == nil) {
        success(array);
      }
      else {
        _defaultFailure(error);
      }
  };
  [self getFromLink:surveysLink onSuccess:onSuccess onFailure:_defaultFailure];
}

- (void)getQuestions:(Link *)questionsLink onSuccess:(void (^)(NSArray <Question> *))success
{
  void (^onSuccess)(id) = ^(id responseObject) {
      NSError *error = nil;
      NSArray<Question> *array = (id) [Question arrayOfModelsFromDictionaries:responseObject error:&error];
      if (error == nil) {
        success(array);
      }
      else {
        _defaultFailure(error);
      }
  };
  [self getFromLink:questionsLink onSuccess:onSuccess onFailure:_defaultFailure];
}

- (void)getChoices:(Link *)choicesLink onSuccess:(void (^)(NSArray <Choice> *))success
{
  void (^onSuccess)(id) = ^(id responseObject) {
      NSError *error = nil;
      NSArray<Choice> *array = (id) [Choice arrayOfModelsFromDictionaries:responseObject error:&error];
      if (error == nil) {
        success(array);
      }
      else {
        _defaultFailure(error);
      }
  };
  [self getFromLink:choicesLink onSuccess:onSuccess onFailure:_defaultFailure];
}

- (void)getSchedules:(Link *)schedulesLink onSuccess:(void (^)(NSArray <Schedule> *))success
{
  void (^onSuccess)(id) = ^(id responseObject) {
      NSError *error = nil;
      NSArray<Schedule> *array = (id) [Schedule arrayOfModelsFromDictionaries:responseObject error:&error];
      if (error == nil) {
        success(array);
      }
      else {
        _defaultFailure(error);
      }
  };
  [self getFromLink:schedulesLink onSuccess:onSuccess onFailure:_defaultFailure];
}

- (void)getSubmissionSummaries:(Link *)submissionSummariesLink onSuccess:(void (^)(NSArray <SubmissionSummary> *))success onFailure:(void (^)(NSError *))failure
{
  void (^onSuccess)(id) = ^(id responseObject) {
      NSError *error = nil;
      NSArray<SubmissionSummary> *array = (id) [SubmissionSummary arrayOfModelsFromDictionaries:responseObject error:&error];
      if (error == nil) {
        success(array);
      }
      else {
        failure(error);
      }
  };
  [self getFromLink:submissionSummariesLink onSuccess:onSuccess onFailure:failure];
}

- (void)postSubmittedSurvey:(Link *)submissionSummariesLink withSubmittedSurvey:(SimpleSubmittedSurvey *)submittedSurvey onSuccess:(void (^)(SubmissionSummary *))success onFailure:(void (^)(NSError *))failure
{
  void (^internalSuccess)(AFHTTPRequestOperation *, id) = ^(AFHTTPRequestOperation *operation, id responseObject) {
      NSError *error = nil;
      SubmissionSummary *submissionSummary = [[SubmissionSummary alloc] initWithDictionary:responseObject error:&error];
      if (error == nil) {
        success(submissionSummary);
      }
      else {
        failure(error);
      }
  };
  void (^internalFailure)(AFHTTPRequestOperation *, NSError *) = ^(AFHTTPRequestOperation *operation, NSError *error) {
      NSLog(@"Error: %@", error);
      failure(error);
  };
  AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
  [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  _manager.requestSerializer = serializer;
  [_manager POST:submissionSummariesLink.href parameters:[submittedSurvey toDictionary] success:internalSuccess failure:internalFailure];
}

- (void)getFromLink:(Link *)link onSuccess:(void (^)(id))success onFailure:(void (^)(NSError *))failure
{
  [self getFromUri:link.href onSuccess:success onFailure:failure];
}

- (void)getFromUri:(NSString *)uri onSuccess:(void (^)(id))success onFailure:(void (^)(NSError *))failure
{
  void (^internalSuccess)(AFHTTPRequestOperation *, id) = ^(AFHTTPRequestOperation *operation, id responseObject) {
      success(responseObject);
  };
  void (^internalFailure)(AFHTTPRequestOperation *, NSError *) = ^(AFHTTPRequestOperation *operation, NSError *error) {
      NSLog(@"Error: %@", error);
      failure(error);
  };
  [_manager GET:uri parameters:nil success:internalSuccess failure:internalFailure];
}

@end
