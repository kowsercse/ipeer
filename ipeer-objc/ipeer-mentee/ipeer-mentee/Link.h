//
//  Link.h
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@protocol Link
@end

@interface Link : JSONModel

@property(nonatomic) NSString *rel;
@property(nonatomic) NSString *href;

@end
