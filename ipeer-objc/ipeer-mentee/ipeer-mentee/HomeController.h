//
//  HomeController.h
//  ipeer-mentee
//
//  Created by Kowser on 10/7/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *callMentorButton;
@property (weak, nonatomic) IBOutlet UIButton *textMentorButton;
@property (weak, nonatomic) IBOutlet UIButton *checkInButton;
@property (weak, nonatomic) IBOutlet UITableView *scheduleTableView;

@end
