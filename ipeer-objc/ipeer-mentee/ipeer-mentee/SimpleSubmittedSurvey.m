//
//  SimpleSubmittedSurvey.m
//  ipeer-mentee
//
//  Created by Kowser on 10/14/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "SimpleSubmittedSurvey.h"

@implementation SimpleSubmittedSurvey

- (instancetype)initWithSchedule:(Schedule *) theSchedule
{
  self = [super init];
  if (self) {
    NSURL *scheduleUrl = [NSURL URLWithString:[theSchedule getSelf].href];
    _schedule = [[scheduleUrl lastPathComponent] intValue];
    _submittedAnswers = (id) [NSMutableArray array];
  }

  return self;
}

@end
