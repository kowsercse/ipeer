//
//  SubmittedSurvey.h
//  ipeer-mentee
//
//  Created by Kowser on 10/8/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Schedule.h"
#import "SubmissionSummary.h"

@protocol SubmittedSurvey
@end

@interface SubmittedSurvey : NSObject

@property (nonatomic) Schedule *schedule;
@property (nonatomic) SubmissionSummary *submissionSummary;

@end
