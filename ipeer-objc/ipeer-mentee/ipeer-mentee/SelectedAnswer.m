//
//  SelectedAnswer.m
//  ipeer-mentee
//
//  Created by Kowser on 10/14/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "SelectedAnswer.h"

@implementation SelectedAnswer

- (instancetype)initWithQuestion:(Question *) theQuestion andChoice: (Choice *) theChoice
{
  self = [super init];
  if (self) {
    NSURL *questionUrl = [NSURL URLWithString:[theQuestion getSelf].href];
    NSURL *choiceUrl = [NSURL URLWithString:[theChoice getSelf].href];
    _question = [[questionUrl lastPathComponent] intValue];
    _choice = [[choiceUrl lastPathComponent] intValue];
  }

  return self;
}

@end
