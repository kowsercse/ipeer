//
//  ViewController.m
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "LoginController.h"
#import "HomeController.h"
#import "AppDelegate.h"
#import "ComponentDecorator.h"

@interface LoginController ()

@end

@implementation LoginController

- (void)viewDidLoad
{
  [super viewDidLoad];
  _usernameTextField.text = @"1002";
  _passwordTextField.text = @"Admin1";
  [ComponentDecorator decorateButton:_loginButton];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)loginAction:(id)sender
{
  AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
  Client *client = appDelegate.client;

  [client setAuthenticationHeaderWithUsername:_usernameTextField.text password:_passwordTextField.text];
  [client getEndpoint:appDelegate.endpointUrl
            onSuccess:^(Endpoint *endpoint) {
                [client getProfile:endpoint.getProfileLink onSuccess:^(Profile *profile) {
                    if (profile.getVeteranLink != nil) {
                      [client getVeteran:profile.getVeteranLink onSuccess:^(Veteran *veteran) {
                          appDelegate.veteran = veteran;
                          appDelegate.endpoint = endpoint;
                          [client getSurveys:appDelegate.endpoint.getSurveysLink onSuccess:^(NSArray <Survey> *surveys) {
                              for (Survey *survey in surveys) {
                                appDelegate.surveys[survey.getSelf.href] = survey;
                              }
                          }];
                          [client getQuestions:appDelegate.endpoint.getQuestionsLink onSuccess:^(NSArray <Question> *questions) {
                              for (Question *question in questions) {
                                appDelegate.questions[question.getSelf.href] = question;
                              }
                          }];
                          [client getChoices:appDelegate.endpoint.getChoicesLink onSuccess:^(NSArray <Choice> *choices) {
                              for (Choice *choice in choices) {
                                appDelegate.choices[choice.getSelf.href] = choice;
                              }
                          }];
                          HomeController *homeController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeController"];
                          [self presentViewController:homeController animated:NO completion:nil];
                      }];
                    }
                    else {
                      MBProgressHUD *hud = [ComponentDecorator createToast:@"Please login as a veteran" forError:true withView:self.view];
                      [hud hide:YES afterDelay:3];
                    }
                }];
            }
            onFailure:^(NSError *error) {
                NSLog(@"result %@", error);
                MBProgressHUD *hud = [ComponentDecorator createToast:@"Login failed. Please try again" forError:true withView:self.view];
                [hud hide:YES afterDelay:3];
            }];
}

@end
