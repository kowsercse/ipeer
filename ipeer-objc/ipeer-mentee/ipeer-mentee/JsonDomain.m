//
//  JsonDomain.m
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "JsonDomain.h"

@implementation JsonDomain

- (Link *)getSelf
{
  return [self getLink:SELF];
}

- (Link *)getLink:(NSString *)linkRelation
{
  for (Link *link in _links) {
    if ([link.rel caseInsensitiveCompare:linkRelation] == NSOrderedSame) {
      return link;
    }
  }
  return nil;
}

@end
