//
//  SubmissionSummary.m
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "SubmissionSummary.h"

@implementation SubmissionSummary

- (Link *)getScheduleLink
{
  return [self getLink:SCHEDULE];
}

- (Link *)getVeteranLink
{
  return [self getLink:VETERAN];
}

- (Link *)getSubmittedAnswersLink
{
  return [self getLink:SUBMITTED_ANSWER];
}

@end
