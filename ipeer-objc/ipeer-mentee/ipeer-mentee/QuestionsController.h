//
//  QuestionsController.h
//  ipeer-mentee
//
//  Created by Kowser on 10/8/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Schedule.h"

@interface QuestionsController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *surveyTitle;
@property (weak, nonatomic) IBOutlet UILabel *questionTitle;
@property (weak, nonatomic) IBOutlet UITableView *questionsTable;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@property(nonatomic, strong) Schedule *activeSchedule;
@property(nonatomic, strong) Link *submissionSummariesLink;

@end
