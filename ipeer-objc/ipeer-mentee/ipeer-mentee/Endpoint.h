//
//  Endpoint.h
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "JsonDomain.h"

@interface Endpoint : JsonDomain

- (Link *)getProfileLink;
- (Link *)getSurveysLink;
- (Link *)getQuestionsLink;
- (Link *)getChoicesLink;
- (Link *)getVersionLink;

@end
