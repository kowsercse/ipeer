//
//  Survey.m
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "Survey.h"

@implementation Survey

- (Link *)getQuestionsLink
{
  return [self getLink:QUESTION];
}

@end
