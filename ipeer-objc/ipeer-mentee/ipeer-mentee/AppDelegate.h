//
//  AppDelegate.h
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Client.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic) NSURL *homeUrl;
@property (nonatomic) NSURL *documentationUrl;
@property (nonatomic) NSURL *endpointUrl;

@property (nonatomic) Client *client;
@property (nonatomic) Endpoint *endpoint;
@property(nonatomic, strong) Veteran *veteran;

@property(nonatomic, strong) NSMutableDictionary *surveys;
@property(nonatomic, strong) NSMutableDictionary *questions;
@property(nonatomic, strong) NSMutableDictionary *choices;

@property(nonatomic) bool surveySubmitted;

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end

