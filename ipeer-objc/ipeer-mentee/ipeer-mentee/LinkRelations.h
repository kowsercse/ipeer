//
//  LinkRelations.h
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef ipeer_mentee_LinkRelations_h
#define ipeer_mentee_LinkRelations_h

static NSString *const SELF = @"self";
static NSString *const PROFILE = @"profile";
static NSString *const VETERAN = @"veteran";
static NSString *const MENTOR = @"mentor";
static NSString *const SURVEY = @"survey";
static NSString *const QUESTION = @"question";
static NSString *const CHOICE = @"choice";
static NSString *const SCHEDULE = @"schedule";
static NSString *const SUBMITTED_ANSWER = @"submittedAnswer";
static NSString *const SUBMISSION_SUMMARY = @"submissionSummary";
static NSString *const TOKEN = @"token";
static NSString *const MENTOR_APP = @"mentor-app";
static NSString *const MENTEE_APP = @"mentee-app";
static NSString *const VERSION = @"version";

#endif
