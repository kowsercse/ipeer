//
//  SubmissionSummary.h
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "JsonDomain.h"

@protocol SubmissionSummary
@end

@interface SubmissionSummary : JsonDomain

@property(nonatomic) NSInteger score;
@property(nonatomic) NSDate *submissionTime;
@property(nonatomic) NSInteger progressDirection;

- (Link *) getScheduleLink;
- (Link *) getVeteranLink;
- (Link *) getSubmittedAnswersLink;

@end
