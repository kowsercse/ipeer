//
//  ComponentDecorator.h
//  ipeer-mentee
//
//  Created by Kowser on 10/15/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

static UIColor *olive1;
static UIColor *olive2;
static UIColor *gray1;
static UIColor *gray2;
static UIColor *red1;

@interface ComponentDecorator : NSObject

+ (void)decorateButton:(UIButton *)button;
+ (void)decorateLabel:(UIView *)label;
+ (MBProgressHUD *)createToast:(NSString *)message forError:(bool)isError withView: (UIView *)view;

@end
