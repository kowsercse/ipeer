//
//  QuestionsController.m
//  ipeer-mentee
//
//  Created by Kowser on 10/8/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "AppDelegate.h"
#import "QuestionsController.h"
#import "QuestionAnswerHolder.h"
#import "ComponentDecorator.h"

@interface QuestionsController ()

@end

@implementation QuestionsController {
  NSArray *questionAnswers;
  NSInteger currentQuestionIndex;
  bool isIgnoreBackOrNext;

  Client *client;
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
  self.navigationItem.leftBarButtonItem = backButton;
}

- (void)goBack
{
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  currentQuestionIndex = -1;
  isIgnoreBackOrNext = false;
  questionAnswers = @[];

  _questionsTable.delegate = self;
  _questionsTable.dataSource = self;
  _questionsTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

  [_backButton addTarget:self action:@selector(goPrevious) forControlEvents:UIControlEventTouchUpInside];
  [_nextButton addTarget:self action:@selector(goNext) forControlEvents:UIControlEventTouchUpInside];
  [ComponentDecorator decorateButton:_backButton];
  [ComponentDecorator decorateButton:_nextButton];
  [ComponentDecorator decorateLabel:_surveyTitle];

  AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
  client = appDelegate.client;
  Survey *survey = appDelegate.surveys[_activeSchedule.getSurveyLink.href];
  _surveyTitle.text = survey.title;

  [self downloadResourcesForSurvey:survey];
}

- (void)downloadResourcesForSurvey:(const Survey *)survey
{
  NSMutableArray *questionAnswerHolders = [NSMutableArray array];
  [client getQuestions:survey.getQuestionsLink onSuccess:^(NSArray <Question> *questions) {
      for (Question *question in questions) {
        [client getChoices:question.getChoicesLink onSuccess:^(NSArray <Choice> *choices) {
            QuestionAnswerHolder *questionAnswerHolder = [[QuestionAnswerHolder alloc] init];
            questionAnswerHolder.choices = (NSArray <Choice> *) [choices sortedArrayUsingComparator:^NSComparisonResult(Choice *left, Choice *right) {
                return left.value == right.value ? NSOrderedSame : left.value < right.value ? NSOrderedAscending : NSOrderedDescending;
            }];
            questionAnswerHolder.question = question;
            questionAnswerHolder.selectedChoice = questionAnswerHolder.choices[0];

            [questionAnswerHolders addObject:questionAnswerHolder];
            if ([questionAnswerHolders count] == [questions count]) {
              questionAnswers = [questionAnswerHolders sortedArrayUsingComparator:^NSComparisonResult(QuestionAnswerHolder *left, QuestionAnswerHolder *right) {
                  return [left.question.getSelf.href caseInsensitiveCompare:right.question.getSelf.href];
              }];

              currentQuestionIndex = 0;
              [self reload];
            }
        }];
      }
  }];
}

- (void)reload
{
  NSString *nextLabel = (currentQuestionIndex + 1 == [questionAnswers count] || [questionAnswers count] == 1) ? @"Submit" : @"Next";
  [_nextButton setTitle:nextLabel forState:UIControlStateNormal];
  _backButton.enabled = !(currentQuestionIndex == 0 || [questionAnswers count] == 1);
  [ComponentDecorator decorateButton:_backButton];

  QuestionAnswerHolder *questionAnswerHolder = questionAnswers[(NSUInteger) currentQuestionIndex];
  _questionTitle.text = questionAnswerHolder.question.title;
  [_questionsTable reloadData];
}

- (void)goPrevious
{
  if (!isIgnoreBackOrNext) {
    currentQuestionIndex--;
    [self reload];
  }
}

- (void)goNext
{
  if (!isIgnoreBackOrNext) {
    if (currentQuestionIndex + 1 == [questionAnswers count]) {
      [self submitSurvey];
    } else {
      currentQuestionIndex++;
      [self reload];
    }
  }
}

- (void)submitSurvey
{
  SimpleSubmittedSurvey *submittedSurvey = [[SimpleSubmittedSurvey alloc] initWithSchedule:_activeSchedule];
  for (QuestionAnswerHolder *questionAnswer in questionAnswers) {
    SelectedAnswer *selectedAnswer = [[SelectedAnswer alloc] initWithQuestion:questionAnswer.question
                                                                    andChoice:questionAnswer.selectedChoice];
    [submittedSurvey.submittedAnswers addObject:selectedAnswer];
  }

  void (^success)(SubmissionSummary *) = ^(SubmissionSummary *summary) {
      ((AppDelegate *) [[UIApplication sharedApplication] delegate]).surveySubmitted = true;
      [self dismissViewControllerAnimated:YES completion:nil];
  };
  void (^failure)(NSError *) = ^(NSError *error) {
      MBProgressHUD *hud = [ComponentDecorator createToast:@"Failed to submit survey. Please try again." forError:true withView:self.view];
      [hud hide:YES afterDelay:3];
  };
  [client postSubmittedSurvey:_submissionSummariesLink withSubmittedSurvey:submittedSurvey onSuccess:success onFailure:failure];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  if ([questionAnswers count] == 0) {
    return 0;
  }

  QuestionAnswerHolder *questionAnswer = questionAnswers[(NSUInteger) currentQuestionIndex];
  return [questionAnswer.choices count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *simpleTableIdentifier = @"ChoiceViewCell";

  UITableViewCell *cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
  if (cell == nil) {
    cell = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil][0];
  }
  QuestionAnswerHolder *questionAnswer = questionAnswers[(NSUInteger) currentQuestionIndex];
  Choice *choice = questionAnswer.choices[(NSUInteger) indexPath.row];
  cell.textLabel.text = choice.label;
  cell.accessoryType = choice == questionAnswer.selectedChoice ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;

  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  QuestionAnswerHolder *questionAnswer = questionAnswers[(NSUInteger) currentQuestionIndex];
  questionAnswer.selectedChoice = questionAnswer.choices[(NSUInteger) indexPath.row];
  [_questionsTable reloadData];

  isIgnoreBackOrNext = true;
  [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(goNextOnSelection:) userInfo:nil repeats:NO];
}

- (void)goNextOnSelection:(id)goNextOnSelection
{
  isIgnoreBackOrNext = false;
  [self goNext];
}

@end
