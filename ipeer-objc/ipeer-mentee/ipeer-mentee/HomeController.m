//
//  HomeController.m
//  ipeer-mentee
//
//  Created by Kowser on 10/7/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "HomeController.h"
#import "AppDelegate.h"
#import "SubmittedSurvey.h"
#import "ScheduleTableViewCell.h"
#import "QuestionsController.h"
#import "ComponentDecorator.h"

@interface HomeController ()

@end

@implementation HomeController {
  Mentor *_mentor;
  Schedule *_activeSchedule;
  NSArray *_submittedSurveys;
  Veteran *_veteran;
  NSArray <Schedule> *_schedules;

  Client *_client;
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];

  if(((AppDelegate *) [[UIApplication sharedApplication] delegate]).surveySubmitted) {
    MBProgressHUD *hud = [ComponentDecorator createToast:@"Survey is submitted successfully" forError:false withView:self.view];
    [hud hide:YES afterDelay:3];

    void (^success)(NSArray <SubmissionSummary> *) = ^(NSArray <SubmissionSummary> *submissionSummaries) {
        _submittedSurveys = [self getSubmittedSurveys:submissionSummaries withSchedules:_schedules];
        [_scheduleTableView reloadData];

        _activeSchedule = [self getActiveScheduleFrom:_submittedSurveys];
        if (_activeSchedule != nil) {
          _checkInButton.enabled = true;
          [ComponentDecorator decorateButton:_checkInButton];
          [_checkInButton addTarget:self action:@selector(showActiveSurveyQuestions) forControlEvents:UIControlEventTouchUpInside];
        }
        ((AppDelegate *) [[UIApplication sharedApplication] delegate]).surveySubmitted = false;
    };
    [_client getSubmissionSummaries:_veteran.getSubmissionSummariesLink onSuccess:success onFailure:^(NSError *error) {
        NSLog(@"Failed to download submission summaries %@", error);
    }];
  }
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  _submittedSurveys = @[];
  _scheduleTableView.delegate = self;
  _scheduleTableView.dataSource = self;
  _scheduleTableView.allowsSelection = NO;
  _scheduleTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
  _checkInButton.enabled = false;
  [ComponentDecorator decorateButton:_checkInButton];
  [ComponentDecorator decorateButton:_callMentorButton];
  [ComponentDecorator decorateButton:_textMentorButton];

  AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];

  _client = appDelegate.client;
  _veteran = appDelegate.veteran;
  [_client getMentor:_veteran.getMentorLink onSuccess:^(Mentor *mentor) {
      _mentor = mentor;
      [_callMentorButton addTarget:self action:@selector(callMentorAction) forControlEvents:UIControlEventTouchUpInside];
      [_textMentorButton addTarget:self action:@selector(textMentorAction) forControlEvents:UIControlEventTouchUpInside];
  }];
  [_client getSchedules:_veteran.getSchedulesLink onSuccess:^(NSArray <Schedule> *schedules) {
      _schedules = schedules;
      [_scheduleTableView reloadData];
      void (^success)(NSArray <SubmissionSummary> *) = ^(NSArray <SubmissionSummary> *submissionSummaries) {
          _submittedSurveys = [self getSubmittedSurveys:submissionSummaries withSchedules:schedules];
          [_scheduleTableView reloadData];

          _activeSchedule = [self getActiveScheduleFrom:_submittedSurveys];
          if (_activeSchedule != nil) {
            _checkInButton.enabled = true;
            [ComponentDecorator decorateButton:_checkInButton];
            [_checkInButton addTarget:self action:@selector(showActiveSurveyQuestions) forControlEvents:UIControlEventTouchUpInside];
          }
      };
      [_client getSubmissionSummaries:_veteran.getSubmissionSummariesLink onSuccess:success onFailure:^(NSError *error) {
          NSLog(@"Failed to download submission summaries %@", error);
      }];
  }];
}

- (void)showActiveSurveyQuestions
{
  QuestionsController *questionsController = [self.storyboard instantiateViewControllerWithIdentifier:@"QuestionsController"];
  questionsController.activeSchedule = _activeSchedule;
  questionsController.submissionSummariesLink = [_veteran getSubmissionSummariesLink];

  UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:questionsController];
  [self presentViewController:navigationController animated:YES completion:nil];
}

- (Schedule *)getActiveScheduleFrom:(NSArray *)submittedSurveys
{
  for (SubmittedSurvey *submittedSurvey in submittedSurveys) {
    if (submittedSurvey.schedule.isActiveNow && submittedSurvey.submissionSummary == nil) {
      return submittedSurvey.schedule;
    }
  }

  return nil;
}

- (NSArray <SubmittedSurvey> *)getSubmittedSurveys:(NSArray <SubmissionSummary> *)submissionSummaries withSchedules:(NSArray <Schedule> *)schedules
{
  NSMutableDictionary *submissionSummaryMap = [[NSMutableDictionary alloc] init];
  for (SubmissionSummary *submissionSummary in submissionSummaries) {
    submissionSummaryMap[submissionSummary.getScheduleLink.href] = submissionSummary;
  }
  NSMutableArray *submittedSurveys = [[NSMutableArray alloc] init];
  for (Schedule *schedule in schedules) {
    SubmittedSurvey *submittedSurvey = [[SubmittedSurvey alloc] init];
    submittedSurvey.schedule = schedule;
    submittedSurvey.submissionSummary = submissionSummaryMap[schedule.getSelf.href];
    [submittedSurveys addObject:submittedSurvey];
  }

  return (NSArray <SubmittedSurvey> *) [submittedSurveys sortedArrayUsingComparator:^NSComparisonResult(SubmittedSurvey *left, SubmittedSurvey *right) {
      return [right.schedule.start compare:left.schedule.start];
  }];
}

- (void)callMentorAction
{
  NSString *phoneNumber = [@"telprompt://" stringByAppendingString:_mentor.phone];
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

- (void)textMentorAction
{
  NSString *phoneNumber = [@"sms://" stringByAppendingString:_mentor.phone];
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [_submittedSurveys count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *simpleTableIdentifier = @"ScheduleTableViewCell";

  ScheduleTableViewCell *cell = (ScheduleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
  if (cell == nil) {
    cell = [[NSBundle mainBundle] loadNibNamed:@"ScheduleTableViewCell" owner:self options:nil][0];
  }
  SubmittedSurvey *submittedSurvey  = _submittedSurveys[(NSUInteger) indexPath.row];

  NSDateFormatter *weekFormatter = [[NSDateFormatter alloc] init];
  [weekFormatter setDateFormat: @"EEEE"];
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat: @"MMM, dd"];

  cell.weekDayLabel.text = [weekFormatter stringFromDate:submittedSurvey.schedule.start];
  cell.dateLabel.text = [dateFormatter stringFromDate:submittedSurvey.schedule.end];
  if(submittedSurvey.schedule.isActiveNow && submittedSurvey.submissionSummary == nil) {
    [cell.checkInButton addTarget:self action:@selector(showActiveSurveyQuestions) forControlEvents:UIControlEventTouchUpInside];
    cell.checkInButton.enabled = true;
    [cell.checkInButton setTitle:@"Available Now" forState:UIControlStateNormal];
  }
  else if(submittedSurvey.schedule.isScheduledFuture) {
    cell.checkInButton.enabled = false;
    [cell.checkInButton setTitle:@"Coming Soon" forState:UIControlStateNormal];
  }
  else if(submittedSurvey.submissionSummary != nil) {
    cell.checkInButton.enabled = false;
    [cell.checkInButton setTitle:@"Completed" forState:UIControlStateNormal];
  }
  else {
    cell.checkInButton.enabled = false;
    [cell.checkInButton setTitle:@"Missed" forState:UIControlStateNormal];
  }
  [ComponentDecorator decorateButton:cell.checkInButton];

  return cell;
}

@end
