//
//  Choice.h
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "JsonDomain.h"

@protocol Choice
@end

@interface Choice : JsonDomain

@property(nonatomic) NSString *label;
@property(nonatomic) int value;

@end
