//
//  JsonDomain.h
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LinkRelations.h"
#import "Link.h"
#import "JSONModel.h"

@interface JsonDomain : JSONModel

@property (nonatomic) NSArray<Link> *links;

- (Link *)getSelf;
- (Link *)getLink:(NSString *)linkRelation;

@end
