//
//  Survey.h
//  ipeer-mentee
//
//  Created by Kowser on 10/1/15.
//  Copyright (c) 2015 Ubicomp Lab, MSCS, Marquette University. All rights reserved.
//

#import "JsonDomain.h"

@protocol Survey
@end

@interface Survey : JsonDomain

@property(nonatomic) NSString *title;
@property(nonatomic) BOOL active;

- (Link *)getQuestionsLink;

@end
