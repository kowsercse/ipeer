dataSource {
  dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
  url = "" // jdbc:mysql://localhost/database
  username = ""
  password = ""
  pooled = true
  driverClassName = "com.mysql.jdbc.Driver"
  properties {
    jmxEnabled = true
    initialSize = 5
    maxActive = 50
    minIdle = 5
    maxIdle = 25
    maxWait = 10 * 1000
    maxAge = 10 * 60 * 1000
    timeBetweenEvictionRunsMillis = 5000
    minEvictableIdleTimeMillis = 60000
    validationQuery = "SELECT 1"
    validationQueryTimeout = 3
    validationInterval = 15000
    testOnBorrow = true
    testWhileIdle = true
    testOnReturn = true
    // controls for leaked connections
    abandonWhenPercentageFull = 100 // settings are active only when pool is full
    removeAbandonedTimeout = 120
    removeAbandoned = true
    // logAbandoned = false // causes stacktrace recording overhead, use only for debugging

    // JDBC driver properties
    dbProperties {
      autoReconnect=true
      zeroDateTimeBehavior='convertToNull'
      // timeouts for TCP/IP
      connectTimeout=15 * 1000
      socketTimeout=120 * 1000
    }
  }
}

log4j = {
  debug 'org.springframework.security'
  debug 'liquibase'
}

admin.username=""
admin.password=""
version.storage.path=""

grails {
  mail {
    host = ""
    port =
    username = ""
    password = ""
    props = [
        "mail.smtp.starttls.enable": true,
        "mail.smtp.port": 25,
        "mail.smtp.auth": true
    ]
  }
}
grails.mail.default.from=""
