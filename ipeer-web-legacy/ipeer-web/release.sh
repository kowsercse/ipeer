#!/usr/bin/env bash

# we would pass the property name and property file name as a parameter to this function
#from where we want to read the property
# @param propertyName
# @param fileName
function getPropertyFromFile()
{
  propertyName=$(echo $2 | sed -e 's/\./\\\./g')
  fileName=$2;
  cat $1 | sed -n -e "s/^[ ]*//g;/^#/d;s/^$propertyName=//p" | tail -1
}

version_major=1
version_minor=1
version_build=$(date +"%Y%m%d%H%M%S")
version=$version_major.$version_minor.$version_build

grails_version=$(getPropertyFromFile application.properties app.grails.version)
app_name=$(getPropertyFromFile application.properties app.name)

echo -e "app.grails.version=${grails_version}\napp.name=${app_name}\napp.version=$version" > application.properties
