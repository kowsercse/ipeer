<% import grails.persistence.Event %>
<%=packageName%>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-desktop  mdl-cell--8-col-tablet  mdl-cell--6-col-phone">
  <h1><g:message code="default.list.label" args="[entityName]"/></h1>
  <g:if test="\${flash.message}">
    <div class="message" role="status">\${flash.message}</div>
  </g:if>

  <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
    <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="${domainClass.propertyName}.id.label" default="#"/>
      </th>
      <% excludedProps = Event.allEvents.toList() << 'id' << 'version'
      allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
      props = domainClass.properties.findAll {
        allowedNames.contains(it.name) && !excludedProps.contains(it.name) && it.type != null && !Collection.isAssignableFrom(it.type) && (domainClass.constrainedProperties[it.name] ? domainClass.constrainedProperties[it.name].display : true)
      }
      Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
      props.eachWithIndex { p, i ->
        if (i < 6) {
          if (p.isAssociation()) { %>
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}"/>
      </th>
      <% } else { %>
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="${p.name}"
                        title="\${message(code: '${domainClass.propertyName}.${p.name}.label', default: '${
                            p.naturalName}')}"/>
      <% }
      }
      } %>
      <th class="mdl-data-table__cell--non-numeric">Action</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="\${${propertyName}List}" status="i" var="${propertyName}">
      <tr>
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="\${${propertyName}.id}">\${${propertyName}.id}</g:link>
        </td>
        <% props.eachWithIndex { p, i -> %>
        <td class="mdl-data-table__cell--non-numeric">
        <% if (i < 6) { %>
          <% if (p.type == Boolean || p.type == boolean) { %>
          <g:formatBoolean boolean="\${${propertyName}.${p.name}}"/>
          <% } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
          <g:formatDate date="\${${propertyName}.${p.name}}"/>
          <% } else if (p.isAssociation()) { %>
            <% if(p.isOptional()) { %><g:if test="\${${propertyName}.${p.name} != null}"><% } %>
            <g:link namespace="admin" controller="\${${propertyName}.${p.name}.adminController}" action="show" id="\${${propertyName}.${p.name}.id}">\${${propertyName}.${p.name}}</g:link>
            <% if(p.isOptional()) { %></g:if><% } %>
          <% } else { %>
          \${fieldValue(bean: ${propertyName}, field: "${p.name}")}
          <% }
          } %>
        </td>
        <% } %>
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="\${${propertyName}.id}"><i class="mdl-color-text--blue-grey-400 material-icons">description</i></g:link>
          <g:link namespace="admin" action="edit" id="\${${propertyName}.id}"><i class="mdl-color-text--blue-grey-400 material-icons">edit</i></g:link>
        </td>
      </tr>
    </g:each>
    </tbody>
  </table>

  <div class="mdl-card__actions pagination">
    <g:link action="create" namespace="admin"
            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast">
      <g:message code="default.new.label" args="[entityName]"/>
    </g:link>
    <g:paginate namespace="admin" action="index" total="\${${propertyName}Count ?: 0}"/>
  </div>
</div>
</body>
</html>
