<% import grails.persistence.Event %>
<%=packageName%>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--6-col mdl-cell--4-col-phone">
  <div class="mdl-card__title mdl-shadow--2dp">
    <g:message code="default.show.label" args="[entityName]"/>
  </div>

  <div class="mdl-card__supporting-text">
    <g:if test="\${flash.message}">
      <div class="message" role="status">\${flash.message}</div>
    </g:if>

    <% excludedProps = Event.allEvents.toList() << 'id' << 'version'
      allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
      props = domainClass.properties.findAll {
        allowedNames.contains(it.name) && !excludedProps.contains(it.name) && (domainClass.constrainedProperties[it.name] ? domainClass.constrainedProperties[it.name].display : true)
      }
      Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
      props.each { p -> %>
    <g:if test="\${${propertyName}?.${p.name}}">
      <div class="mdl-grid">
        <div id="${p.name}-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="${p.name}-label">
          <% if (p.isEnum()) { %>
          <g:fieldValue bean="\${${propertyName}}" field="${p.name}"/>
          <% } else if (p.oneToMany || p.manyToMany) { %>
          <g:each in="\${${propertyName}.${p.name}}" var="${p.name[0]}">
            <g:link controller="${p.referencedDomainClass?.propertyName}" namespace="admin" action="show" id="\${${p.name[0]}.id}">\${${p.name[0]}?.encodeAsHTML()}</g:link>
          </g:each>
          <% } else if (p.manyToOne || p.oneToOne) { %>
          <g:link controller="${p.referencedDomainClass?.propertyName}" namespace="admin" action="show" id="\${${propertyName}?.${p.name}?.id}">\${${propertyName}?.${p.name}?.encodeAsHTML()}</g:link>
          <% } else if (p.type == Boolean || p.type == boolean) { %>
          <g:formatBoolean boolean="\${${propertyName}?.${p.name}}"/>
          <% } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
          <g:formatDate date="\${${propertyName}?.${p.name}}"/>
          <% } else if (!p.type.isArray()) { %>
          <g:fieldValue bean="\${${propertyName}}" field="${p.name}"/>
          <% } %>
        </div>
      </div>
    </g:if>
    <% } %>
  </div>


  <g:form class="mdl-card__actions mdl-card--border" action="delete" resource="\${${propertyName}}" namespace="admin" method="DELETE">
      <g:link action="edit" namespace="admin" resource="\${${propertyName}}"
              class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
        <g:message code="default.button.edit.label" default="Edit"/>
      </g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link>
  </g:form>
</div>

</body>
</html>
