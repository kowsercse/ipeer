package ipeer.service

import grails.transaction.Transactional
import ipeer.entity.Role
import ipeer.entity.User
import ipeer.entity.UserRole
import org.apache.commons.lang.StringUtils

import java.time.LocalDateTime
import java.time.ZoneId
import java.util.regex.Pattern

class UserService {
  private static String PASSWORD_PATTERN = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,10}";

  def springSecurityService

  @Transactional
  User addUser(String username, Role role) {
    def user = new User(username: username, accountExpired: false, accountLocked: false, passwordExpired: true, enabled: true)
    user.password = ""
    user.save flush: true

    UserRole.create user, role, true

    user
  }

  @Transactional
  User addUser(String username, String password, Role role) {
    def user = new User(username: username, accountExpired: false, accountLocked: false, passwordExpired: false, password: password, enabled: true)
    user.save flush: true

    UserRole.create user, role, true

    user
  }

  @Transactional
  def getPasswordResetToken(User user) {
    def expirationTime = LocalDateTime.now().minusDays(1)
    if(user.resetToken == null || toLocalDateTime(user).isBefore(expirationTime)) {
      user.resetToken = UUID.randomUUID().toString()
      user.tokenGenerationTime = new Date()

      user.save flush: true
    }

    user.resetToken
  }

  private static LocalDateTime toLocalDateTime(User user) {
    LocalDateTime.ofInstant(user.tokenGenerationTime.toInstant(), ZoneId.systemDefault())
  }

  def changePassword(final User user, final String currentPassword, final String newPassword, final String confirmPassword) {
    if(!springSecurityService.passwordEncoder.isPasswordValid(user.password, currentPassword, null)) {
      return ['Current password does not match']
    }

    updatePassword(user, newPassword, confirmPassword)
  }

  @Transactional
  def updatePassword(User user, String newPassword, String confirmPassword) {
    def errors = validatePassword(newPassword, confirmPassword)
    if(errors.isEmpty()) {
      user.password = newPassword
      user.passwordExpired = false
      user.resetToken = null
      user.save flush: true
    }

    errors
  }

  private static def validatePassword(String newPassword, String confirmPassword) {
    def errors = [];
    if (StringUtils.isBlank(newPassword) || StringUtils.isBlank(confirmPassword)) {
      errors.add("Password can not be empty")
      return;
    }
    if (!newPassword.equals(confirmPassword) && PASSWORD_PATTERN) {
      errors.add("Confirm password does not match")
    }

    def pattern = Pattern.compile(PASSWORD_PATTERN)
    if (!pattern.matcher(newPassword).matches()) {
      errors.add("Password must contain number, upper case letter & small case letter")
      errors.add("Password length must be between 6 to 10 character")
    }

    errors
  }
}
