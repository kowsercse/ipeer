package ipeer.service

import ipeer.entity.Version

class VersionService {

  public static final String MENTOR = "mentor"
  public static final String MENTEE = "veteran"
  public static final String EXTENSION = ".apk"

  def grailsApplication

  def getLatestVersion() {
    def versions = Version.where { enabled }.list(sort: 'releaseDate', order: 'desc', max: 1)
    versions?.get(0)
  }

  def storeMentorApp(final Version version, final InputStream inputStream) {
    writeToFile(inputStream, version.id, MENTOR)
  }

  def storeMenteeApp(final Version version, final InputStream inputStream) {
    writeToFile(inputStream, version.id, MENTEE)
  }

  def getMentorApp(final Version version) {
    getFile(MENTOR, version?.id)
  }

  def getMenteeApp(final Version version) {
    getFile(MENTEE, version?.id)
  }

  def getMentorAppName(final Version version) {
    "${MENTOR}-${version.major}.${version.minor}.${version.buildNumber}${EXTENSION}"
  }

  def getMenteeAppName(final Version version) {
    "${MENTEE}-${version.major}.${version.minor}.${version.buildNumber}${EXTENSION}"
  }

  private boolean writeToFile(InputStream inputStream, Long id, final String prefix) {
    File file = getFile(prefix, id)
    if (file.exists()) {
      file.delete()
    }

    if(file.createNewFile()) {
      file.bytes = inputStream.bytes
      return true
    }

    return false
  }

  private File getFile(String prefix, long id) {
    String uploadDirectory = grailsApplication.config.version.storage.path
    return new File(uploadDirectory, prefix + "-" + id + EXTENSION)
  }
}
