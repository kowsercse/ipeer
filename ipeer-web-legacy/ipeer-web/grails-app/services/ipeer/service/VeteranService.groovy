package ipeer.service

import grails.transaction.Transactional
import ipeer.entity.*
import ipeer.utils.DateUtils

import java.time.LocalDate

class VeteranService {

  public static final int MAX_SURVEY = 12
  def userService
  def grailsLinkGenerator

  @Transactional
  def addVeteran(Mentor mentor, Date startDate, User user) {
    startDate = startDate ?: new Date();
    def veteran = new Veteran(mentor: mentor, startDate: startDate, user: user)
    veteran.save flush: true

    if (!veteran.user) {
      def veteranRole = Role.findByAuthority Role.ROLE_VETERAN
      veteran.user = userService.addUser veteran.id.toString(), veteranRole
      veteran.save flush: true
    }

    veteran
  }

  @Transactional
  def getPasswordResetTokenUrl(Veteran veteran) {
    def resetToken = userService.getPasswordResetToken(veteran.user)
    grailsLinkGenerator.link namespace: "front", controller: "profile", action: "reset", absolute: true, params: [token: resetToken]
  }

  def getGetSchedulesFor(Veteran veteran) {
    Schedule.where {end > veteran.startDate}.list(sort: 'end', order: 'asc', max: MAX_SURVEY)
  }

  Schedule getCurrentScheduleFor(final Veteran veteran) {
    Date now = DateUtils.toDate(LocalDate.now())
    def schedules = getGetSchedulesFor veteran
    for (Schedule schedule : schedules) {
      if (now.compareTo(schedule.start) >= 0 && now.compareTo(schedule.end) <= 0) {
        def currentSummary = SubmissionSummary.findByVeteranAndSchedule(veteran, schedule)
        if(!currentSummary) {
          schedule.survey.questions = schedule.survey.questions.sort({ it.title })
          schedule.survey.questions.forEach({
            it.choices = it.choices.sort({ it.value })
          })

          return schedule
        }
      }
    }

    return null
  }

}
