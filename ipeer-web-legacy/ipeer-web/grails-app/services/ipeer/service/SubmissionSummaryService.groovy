package ipeer.service

import grails.transaction.Transactional
import ipeer.entity.Schedule
import ipeer.entity.SubmissionSummary
import ipeer.entity.SubmittedAnswer
import ipeer.entity.Veteran
import ipeer.utils.DateUtils
import org.codehaus.groovy.grails.web.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.time.LocalDate

@Transactional
class SubmissionSummaryService {

  private final Logger logger = LoggerFactory.getLogger(getClass())

  def veteranService

  def submitSurvey(JSONObject jsonSummary, Veteran veteran) {
    def schedule = Schedule.findById(jsonSummary.schedule)
    def currentSummary = SubmissionSummary.findByVeteranAndSchedule(veteran, schedule)
    if(isActive(schedule, currentSummary)) {
      logger.debug "Schedule is active and continuing with submission"
      def submittedAnswers = jsonSummary.submittedAnswers?.collect { new SubmittedAnswer(it) };
      if(submittedAnswers) {
        SubmissionSummary submissionSummary = new SubmissionSummary(
            schedule: schedule,
            veteran: veteran,
            submissionTime: new Date(),
            score: submittedAnswers.choice.value.sum()
        );
        submittedAnswers.forEach({ submissionSummary.addToSubmittedAnswers(it) });
        submissionSummary.progressDirection = calculateProgressDirection(veteran, schedule, submissionSummary.score);
        logger.debug "Saving the submission with {} answers", submittedAnswers.size()

        return submissionSummary.save(flush: true)
      }
    }
    logger.warn "Schedule is not active for submission {}", schedule
  }

  private int calculateProgressDirection(Veteran veteran, Schedule currentSchedule, int score) {
    SubmissionSummary previousSummary = getPreviousSubmissionSummary(veteran, currentSchedule)
    previousSummary? score.compareTo(previousSummary.score) : 1
  }

  private SubmissionSummary getPreviousSubmissionSummary(Veteran veteran, Schedule currentSchedule) {
    def veteranSchedules = veteranService.getGetSchedulesFor(veteran)
    if(veteranSchedules.size() == 1) {
      return null
    }

    for (int i = 0; i < veteranSchedules.size(); i++) {
      if (currentSchedule.equals(veteranSchedules[i])) {
        return SubmissionSummary.findByVeteranAndSchedule(veteran, veteranSchedules[i - 1])
      }
    }

    return null
  }

  private static boolean isActive(Schedule schedule, SubmissionSummary currentSummary) {
    schedule && !currentSummary && isActive(schedule)
  }

  private static boolean isActive(Schedule schedule) {
    Date now = DateUtils.toDate(LocalDate.now())
    now.compareTo(schedule.start) >= 0 && now.compareTo(schedule.end) <= 0
  }
}
