package db.dump

import grails.transaction.Transactional
import ipeer.entity.Choice
import ipeer.entity.Question
import ipeer.entity.Survey

@Transactional
class SurveyImporterService {

  void importSurvey() {
    def yes = new Choice(label: "Yes", value: 3).save flush: true
    def better = new Choice(label: "Better", value: 3).save flush: true
    def more = new Choice(label: "More", value: 3).save flush: true
    def same = new Choice(label: "Same", value: 2).save flush: true
    def mayBe = new Choice(label: "May be", value: 2).save flush: true
    def less = new Choice(label: "Less", value: 1).save flush: true
    def worse = new Choice(label: "Worse", value: 1).save flush: true
    def no = new Choice(label: "No", value: 1).save flush: true

    def survey = new Survey(active: true, title: "Dryhootch Mentor Survey")

    [
        [title: "Have you engaged in any risky behavior (as you define it) this week?", choices: [more, same, less]],
        [title: "How well did you sleep this week?", choices: [better, same, worse]],
        [title: "Has your health changed this week?", choices: [better, same, worse]],
        [title: "How stressful has this week been?", choices: [more, same, less]],
        [title: "Are you feeling good about yourself overall this week?", choices: [yes, mayBe, no]],
    ].forEach({ item ->
        def question = new Question(title: item.title)
        item.choices.forEach({ choice -> question.addToChoices(choice) })
        question.save flush: true
        survey.addToQuestions(question)
    })

    survey.save flush: true
  }

}
