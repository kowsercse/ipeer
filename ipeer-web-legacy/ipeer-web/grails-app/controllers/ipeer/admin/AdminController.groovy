package ipeer.admin

import grails.plugin.springsecurity.annotation.Secured


@Secured(['ROLE_MENTOR'])
class AdminController {

  static namespace = "admin"

  def versionService

  def index() {
    respond versionService.latestVersion
  }

}
