package ipeer.admin

import grails.plugin.springsecurity.annotation.Secured
import ipeer.entity.User

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
@Secured(['ROLE_MENTOR'])
class UserController {

  static namespace = "admin"
  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", updatePassword: "PUT"]

  def userService

  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond User.list(params), model: [userInstanceCount: User.count()]
  }

  def show(User userInstance) {
    respond userInstance
  }

  def create() {
    respond new User(params)
  }

  @Transactional
  def save(User userInstance) {
    if (userInstance == null) {
      notFound()
      return
    }

    userInstance.password = ""
    userInstance.passwordExpired = true
    if (!userInstance.validate()) {
      respond userInstance.errors, view: 'create'
      return
    }

    userInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])
        redirect userInstance
      }
      '*' { respond userInstance, [status: CREATED] }
    }
  }

  def edit(User userInstance) {
    respond userInstance
  }

  @Transactional
  def update(User userInstance) {
    if (userInstance == null) {
      notFound()
      return
    }

    if (userInstance.hasErrors()) {
      respond userInstance.errors, view: 'edit'
      return
    }

    userInstance.save flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
        redirect userInstance
      }
      '*' { respond userInstance, [status: OK] }
    }
  }

  @Transactional
  def delete(User userInstance) {
    if (userInstance == null) {
      notFound()
      return
    }

    userInstance.delete flush: true

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NO_CONTENT }
    }
  }

  def changePassword(User userInstance) {
    if (userInstance == null) {
      notFound()
      return
    }

    respond userInstance
  }

  @Transactional
  def updatePassword(User userInstance) {
    if (userInstance == null) {
      notFound()
      return
    }

    def errors = userService.updatePassword(userInstance, params.newPassword, params.confirmPassword)
    if(!errors.isEmpty()) {
      render(view: 'changePassword', model: [userInstance: userInstance, errors: errors])
    }
    else if(userInstance.mentor) {
      redirect namespace: 'admin', controller: 'mentor', action: 'show', id: userInstance.mentorId
    }
    else if(userInstance.veteran) {
      redirect namespace: 'admin', controller: 'veteran', action: 'show', id: userInstance.veteranId
    }
    else {
      redirect userInstance
    }
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }
}
