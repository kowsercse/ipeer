package ipeer.admin

import grails.plugin.springsecurity.annotation.Secured
import ipeer.entity.User
import ipeer.entity.Veteran

@Secured(['ROLE_MENTOR'])
class ReportController {

  static namespace = "admin"

  def springSecurityService

  def surveyCompletion(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    User user = springSecurityService.currentUser as User
    def veterans = user.mentor ? Veteran.where { mentor == user.mentor }.list() : Veteran.list(params);
    respond veterans, model: [veteranInstanceCount: Veteran.count()]
  }

}
