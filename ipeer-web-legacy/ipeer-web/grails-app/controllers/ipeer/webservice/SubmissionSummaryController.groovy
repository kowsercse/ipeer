package ipeer.webservice

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import ipeer.entity.Role
import ipeer.entity.SubmissionSummary
import ipeer.entity.User
import ipeer.entity.Veteran

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class SubmissionSummaryController {

  static namespace = "webservice"
  static responseFormats = ['json', 'xml']
  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  def springSecurityService

  def beforeInterceptor = {
    if(springSecurityService.authentication.authorities.any {it.authority == Role.ROLE_MENTOR}) {
      return true
    }

    def submissionSummary = SubmissionSummary.findById(params.id)
    def user = springSecurityService.currentUser as User
    if(user?.veteran != submissionSummary.veteran) {
      render(status: NOT_FOUND)
      return false
    }

    return true
  }

  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond SubmissionSummary.list(params), [status: OK]
  }

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def show(SubmissionSummary submissionSummary) {
    respond submissionSummary
  }

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def submittedAnswers(SubmissionSummary submissionSummary) {
    respond submissionSummary.submittedAnswers
  }

  @Transactional
  def save(SubmissionSummary submissionSummaryInstance) {
    if (submissionSummaryInstance == null) {
      render status: NOT_FOUND
      return
    }

    submissionSummaryInstance.validate()
    if (submissionSummaryInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    submissionSummaryInstance.save flush: true
    respond submissionSummaryInstance, [status: CREATED]
  }

  @Transactional
  def update(SubmissionSummary submissionSummaryInstance) {
    if (submissionSummaryInstance == null) {
      render status: NOT_FOUND
      return
    }

    submissionSummaryInstance.validate()
    if (submissionSummaryInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    submissionSummaryInstance.save flush: true
    respond submissionSummaryInstance, [status: OK]
  }

  @Transactional
  def delete(SubmissionSummary submissionSummaryInstance) {

    if (submissionSummaryInstance == null) {
      render status: NOT_FOUND
      return
    }

    submissionSummaryInstance.delete flush: true
    render status: NO_CONTENT
  }
}
