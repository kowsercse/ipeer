package ipeer.webservice

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import ipeer.entity.Question

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class QuestionController {

  static namespace = "webservice"
  static responseFormats = ['json', 'xml']
  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond Question.list(params), [status: OK]
  }

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def show(Question question) {
    respond question
  }

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def choices(Question question) {
    respond question.choices
  }

  @Transactional
  def save(Question questionInstance) {
    if (questionInstance == null) {
      render status: NOT_FOUND
      return
    }

    questionInstance.validate()
    if (questionInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    questionInstance.save flush: true
    respond questionInstance, [status: CREATED]
  }

  @Transactional
  def update(Question questionInstance) {
    if (questionInstance == null) {
      render status: NOT_FOUND
      return
    }

    questionInstance.validate()
    if (questionInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    questionInstance.save flush: true
    respond questionInstance, [status: OK]
  }

  @Transactional
  def delete(Question questionInstance) {

    if (questionInstance == null) {
      render status: NOT_FOUND
      return
    }

    questionInstance.delete flush: true
    render status: NO_CONTENT
  }
}
