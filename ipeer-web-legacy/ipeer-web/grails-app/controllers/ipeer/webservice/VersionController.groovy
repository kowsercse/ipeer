package ipeer.webservice

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import ipeer.entity.Version

import static org.springframework.http.HttpStatus.*

class VersionController {

  static namespace = "webservice"
  static responseFormats = ['json', 'xml']
  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  def versionService

  @Secured('permitAll')
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond Version.where { enabled }.list(params), [status: OK]
  }

  @Secured('permitAll')
  def show(Version version) {
    respond version
  }

  @Transactional
  @Secured('ROLE_SUPER_ADMIN')
  def save() {
    def versionInstance = new Version(request.JSON)
    versionInstance.enabled = true
    versionInstance.releaseDate = new Date()

    versionInstance.validate()
    if (versionInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    versionInstance.save flush: true
    respond versionInstance, [status: CREATED]
  }

  @Secured('permitAll')
  def latest() {
    def latestVersion = versionService.getLatestVersion()
    if (latestVersion == null) {
      render status: NOT_FOUND
      return
    }

    redirect mapping: 'version', id: latestVersion.id
  }

  @Secured('ROLE_SUPER_ADMIN')
  def uploadMentor(Version versionInstance) {
    if (versionInstance == null) {
      return render(status: NOT_FOUND)
    }

    def result = versionService.storeMentorApp(versionInstance, request.inputStream)
    return result ? render(status: OK) : render(status: INTERNAL_SERVER_ERROR)
  }

  @Secured('ROLE_SUPER_ADMIN')
  def uploadMentee(Version versionInstance) {
    if (versionInstance == null) {
      return render(status: NOT_FOUND)
    }

    def result = versionService.storeMenteeApp(versionInstance, request.inputStream)
    return result ? render(status: OK) : render(status: INTERNAL_SERVER_ERROR)
  }

  @Secured('permitAll')
  def downloadMentor(Version versionInstance) {
    if(versionInstance == null) {
      return render(status: NOT_FOUND)
    }

    def app = versionService.getMentorApp(versionInstance)
    if (!app?.exists()) {
      return render(status: NOT_FOUND)
    }

    def appName = versionService.getMentorAppName(versionInstance)
    prepareResponse(app, appName)
  }

  @Secured('permitAll')
  def downloadMentee(Version versionInstance) {
    if(versionInstance == null) {
      return render(status: NOT_FOUND)
    }

    def app = versionService.getMenteeApp(versionInstance)
    if (!app?.exists()) {
      return render(status: NOT_FOUND)
    }

    def appName = versionService.getMenteeAppName(versionInstance)
    prepareResponse(app, appName)
  }

  @Transactional
  def update(Version versionInstance) {
    if (versionInstance == null) {
      render status: NOT_FOUND
      return
    }

    versionInstance.validate()
    if (versionInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    versionInstance.save flush: true
    respond versionInstance, [status: OK]
  }

  @Transactional
  def delete(Version versionInstance) {
    if (versionInstance == null) {
      render status: NOT_FOUND
      return
    }

    versionInstance.delete flush: true
    render status: NO_CONTENT
  }

  private void prepareResponse(File app, appName) {
    response.setHeader "Content-disposition", "attachment; filename=${appName}"
    response.contentType = "application/vnd.android.package-archive"
    response.outputStream << app.newInputStream()
    response.outputStream.flush()
  }
}
