package ipeer.webservice

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import ipeer.entity.Issue
import ipeer.entity.User

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class IssueController {

  static responseFormats = ['json', 'xml']
  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
  static namespace = "webservice"

  def springSecurityService

  @Secured(['ROLE_SUPER_ADMIN'])
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond Issue.list(params), [status: OK]
  }

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def show(Issue issue) {
    respond issue
  }

  @Transactional
  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def save(Issue issueInstance) {
    if (issueInstance == null) {
      render status: NOT_FOUND
      return
    }

    issueInstance.createdBy = springSecurityService.currentUser as User
    issueInstance.autoReported = Boolean.FALSE

    if (!issueInstance.validate()) {
      render status: NOT_ACCEPTABLE
      return
    }

    issueInstance.save flush: true
    respond issueInstance, [status: CREATED]
  }

  @Transactional
  def update(Issue issueInstance) {
    if (issueInstance == null) {
      render status: NOT_FOUND
      return
    }

    issueInstance.validate()
    if (issueInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    issueInstance.save flush: true
    respond issueInstance, [status: OK]
  }

  @Transactional
  def delete(Issue issueInstance) {

    if (issueInstance == null) {
      render status: NOT_FOUND
      return
    }

    issueInstance.delete flush: true
    render status: NO_CONTENT
  }
}
