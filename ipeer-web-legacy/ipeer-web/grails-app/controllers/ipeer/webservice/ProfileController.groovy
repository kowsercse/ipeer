package ipeer.webservice

import grails.plugin.springsecurity.annotation.Secured
import ipeer.entity.Role
import ipeer.entity.SubmissionSummary
import ipeer.entity.User

import static org.springframework.http.HttpStatus.NOT_FOUND

@Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
class ProfileController {

  static namespace = "webservice"
  static responseFormats = ['json', 'xml']

  def springSecurityService

  def beforeInterceptor = {
    if(springSecurityService.authentication.authorities.any {it.authority == Role.ROLE_MENTOR}) {
      return true
    }

    def user = User.findById(params.id)
    def currentUser = springSecurityService.currentUser as User
    if(currentUser != user) {
      render(status: NOT_FOUND)
      return false
    }

    return true
  }

  def show(User user) {
    respond user
  }

}
