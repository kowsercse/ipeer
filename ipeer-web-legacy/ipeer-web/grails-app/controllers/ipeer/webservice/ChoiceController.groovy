package ipeer.webservice

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import ipeer.entity.Choice

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class ChoiceController {

  static namespace = "webservice"
  static responseFormats = ['json', 'xml']
  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond Choice.list(params), [status: OK]
  }

  @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
  def show(Choice choice) {
    respond choice
  }

  @Transactional
  def save(Choice choiceInstance) {
    if (choiceInstance == null) {
      render status: NOT_FOUND
      return
    }

    choiceInstance.validate()
    if (choiceInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    choiceInstance.save flush: true
    respond choiceInstance, [status: CREATED]
  }

  @Transactional
  def update(Choice choiceInstance) {
    if (choiceInstance == null) {
      render status: NOT_FOUND
      return
    }

    choiceInstance.validate()
    if (choiceInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    choiceInstance.save flush: true
    respond choiceInstance, [status: OK]
  }

  @Transactional
  def delete(Choice choiceInstance) {

    if (choiceInstance == null) {
      render status: NOT_FOUND
      return
    }

    choiceInstance.delete flush: true
    render status: NO_CONTENT
  }
}
