package ipeer.webservice

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import ipeer.entity.Survey

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class SurveyController {

  static namespace = "webservice"
  static responseFormats = ['json', 'xml']
  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  @Secured(['ROLE_MENTOR', 'ROLE_VETERAN'])
  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond Survey.list(params), [status: OK]
  }

  @Secured(['ROLE_MENTOR', 'ROLE_VETERAN'])
  def show(Survey survey) {
    respond survey
  }

  @Secured(['ROLE_MENTOR', 'ROLE_VETERAN'])
  def questions(Survey survey) {
    respond survey.questions
  }

  @Transactional
  def save(Survey surveyInstance) {
    if (surveyInstance == null) {
      render status: NOT_FOUND
      return
    }

    surveyInstance.validate()
    if (surveyInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    surveyInstance.save flush: true
    respond surveyInstance, [status: CREATED]
  }

  @Transactional
  def update(Survey surveyInstance) {
    if (surveyInstance == null) {
      render status: NOT_FOUND
      return
    }

    surveyInstance.validate()
    if (surveyInstance.hasErrors()) {
      render status: NOT_ACCEPTABLE
      return
    }

    surveyInstance.save flush: true
    respond surveyInstance, [status: OK]
  }

  @Transactional
  def delete(Survey surveyInstance) {

    if (surveyInstance == null) {
      render status: NOT_FOUND
      return
    }

    surveyInstance.delete flush: true
    render status: NO_CONTENT
  }
}
