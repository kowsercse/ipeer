package ipeer.webservice

import grails.plugin.springsecurity.annotation.Secured
import ipeer.entity.Schedule

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ScheduleController {

    static namespace = "webservice"
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Schedule.list(params), [status: OK]
    }

    @Secured(['ROLE_VETERAN', 'ROLE_MENTOR'])
    def show(Schedule schedule) {
        respond schedule
    }

    @Transactional
    def save(Schedule scheduleInstance) {
        if (scheduleInstance == null) {
            render status: NOT_FOUND
            return
        }

        scheduleInstance.validate()
        if (scheduleInstance.hasErrors()) {
            render status: NOT_ACCEPTABLE
            return
        }

        scheduleInstance.save flush:true
        respond scheduleInstance, [status: CREATED]
    }

    @Transactional
    def update(Schedule scheduleInstance) {
        if (scheduleInstance == null) {
            render status: NOT_FOUND
            return
        }

        scheduleInstance.validate()
        if (scheduleInstance.hasErrors()) {
            render status: NOT_ACCEPTABLE
            return
        }

        scheduleInstance.save flush:true
        respond scheduleInstance, [status: OK]
    }

    @Transactional
    def delete(Schedule scheduleInstance) {

        if (scheduleInstance == null) {
            render status: NOT_FOUND
            return
        }

        scheduleInstance.delete flush:true
        render status: NO_CONTENT
    }
}
