package ipeer

import grails.transaction.Transactional
import groovy.text.SimpleTemplateEngine
import ipeer.entity.Mentor
import ipeer.entity.User
import org.springframework.security.access.annotation.Secured

import static org.springframework.http.HttpStatus.NOT_FOUND

@Secured('permitAll')
class ProfileController {

  static namespace = "front"

  def userService
  def springSecurityService
  def mailService
  def grailsLinkGenerator

  @Secured('isAuthenticated()')
  def changePassword() {
  }

  @Secured('isAuthenticated()')
  def updatePassword() {
    User user = springSecurityService.currentUser as User
    def errors = userService.changePassword(user, params.currentPassword, params.newPassword, params.confirmPassword)
    return errors.isEmpty() ? render(view: 'success') : render(view: 'changePassword', model: [errors: errors])
  }

  def reset(String token) {
    def user = User.findByResetToken(token)
    user ? render(view: 'reset', model: [user: user]) : notFound()
  }

  @Transactional
  def resetPassword(String token) {
    def user = User.findByResetToken(token)

    if (user == null) {
      notFound()
      return
    }

    def errors = userService.updatePassword(user, params.newPassword, params.confirmPassword)
    return errors.isEmpty() ? render(view: 'success') : render(view: 'reset', model: [user: user, errors: errors])
  }

  @Transactional
  def sendResetToken() {
    def user = User.findByUsername(params.username)

    if (user?.mentor && user?.enabled) {
      def resetToken = userService.getPasswordResetToken(user)
      String mailBody = getEmailBody(user.mentor, resetToken)

      mailService.sendMail {
        to user.mentor.email
        subject "Reset your password"
        body mailBody
      }
    }

    return render(view: 'emailSent')
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'profile.label', default: 'Profile'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }

  private String getEmailBody(Mentor mentorInstance, String token) {
    def text = 'Dear $firstName $lastName,\n' +
        '\n' +
        'Here is the information to set your password.\n' +
        '\n' +
        'Your username: $username.\n' +
        'Please set your password at $resetLink\n' +
        '\n' +
        'For more:\n' +
        'Homepage: $homeLink\n' +
        'Documentation: $documentationLink\n' +
        '\n' +
        '\n' +
        'Thank you\n' +
        'Administrator'
    def engine = new SimpleTemplateEngine()

    def template = engine.createTemplate(text).make([
        firstName        : mentorInstance.firstName,
        lastName         : mentorInstance.lastName,
        username         : mentorInstance.user.username,
        resetLink        : grailsLinkGenerator.link(namespace: "front", controller: "profile", action: "reset", absolute: true, params: [token: token]),
        homeLink         : grailsLinkGenerator.link(controller: "home", action: "index", absolute: true),
        documentationLink: grailsLinkGenerator.link(controller: "documentation", action: "index", absolute: true)
    ])
    template.toString()
  }

}
