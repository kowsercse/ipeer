package ipeer

import ipeer.entity.Role
import ipeer.entity.Schedule
import ipeer.entity.User
import ipeer.entity.Version
import org.springframework.security.access.annotation.Secured

@Secured('permitAll')
class HomeController {

  def springSecurityService
  def versionService
  def veteranService

  def index() {
    if (springSecurityService.isLoggedIn()) {
      if (springSecurityService.authentication.authorities.any { it.authority == Role.ROLE_MENTOR }) {
        redirect controller: "admin", action: "index", namespace: "admin"
      }
      else {
        def user = springSecurityService.currentUser as User
        Schedule currentSchedule = veteranService.getCurrentScheduleFor user.veteran
        return render(view: 'index', model: [latestVersion: versionService.latestVersion, currentSchedule: currentSchedule])
      }
    } else {
      redirect controller: "login", action: "auth"
    }
  }

}
