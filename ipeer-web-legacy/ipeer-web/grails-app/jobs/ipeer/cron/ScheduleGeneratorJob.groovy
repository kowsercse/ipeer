package ipeer.cron

import grails.transaction.Transactional
import ipeer.entity.Schedule
import ipeer.entity.Survey
import ipeer.utils.DateUtils

import java.time.LocalDate

@Transactional
class ScheduleGeneratorJob {

  static triggers = {
    cron name: 'schedule-generator', cronExpression: "0 0 0 ? * MON,FRI *"
  }

  def surveyService

  def execute() {
    def start = DateUtils.toDate(LocalDate.now())
    def end = DateUtils.toDate(LocalDate.now().plusDays(1))
    def survey = surveyService.getLatestSurvey()

    if (survey) {
      def latestSchedule = new Schedule(start: start, end: end, survey: survey)
      latestSchedule.save flush: true
    }
  }

}
