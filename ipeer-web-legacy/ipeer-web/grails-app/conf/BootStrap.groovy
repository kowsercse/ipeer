import grails.converters.JSON
import grails.util.Environment
import ipeer.entity.*
import ipeer.utils.DateUtils

import java.beans.Introspector
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.temporal.TemporalField
import java.time.temporal.WeekFields

class BootStrap {

  def grailsLinkGenerator
  def grailsApplication
  def userService
  def surveyImporterService

  def init = { servletContext ->
    if(Role.count() == 0) {
      [Role.ROLE_MENTOR, Role.ROLE_VETERAN, Role.ROLE_SUPER_ADMIN].forEach({
        role -> new Role(authority: role).save(flush: true)
      });
    }

    if(User.count() == 0) {
      def adminConfig = grailsApplication.config.admin
      userService.addUser adminConfig.username, adminConfig.password, Role.findByAuthority(Role.ROLE_SUPER_ADMIN)
    }

    if(Survey.count() == 0) {
      surveyImporterService.importSurvey()
    }

    if(Environment.current.name == "development") {
      for (domainClass in grailsApplication.domainClasses) {
        domainClass.metaClass.adminController = Introspector.decapitalize(domainClass.getShortName())
      }
      if(Schedule.count() == 0) {
        final TemporalField dayOfWeek = WeekFields.of(DayOfWeek.MONDAY, 1).dayOfWeek();
        def start = LocalDate.now()
            .minusDays(30)
            .with(dayOfWeek, 1)
        def survey = Survey.findById(1)

        for (int i = 0; i < 10; i++) {
          def monday = start.plusWeeks(i)
          def friday = monday.plusDays(4)

          new Schedule(start: DateUtils.toDate(monday), end: DateUtils.toDate(monday.plusDays(1)), survey: survey).save flush: true
          new Schedule(start: DateUtils.toDate(friday), end: DateUtils.toDate(friday.plusDays(1)), survey: survey).save flush: true
        }
      }
    }

    JSON.registerObjectMarshaller(Choice) {
      return [
          label: it.label,
          value: it.value,
          links : [
              [rel: "self", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'choice', id: it.id, absolute: true)]
          ]
      ]
    }
    JSON.registerObjectMarshaller(User) {
      def map = [
          username: it.username,
          links   : [
              [rel: "self", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'profile', id: it.id, absolute: true)],
          ]
      ]

      if(it.veteran) {
        map.links.push([rel: "veteran", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'veteran', id: it.veteran.id, absolute: true)])
      }
      if(it.mentor) {
        map.links.push([rel: "mentor", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'mentor', id: it.mentor.id, absolute: true)])
      }

      return map
    }
    JSON.registerObjectMarshaller(Mentor) {
      return [
          firstName : it.firstName,
          lastName: it.lastName,
          phone: it.phone,
          email: it.email,
          links : [
              [rel: "self", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'mentor', id: it.id, absolute: true)],
              [rel: "veteran", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'mentor', action: 'veterans', id: it.id, absolute: true)]
          ]
      ]
    }
    JSON.registerObjectMarshaller(Question) {
      return [
          title : it.title,
          links : [
              [rel: "self", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'question', id: it.id, absolute: true)],
              [rel: "choice", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'question', action: 'choices', id: it.id, absolute: true)]
          ]
      ]
    }
    JSON.registerObjectMarshaller(Schedule) {
      return [
          start: it.start,
          end: it.end,
          links : [
              [rel: "self", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'schedule', id: it.id, absolute: true)],
              [rel: "survey", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'survey', id: it.survey.id, absolute: true)],
          ]
      ]
    }
    JSON.registerObjectMarshaller(SubmissionSummary) {
      return [
          submissionTime: it.submissionTime,
          score: it.score,
          progressDirection: it.progressDirection,
          links : [
              [rel: "self", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'submissionSummary', id: it.id, absolute: true)],
              [rel: "schedule", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'schedule', id: it.schedule.id, absolute: true)],
              [rel: "veteran", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'veteran', id: it.veteran.id, absolute: true)],
              [rel: "submittedAnswer", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'submissionSummary', action: 'submittedAnswers', id: it.id, absolute: true)],
          ]
      ]
    }
    JSON.registerObjectMarshaller(SubmittedAnswer) {
      return [
          links : [
              [rel: "self", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'submittedAnswer', id: it.id, absolute: true)],
              [rel: "question", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'question', id: it.question.id, absolute: true)],
              [rel: "choice", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'choice', id: it.choice.id, absolute: true)],
              [rel: "submissionSummary", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'submissionSummary', id: it.submissionSummary.id, absolute: true)],
          ]
      ]
    }
    JSON.registerObjectMarshaller(Survey) {
      return [
          title : it.title,
          active : it.active,
          links : [
              [rel: "self", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'survey', id: it.id, absolute: true)],
              [rel: "question", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'survey', action: 'questions', id: it.id, absolute: true)]
          ]
      ]
    }
    JSON.registerObjectMarshaller(Veteran) {
      return [
          startDate : it.startDate,
          links : [
              [rel: "self", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'veteran', id: it.id, absolute: true)],
              [rel: "token", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'veteran', action: 'resetToken', id: it.id, absolute: true)],
              [rel: "schedule", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'veteran', action: 'schedules', id: it.id, absolute: true)],
              [rel: "submissionSummary", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'veteran', action: 'submissionSummaries', id: it.id, absolute: true)],
              [rel: "mentor", href: grailsLinkGenerator.link(namespace: "webservice", controller: 'mentor', id: it.mentor.id, absolute: true)],
          ]
      ]
    }
    JSON.registerObjectMarshaller(Version) {
      return [
          major : it.major,
          minor : it.minor,
          buildNumber : it.buildNumber,
          enabled : it.enabled,
          releaseDate : it.releaseDate,
          links : [
              [rel: "self", href: grailsLinkGenerator.link(mapping: 'version', id: it.id, absolute: true)],
              [rel: "mentor-app", href: grailsLinkGenerator.link(mapping: 'downloadMentor', id: it.id, absolute: true)],
              [rel: "mentee-app", href: grailsLinkGenerator.link(mapping: 'downloadMentee', id: it.id, absolute: true)],
          ]
      ]
    }
    JSON.registerObjectMarshaller(Issue) {
      return [
          summary : it.summary,
          detail : it.detail,
          errorLog : it.errorLog,
          links : [
              [rel: "self", href: grailsLinkGenerator.link(mapping: 'issue', id: it.id, absolute: true)],
          ]
      ]
    }
  }

  def destroy = {
  }

}
