class UrlMappings {

  static mappings = {
    group "/webservice", {
      "/"(controller: "endpoint", namespace: "webservice")
      "/survey/"(namespace: "webservice", controller: "endpoint", action: 'surveys')
      "/question/"(namespace: "webservice", controller: "endpoint", action: 'questions')
      "/choice/"(namespace: "webservice", controller: "endpoint", action: 'choices')
      "/profile/$id?"(controller: "profile", namespace: "webservice") { action = [GET:"show", PUT:"update", DELETE:"delete"] }
      "/mentor/$id?/veterans"(controller: "mentor", namespace: "webservice") { action = [GET:"veterans", POST:"addVeteran"] }
      "/mentor/$id?"(controller: "mentor", namespace: "webservice") { action = [GET:"show", PUT:"update", DELETE:"delete"] }
      "/veteran/$id/submission-summaries"(controller: "veteran", namespace: "webservice") { action = [GET:"submissionSummaries", POST:"submitAnswers"] }
      "/veteran/$id"(controller: "veteran", namespace: "webservice") { action = [GET:"show", PUT:"update", DELETE:"delete"] }
      "/choice/$id?"(controller: "choice", namespace: "webservice") { action = [GET:"show", PUT:"update", DELETE:"delete"] }
      "/question/$id?"(controller: "question", namespace: "webservice") { action = [GET:"show", PUT:"update", DELETE:"delete"] }
      "/survey/$id?"(controller: "survey", namespace: "webservice") { action = [GET:"show", PUT:"update", DELETE:"delete"] }
      "/schedule/$id?"(controller: "schedule", namespace: "webservice") { action = [GET:"show", PUT:"update", DELETE:"delete"] }
      name issues: "/issue" (controller: "issue", namespace: "webservice") { action = [GET: 'index', POST: 'save']}
      name issue: "/issue/$id?" (controller: "issue", namespace: "webservice") { action = [GET: 'show', PUT: 'update', DELETE: 'delete']}
      name versions: "/version" (controller: "version", namespace: "webservice") { action = [GET: 'index', POST: 'save']}
      name latestVersion: "/version/latest"(controller: "version", namespace: "webservice", action: 'latest')
      name downloadMentor: "/version/$id/mentor-app"(controller: "version", namespace: "webservice") { action = [GET:"downloadMentor", PUT:"uploadMentor"] }
      name downloadMentee: "/version/$id/mentee-app"(controller: "version", namespace: "webservice") { action = [GET:"downloadMentee", PUT:"uploadMentee"] }
      name version: "/version/$id" (controller: "version", namespace: "webservice") { action = [GET:"show", PUT:"update", DELETE:"delete"] }
      "/$controller/$id/$action?"(namespace: "webservice")
    }
    group "/admin", {
      "/"(controller: "admin", namespace: "admin", action: "index")
      "/$controller/$action?/$id?"(namespace: "admin")
    }
    group "/survey", {
      name currentSurvey: "/current" (namespace: "front", controller: "veteranSurvey") { action = [GET:"currentSurvey", POST: "submitAnswers"] }
    }
    "/issue/$action?/$id?"(namespace: "front", controller: "issue")
    "/profile/$action?" (namespace: "front", controller: "profile")
    "/$controller/$action?/$id?" {
      constraints {
        // apply constraints here
      }
    }

    "/"(controller: "home")
    "500"(view: '/error')
  }
}
