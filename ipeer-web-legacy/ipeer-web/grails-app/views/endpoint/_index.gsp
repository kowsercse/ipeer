{
    "links": [
        {"rel": "version", "href": "<g:createLink mapping="latestVersion" absolute="true"/>"},
        {"rel": "issue", "href": "<g:createLink mapping="issues" absolute="true" />"},
        {"rel": "profile", "href": "<g:createLink controller="profile" action="show" namespace="webservice" absolute="true" id="${user.id}"/>"},
        {"rel": "survey", "href": "<g:createLink controller="survey" action="index" namespace="webservice" absolute="true"/>"},
        {"rel": "question", "href": "<g:createLink controller="question" action="index" namespace="webservice" absolute="true"/>"},
        {"rel": "choice", "href": "<g:createLink controller="choice" action="index" namespace="webservice" absolute="true"/>"}
    ]
}
