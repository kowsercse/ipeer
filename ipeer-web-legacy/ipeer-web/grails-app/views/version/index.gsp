
<%@ page import="ipeer.entity.Version" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'version.label', default: 'Version')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-desktop  mdl-cell--8-col-tablet  mdl-cell--6-col-phone">
  <h1><g:message code="default.list.label" args="[entityName]"/></h1>
  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>

  <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
    <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="version.id.label" default="#"/>
      </th>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="major"
                        title="${message(code: 'version.major.label', default: 'Major')}"/>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="minor"
                        title="${message(code: 'version.minor.label', default: 'Minor')}"/>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="buildNumber"
                        title="${message(code: 'version.buildNumber.label', default: 'Build Number')}"/>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="releaseDate"
                        title="${message(code: 'version.releaseDate.label', default: 'Release Date')}"/>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="enabled"
                        title="${message(code: 'version.enabled.label', default: 'Enabled')}"/>
      
      <th class="mdl-data-table__cell--non-numeric">Action</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${versionInstanceList}" status="i" var="versionInstance">
      <tr>
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${versionInstance.id}">${versionInstance.id}</g:link>
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          ${fieldValue(bean: versionInstance, field: "major")}
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          ${fieldValue(bean: versionInstance, field: "minor")}
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          ${fieldValue(bean: versionInstance, field: "buildNumber")}
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          <g:formatDate date="${versionInstance.releaseDate}"/>
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          <g:formatBoolean boolean="${versionInstance.enabled}"/>
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${versionInstance.id}"><i class="mdl-color-text--grey material-icons">description</i></g:link>
          <g:link namespace="admin" action="edit" id="${versionInstance.id}"><i class="mdl-color-text--grey material-icons">edit</i></g:link>
        </td>
      </tr>
    </g:each>
    </tbody>
  </table>

  <div class="mdl-card__actions pagination">
    <g:link action="create" namespace="admin"
            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--green-700 mdl-color-text--white">
      <g:message code="default.new.label" args="[entityName]"/>
    </g:link>
    <g:paginate namespace="admin" controller="version" action="index" total="${versionInstanceCount ?: 0}"/>
  </div>
</div>
</body>
</html>
