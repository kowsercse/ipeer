
<%@ page import="ipeer.entity.Version" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'version.label', default: 'Version')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--6-col mdl-cell--4-col-phone">
  <div class="mdl-card__title mdl-shadow--2dp">
    <g:message code="default.show.label" args="[entityName]"/>
  </div>

  <div class="mdl-card__supporting-text">
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>

    
    <g:if test="${versionInstance?.major}">
      <div class="mdl-grid">
        <div id="major-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="version.major.label" default="Major"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="major-label">
          
          <g:fieldValue bean="${versionInstance}" field="major"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${versionInstance?.minor}">
      <div class="mdl-grid">
        <div id="minor-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="version.minor.label" default="Minor"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="minor-label">
          
          <g:fieldValue bean="${versionInstance}" field="minor"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${versionInstance?.buildNumber}">
      <div class="mdl-grid">
        <div id="buildNumber-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="version.buildNumber.label" default="Build Number"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="buildNumber-label">
          
          <g:fieldValue bean="${versionInstance}" field="buildNumber"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${versionInstance?.releaseDate}">
      <div class="mdl-grid">
        <div id="releaseDate-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="version.releaseDate.label" default="Release Date"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="releaseDate-label">
          
          <g:formatDate date="${versionInstance?.releaseDate}"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${versionInstance?.enabled}">
      <div class="mdl-grid">
        <div id="enabled-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="version.enabled.label" default="Enabled"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="enabled-label">
          
          <g:formatBoolean boolean="${versionInstance?.enabled}"/>
          
        </div>
      </div>
    </g:if>
    
  </div>


  <g:form class="mdl-card__actions mdl-card--border" action="delete" resource="${versionInstance}" namespace="admin" method="DELETE">
      <g:link action="edit" namespace="admin" resource="${versionInstance}"
              class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
        <g:message code="default.button.edit.label" default="Edit"/>
      </g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link>
  </g:form>
</div>

</body>
</html>
