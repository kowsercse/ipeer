<%@ page import="ipeer.entity.Version" %>




<div class="mdl-grid ${hasErrors(bean: versionInstance, field: 'major', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="major">
    <g:message code="version.major.label" default="Major"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:textField name="major" maxlength="128" required="" value="${versionInstance?.major}"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: versionInstance, field: 'minor', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="minor">
    <g:message code="version.minor.label" default="Minor"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:textField name="minor" maxlength="128" required="" value="${versionInstance?.minor}"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: versionInstance, field: 'buildNumber', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="buildNumber">
    <g:message code="version.buildNumber.label" default="Build Number"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:textField name="buildNumber" maxlength="128" required="" value="${versionInstance?.buildNumber}"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: versionInstance, field: 'releaseDate', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="releaseDate">
    <g:message code="version.releaseDate.label" default="Release Date"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:datePicker name="releaseDate" precision="day"  value="${versionInstance?.releaseDate}"  />
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: versionInstance, field: 'enabled', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="enabled">
    <g:message code="version.enabled.label" default="Enabled"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:checkBox name="enabled" value="${versionInstance?.enabled}" />
</div>
</div>

