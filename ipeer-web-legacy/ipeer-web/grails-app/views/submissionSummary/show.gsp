
<%@ page import="ipeer.entity.SubmissionSummary" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'submissionSummary.label', default: 'SubmissionSummary')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--6-col mdl-cell--4-col-phone">
  <div class="mdl-card__title mdl-shadow--2dp">
    <g:message code="default.show.label" args="[entityName]"/>
  </div>

  <div class="mdl-card__supporting-text">
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>

    
    <g:if test="${submissionSummaryInstance?.schedule}">
      <div class="mdl-grid">
        <div id="schedule-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="submissionSummary.schedule.label" default="Schedule"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="schedule-label">
          
          <g:link controller="schedule" namespace="admin" action="show" id="${submissionSummaryInstance?.schedule?.id}">${submissionSummaryInstance?.schedule?.encodeAsHTML()}</g:link>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${submissionSummaryInstance?.veteran}">
      <div class="mdl-grid">
        <div id="veteran-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="submissionSummary.veteran.label" default="Veteran"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="veteran-label">
          
          <g:link controller="veteran" namespace="admin" action="show" id="${submissionSummaryInstance?.veteran?.id}">${submissionSummaryInstance?.veteran?.encodeAsHTML()}</g:link>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${submissionSummaryInstance?.submissionTime}">
      <div class="mdl-grid">
        <div id="submissionTime-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="submissionSummary.submissionTime.label" default="Submission Time"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="submissionTime-label">
          
          <g:formatDate date="${submissionSummaryInstance?.submissionTime}"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${submissionSummaryInstance?.score}">
      <div class="mdl-grid">
        <div id="score-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="submissionSummary.score.label" default="Score"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="score-label">
          
          <g:fieldValue bean="${submissionSummaryInstance}" field="score"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${submissionSummaryInstance?.progressDirection}">
      <div class="mdl-grid">
        <div id="progressDirection-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="submissionSummary.progressDirection.label" default="Progress Direction"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="progressDirection-label">
          
          <g:fieldValue bean="${submissionSummaryInstance}" field="progressDirection"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${submissionSummaryInstance?.submittedBy}">
      <div class="mdl-grid">
        <div id="submittedBy-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="submissionSummary.submittedBy.label" default="Submitted By"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="submittedBy-label">
          
          <g:link controller="mentor" namespace="admin" action="show" id="${submissionSummaryInstance?.submittedBy?.id}">${submissionSummaryInstance?.submittedBy?.encodeAsHTML()}</g:link>
          
        </div>
      </div>
    </g:if>

  </div>


  <g:form class="mdl-card__actions mdl-card--border" action="delete" resource="${submissionSummaryInstance}" namespace="admin" method="DELETE">
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" controller="veteran" action="show" id="${submissionSummaryInstance.veteran.id}"><g:message code="default.list.label" args="[entityName]"/></g:link>
  </g:form>
</div>

<div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-desktop  mdl-cell--8-col-tablet  mdl-cell--6-col-phone">
  <h4>Submitted Answers</h4>

  <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
    <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">Question</th>
      <th class="mdl-data-table__cell--non-numeric">Answer</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${submissionSummaryInstance?.submittedAnswers}" status="i" var="submittedAnswerInstance">
      <tr>
        <td class="mdl-data-table__cell--non-numeric">${submittedAnswerInstance.question}</td>
        <td class="mdl-data-table__cell--non-numeric">${submittedAnswerInstance.choice}</td>
    </g:each>
    </tbody>
  </table>
</div>

</body>
</html>
