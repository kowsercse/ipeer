<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="dashboard"/>
  <title>Survey</title>
</head>

<body>

<g:if test="${currentSchedule}">
  <form method='POST' autocomplete='off' action="${createLink(mapping: 'currentSurvey')}"
        class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--9-col-tablet mdl-cell--12-col-desktop">

    <div class="mdl-card__title mdl-shadow--2dp">
      ${currentSchedule.survey.title}
    </div>

    <div class="mdl-card__supporting-text">
      <g:each in="${currentSchedule.survey.questions}" var="question">
        <div class="mdl-shadow--2dp mdl-cell mdl-cell--6-col mdl-cell--3-col-phone mdl-cell--9-col-tablet mdl-cell--9-col-desktop" style="padding: 8px 16px">
          <label class="mdl-textfield--floating-label" style="font-size: 16px; display: block">${question.title}</label>
          <g:each in="${question.choices}" var="choice">
            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect mdl-cell mdl-cell--3-col mdl-cell--3-col-phone mdl-color"
                   for="option-${question.id}-${choice.id}">
              <input type="radio" id="option-${question.id}-${choice.id}" class="mdl-radio__button"
                     name="question-${question.id}" value="${choice.id}">
              <span class="mdl-radio__label">${choice.label}</span>
            </label>
          </g:each>
        </div>
      </g:each>
    </div>

    <div class="mdl-card__actions mdl-card--border">
      <input
          class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--green-700 mdl-color-text--white"
          type='submit' id="submit" value='Submit'/>
    </div>
  </form>
</g:if>
<g:else>
  <div class="mdl-cell mdl-cell--12-col">
    Currently no survey is available.
  </div>
</g:else>

</body>
</html>
