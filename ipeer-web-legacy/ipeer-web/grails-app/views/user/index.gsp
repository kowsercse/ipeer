
<%@ page import="ipeer.entity.User" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-desktop  mdl-cell--8-col-tablet  mdl-cell--6-col-phone">
  <h1><g:message code="default.list.label" args="[entityName]"/></h1>
  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>

  <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
    <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="user.id.label" default="#"/>
      </th>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="username"
                        title="${message(code: 'user.username.label', default: 'Username')}"/>

      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="user.veteran.label" default="Veteran"/>
      </th>
      
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="user.mentor.label" default="Mentor"/>
      </th>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="accountExpired"
                        title="${message(code: 'user.accountExpired.label', default: 'Account Expired')}"/>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="accountLocked"
                        title="${message(code: 'user.accountLocked.label', default: 'Account Locked')}"/>

      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="passwordExpired"
                        title="${message(code: 'user.accountLocked.label', default: 'Password Expired')}"/>

      <th class="mdl-data-table__cell--non-numeric">Action</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${userInstanceList}" status="i" var="userInstance">
      <tr>
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${userInstance.id}">${userInstance.id}</g:link>
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          ${fieldValue(bean: userInstance, field: "username")}
          
        </td>

        <td class="mdl-data-table__cell--non-numeric">
            <g:if test="${userInstance.veteran != null}">
            <g:link namespace="admin" controller="veteran" action="show" id="${userInstance.veteran.id}">${userInstance.veteran.id}</g:link>
            </g:if>
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
            <g:if test="${userInstance.mentor != null}">
            <g:link namespace="admin" controller="mentor" action="show" id="${userInstance.mentor.id}">${userInstance.mentor.id}</g:link>
            </g:if>
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          <g:formatBoolean boolean="${userInstance.accountExpired}"/>
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          <g:formatBoolean boolean="${userInstance.accountLocked}"/>
          
        </td>

        <td class="mdl-data-table__cell--non-numeric">
          <g:formatBoolean boolean="${userInstance.passwordExpired}"/>
        </td>

        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${userInstance.id}"><i class="mdl-color-text--grey material-icons">description</i></g:link>
          <g:link namespace="admin" action="edit" id="${userInstance.id}"><i class="mdl-color-text--grey material-icons">edit</i></g:link>
        </td>
      </tr>
    </g:each>
    </tbody>
  </table>

  <div class="mdl-card__actions pagination">
    <g:link action="create" namespace="admin"
            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--green-700 mdl-color-text--white">
      <g:message code="default.new.label" args="[entityName]"/>
    </g:link>
    <g:paginate namespace="admin" controller="user" action="index" total="${userInstanceCount ?: 0}"/>
  </div>
</div>
</body>
</html>
