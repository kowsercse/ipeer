
<%@ page import="ipeer.entity.User" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--6-col mdl-cell--4-col-phone">
  <div class="mdl-card__title mdl-shadow--2dp">
    <g:message code="default.show.label" args="[entityName]"/>
  </div>

  <div class="mdl-card__supporting-text">
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>

    
    <g:if test="${userInstance?.username}">
      <div class="mdl-grid">
        <div id="username-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="user.username.label" default="Username"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="username-label">
          
          <g:fieldValue bean="${userInstance}" field="username"/>
          
        </div>
      </div>
    </g:if>

    <g:if test="${userInstance?.veteran}">
      <div class="mdl-grid">
        <div id="veteran-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="user.veteran.label" default="Veteran"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="veteran-label">
          
          <g:link controller="veteran" namespace="admin" action="show" id="${userInstance?.veteran?.id}">${userInstance?.veteran?.encodeAsHTML()}</g:link>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${userInstance?.mentor}">
      <div class="mdl-grid">
        <div id="mentor-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="user.mentor.label" default="Mentor"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="mentor-label">
          
          <g:link controller="mentor" namespace="admin" action="show" id="${userInstance?.mentor?.id}">${userInstance?.mentor?.encodeAsHTML()}</g:link>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${userInstance?.accountExpired}">
      <div class="mdl-grid">
        <div id="accountExpired-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="user.accountExpired.label" default="Account Expired"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="accountExpired-label">
          
          <g:formatBoolean boolean="${userInstance?.accountExpired}"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${userInstance?.accountLocked}">
      <div class="mdl-grid">
        <div id="accountLocked-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="user.accountLocked.label" default="Account Locked"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="accountLocked-label">
          
          <g:formatBoolean boolean="${userInstance?.accountLocked}"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${userInstance?.enabled}">
      <div class="mdl-grid">
        <div id="enabled-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="user.enabled.label" default="Enabled"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="enabled-label">
          
          <g:formatBoolean boolean="${userInstance?.enabled}"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${userInstance?.passwordExpired}">
      <div class="mdl-grid">
        <div id="passwordExpired-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="user.passwordExpired.label" default="Password Expired"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="passwordExpired-label">
          
          <g:formatBoolean boolean="${userInstance?.passwordExpired}"/>
          
        </div>
      </div>
    </g:if>
    
  </div>


  <g:form class="mdl-card__actions mdl-card--border" action="delete" resource="${userInstance}" namespace="admin" method="DELETE">
      <g:link action="edit" namespace="admin" resource="${userInstance}"
              class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
        <g:message code="default.button.edit.label" default="Edit"/>
      </g:link>
    <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" controller="user" action="changePassword" resource="${userInstance}">Change Password</g:link>
    <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link>
  </g:form>
</div>

</body>
</html>
