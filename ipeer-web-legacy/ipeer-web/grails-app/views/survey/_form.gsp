<%@ page import="ipeer.entity.Survey" %>




<div class="mdl-grid ${hasErrors(bean: surveyInstance, field: 'title', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="title">
    <g:message code="survey.title.label" default="Title"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:textField name="title" required="" value="${surveyInstance?.title}"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: surveyInstance, field: 'active', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="active">
    <g:message code="survey.active.label" default="Active"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:checkBox name="active" value="${surveyInstance?.active}" />
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: surveyInstance, field: 'questions', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="questions">
    <g:message code="survey.questions.label" default="Questions"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:select name="questions" from="${ipeer.entity.Question.list()}" multiple="multiple" optionKey="id" size="5" value="${surveyInstance?.questions*.id}" class="many-to-many"/>
</div>
</div>

