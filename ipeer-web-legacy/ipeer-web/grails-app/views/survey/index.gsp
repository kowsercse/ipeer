
<%@ page import="ipeer.entity.Survey" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'survey.label', default: 'Survey')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-desktop  mdl-cell--8-col-tablet  mdl-cell--6-col-phone">
  <h1><g:message code="default.list.label" args="[entityName]"/></h1>
  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>

  <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
    <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="survey.id.label" default="#"/>
      </th>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="title"
                        title="${message(code: 'survey.title.label', default: 'Title')}"/>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="active"
                        title="${message(code: 'survey.active.label', default: 'Active')}"/>
      
      <th class="mdl-data-table__cell--non-numeric">Action</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${surveyInstanceList}" status="i" var="surveyInstance">
      <tr>
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${surveyInstance.id}">${surveyInstance.id}</g:link>
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          ${fieldValue(bean: surveyInstance, field: "title")}
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          <g:formatBoolean boolean="${surveyInstance.active}"/>
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${surveyInstance.id}"><i class="mdl-color-text--grey material-icons">description</i></g:link>
          <g:link namespace="admin" action="edit" id="${surveyInstance.id}"><i class="mdl-color-text--grey material-icons">edit</i></g:link>
        </td>
      </tr>
    </g:each>
    </tbody>
  </table>

  <div class="mdl-card__actions pagination">
    <g:link action="create" namespace="admin"
            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--green-700 mdl-color-text--white">
      <g:message code="default.new.label" args="[entityName]"/>
    </g:link>
    <g:paginate namespace="admin" controller="survey" action="index" total="${surveyInstanceCount ?: 0}"/>
  </div>
</div>
</body>
</html>
