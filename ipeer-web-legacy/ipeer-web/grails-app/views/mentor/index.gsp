
<%@ page import="ipeer.entity.Mentor" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'mentor.label', default: 'Mentor')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-desktop  mdl-cell--8-col-tablet  mdl-cell--6-col-phone">
  <h1><g:message code="default.list.label" args="[entityName]"/></h1>
  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>

  <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
    <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="mentor.id.label" default="#"/>
      </th>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="firstName"
                        title="${message(code: 'mentor.firstName.label', default: 'First Name')}"/>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="lastName"
                        title="${message(code: 'mentor.lastName.label', default: 'Last Name')}"/>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="phone"
                        title="${message(code: 'mentor.phone.label', default: 'Phone')}"/>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="email"
                        title="${message(code: 'mentor.email.label', default: 'Email')}"/>
      
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="mentor.user.label" default="User"/>
      </th>
      
      <th class="mdl-data-table__cell--non-numeric">Action</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${mentorInstanceList}" status="i" var="mentorInstance">
      <tr>
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${mentorInstance.id}">${mentorInstance.id}</g:link>
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          ${fieldValue(bean: mentorInstance, field: "firstName")}
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          ${fieldValue(bean: mentorInstance, field: "lastName")}
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          ${fieldValue(bean: mentorInstance, field: "phone")}
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          ${fieldValue(bean: mentorInstance, field: "email")}
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
            <g:if test="${mentorInstance.user != null}">
            <g:link namespace="admin" controller="user" action="show" id="${mentorInstance.user.id}">${mentorInstance.user.toString()}</g:link>
            </g:if>
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${mentorInstance.id}"><i class="mdl-color-text--grey material-icons">description</i></g:link>
          <g:link namespace="admin" action="edit" id="${mentorInstance.id}"><i class="mdl-color-text--grey material-icons">edit</i></g:link>
        </td>
      </tr>
    </g:each>
    </tbody>
  </table>

  <div class="mdl-card__actions pagination">
    <g:link action="create" namespace="admin"
            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--green-700 mdl-color-text--white">
      <g:message code="default.new.label" args="[entityName]"/>
    </g:link>
    <g:paginate namespace="admin" controller="mentor" action="index" total="${mentorInstanceCount ?: 0}"/>
  </div>
</div>
</body>
</html>
