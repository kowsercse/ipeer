
<%@ page import="ipeer.entity.Mentor" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'mentor.label', default: 'Mentor')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--6-col mdl-cell--4-col-phone">
  <div class="mdl-card__title mdl-shadow--2dp">
    <g:message code="default.show.label" args="[entityName]"/>
  </div>

  <div class="mdl-card__supporting-text">
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>

    
    <g:if test="${mentorInstance?.firstName}">
      <div class="mdl-grid">
        <div id="firstName-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="mentor.firstName.label" default="First Name"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="firstName-label">
          
          <g:fieldValue bean="${mentorInstance}" field="firstName"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${mentorInstance?.lastName}">
      <div class="mdl-grid">
        <div id="lastName-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="mentor.lastName.label" default="Last Name"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="lastName-label">
          
          <g:fieldValue bean="${mentorInstance}" field="lastName"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${mentorInstance?.phone}">
      <div class="mdl-grid">
        <div id="phone-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="mentor.phone.label" default="Phone"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="phone-label">
          
          <g:fieldValue bean="${mentorInstance}" field="phone"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${mentorInstance?.email}">
      <div class="mdl-grid">
        <div id="email-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="mentor.email.label" default="Email"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="email-label">
          
          <g:fieldValue bean="${mentorInstance}" field="email"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${mentorInstance?.user}">
      <div class="mdl-grid">
        <div id="user-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="mentor.user.label" default="User"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="user-label">
          
          <g:link controller="user" namespace="admin" action="show" id="${mentorInstance?.user?.id}">${mentorInstance?.user?.encodeAsHTML()}</g:link>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${mentorInstance?.veterans}">
      <div class="mdl-grid">
        <div id="veterans-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="mentor.veterans.label" default="Veterans"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="veterans-label">
          
          <g:each in="${mentorInstance.veterans}" var="v">
            <g:link controller="veteran" namespace="admin" action="show" id="${v.id}">${v?.encodeAsHTML()}</g:link>
          </g:each>
          
        </div>
      </div>
    </g:if>
    
  </div>


  <g:form class="mdl-card__actions mdl-card--border" action="delete" resource="${mentorInstance}" namespace="admin" method="DELETE">
      <g:link action="edit" namespace="admin" resource="${mentorInstance}"
              class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
        <g:message code="default.button.edit.label" default="Edit"/>
      </g:link>
    <g:if test="${mentorInstance.user != null}">
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" controller="user" action="changePassword" id="${mentorInstance.user.id}">Change Password</g:link>
    </g:if>

    <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link>
  </g:form>
</div>

</body>
</html>
