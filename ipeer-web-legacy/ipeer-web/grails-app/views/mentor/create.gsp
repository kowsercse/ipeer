<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'mentor.label', default: 'Mentor')}"/>
  <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>


<g:form class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--8-col mdl-cell--4-col-phone mdl-cell--4-col-tablet mdl-cell--8-col-desktop"
        action="save" namespace="admin" >
  <div class="mdl-card__title mdl-shadow--2dp">
    <g:message code="default.create.label" args="[entityName]"/>
  </div>

  <div class="mdl-card__supporting-text">
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${mentorInstance}">
      <ul class="errors" role="alert">
      <g:eachError bean="${mentorInstance}" var="error">
        <li<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>>
        <g:message error="${error}"/>
        </li>
      </g:eachError>
      </ul>
    </g:hasErrors>

    <div class="mdl-grid">
      <div class="mdl-cell mdl-cell--3-col"><label for="username">Username</label></div>
      <div class="mdl-cell mdl-cell--3-col"><g:textField name="username" required=""/></div>
    </div>

    <g:render template="form"/>
  </div>

  <div class="mdl-card__actions mdl-card--border">
    <g:submitButton name="create" value="${message(code: 'default.button.create.label', default: 'Create')}"
                    class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--green-700 mdl-color-text--white"/>
    <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" action="index" namespace="admin">
      Cancel
    </g:link>
  </div>
</g:form>

</body>
</html>
