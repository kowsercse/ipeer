<%@ page import="ipeer.entity.Veteran" %>




<div class="mdl-grid ${hasErrors(bean: veteranInstance, field: 'user', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="user">
    <g:message code="veteran.user.label" default="User"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:select id="user" name="user.id" from="${ipeer.entity.User.list()}" optionKey="id" value="${veteranInstance?.user?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: veteranInstance, field: 'mentor', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="mentor">
    <g:message code="veteran.mentor.label" default="Mentor"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:select id="mentor" name="mentor.id" from="${ipeer.entity.Mentor.list()}" optionKey="id" value="${veteranInstance?.mentor?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: veteranInstance, field: 'startDate', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="startDate">
    <g:message code="veteran.startDate.label" default="Start Date"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:datePicker name="startDate" precision="day"  value="${veteranInstance?.startDate}"  />
</div>
</div>
