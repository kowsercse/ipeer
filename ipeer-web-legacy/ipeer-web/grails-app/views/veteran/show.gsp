
<%@ page import="ipeer.entity.Veteran" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'veteran.label', default: 'Veteran')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--6-col mdl-cell--4-col-phone">
  <div class="mdl-card__title mdl-shadow--2dp">
    <g:message code="default.show.label" args="[entityName]"/>
  </div>

  <div class="mdl-card__supporting-text">
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>

    
    <g:if test="${veteranInstance?.user}">
      <div class="mdl-grid">
        <div id="user-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="veteran.user.label" default="User"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="user-label">
          
          <g:link controller="user" namespace="admin" action="show" id="${veteranInstance?.user?.id}">${veteranInstance?.user?.toString()?.encodeAsHTML()}</g:link>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${veteranInstance?.mentor}">
      <div class="mdl-grid">
        <div id="mentor-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="veteran.mentor.label" default="Mentor"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="mentor-label">
          
          <g:link controller="mentor" namespace="admin" action="show" id="${veteranInstance?.mentor?.id}">${veteranInstance?.mentor?.toString()?.encodeAsHTML()}</g:link>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${veteranInstance?.startDate}">
      <div class="mdl-grid">
        <div id="startDate-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="veteran.startDate.label" default="Start Date"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="startDate-label">
          
          <g:formatDate date="${veteranInstance?.startDate}"/>
          
        </div>
      </div>
    </g:if>

  </div>


  <g:form class="mdl-card__actions mdl-card--border" action="delete" resource="${veteranInstance}" namespace="admin" method="DELETE">
      <g:link action="edit" namespace="admin" resource="${veteranInstance}"
              class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
        <g:message code="default.button.edit.label" default="Edit"/>
      </g:link>
    <g:if test="${veteranInstance.user != null}">
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" controller="user" action="changePassword" id="${veteranInstance.user.id}">Change Password</g:link>
    </g:if>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link>
  </g:form>
</div>

<div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-desktop  mdl-cell--8-col-tablet  mdl-cell--6-col-phone">
  <h4>Survey Submissions</h4>

  <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
    <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="submissionSummary.id.label" default="#"/>
      </th>

      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="submissionSummary.schedule.label" default="Schedule"/>
      </th>

      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="submissionSummary.veteran.label" default="Veteran"/>
      </th>

      <th class="mdl-data-table__cell--non-numeric">Submission Time</th>

      <th class="mdl-data-table__cell--non-numeric">Score</th>

      <th class="mdl-data-table__cell--non-numeric">Progress Direction</th>

      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="submissionSummary.submittedBy.label" default="Submitted By"/>
      </th>

      <th class="mdl-data-table__cell--non-numeric">Action</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${veteranInstance?.submissionSummaries}" status="i" var="submissionSummaryInstance">
      <tr>
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${submissionSummaryInstance.id}">${submissionSummaryInstance.id}</g:link>
        </td>

        <td class="mdl-data-table__cell--non-numeric">



          <g:link namespace="admin" controller="schedule" action="show" id="${submissionSummaryInstance.schedule.id}">${submissionSummaryInstance.schedule}</g:link>


        </td>

        <td class="mdl-data-table__cell--non-numeric">



          <g:link namespace="admin" controller="veteran" action="show" id="${submissionSummaryInstance.veteran.id}">${submissionSummaryInstance.veteran}</g:link>


        </td>

        <td class="mdl-data-table__cell--non-numeric">


          <g:formatDate date="${submissionSummaryInstance.submissionTime}"/>

        </td>

        <td class="mdl-data-table__cell--non-numeric">


          ${fieldValue(bean: submissionSummaryInstance, field: "score")}

        </td>

        <td class="mdl-data-table__cell--non-numeric">


          ${fieldValue(bean: submissionSummaryInstance, field: "progressDirection")}

        </td>

        <td class="mdl-data-table__cell--non-numeric">


          <g:if test="${submissionSummaryInstance.submittedBy != null}">
            <g:link namespace="admin" controller="mentor" action="show" id="${submissionSummaryInstance.submittedBy.id}">${submissionSummaryInstance.submittedBy}</g:link>
          </g:if>

        </td>

        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" controller="submissionSummary" action="show" id="${submissionSummaryInstance.id}"><i class="mdl-color-text--grey material-icons">description</i></g:link>
        </td>
      </tr>
    </g:each>
    </tbody>
  </table>

</div>

</body>
</html>
