
<%@ page import="ipeer.entity.Veteran" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'veteran.label', default: 'Veteran')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-desktop  mdl-cell--8-col-tablet  mdl-cell--6-col-phone">
  <h1>My Veterans</h1>
  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>

  <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
    <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="veteran.id.label" default="#"/>
      </th>
      
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="veteran.user.label" default="User"/>
      </th>
      
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="veteran.mentor.label" default="Mentor"/>
      </th>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="startDate"
                        title="${message(code: 'veteran.startDate.label', default: 'Start Date')}"/>
      
      <th class="mdl-data-table__cell--non-numeric">Action</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${veteranInstanceList}" status="i" var="veteranInstance">
      <tr>
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${veteranInstance.id}">${veteranInstance.id}</g:link>
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
            <g:if test="${veteranInstance.user != null}">
            <g:link namespace="admin" controller="user" action="show" id="${veteranInstance.user.id}">${veteranInstance.user}</g:link>
            </g:if>
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
            <g:if test="${veteranInstance.mentor != null}">
            <g:link namespace="admin" controller="mentor" action="show" id="${veteranInstance.mentor.id}">${veteranInstance.mentor}</g:link>
            </g:if>
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          <g:formatDate date="${veteranInstance.startDate}"/>
          
        </td>

        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${veteranInstance.id}"><i class="mdl-color-text--grey material-icons">description</i></g:link>
          <g:link namespace="admin" action="edit" id="${veteranInstance.id}"><i class="mdl-color-text--grey material-icons">edit</i></g:link>
        </td>
      </tr>
    </g:each>
    </tbody>
  </table>

  <div class="mdl-card__actions pagination">
    <g:link action="create" namespace="admin"
            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--green-700 mdl-color-text--white">
      <g:message code="default.new.label" args="[entityName]"/>
    </g:link>
    <g:paginate namespace="admin" controller="veteran" action="index" total="${veteranInstanceCount ?: 0}"/>
  </div>
</div>
</body>
</html>
