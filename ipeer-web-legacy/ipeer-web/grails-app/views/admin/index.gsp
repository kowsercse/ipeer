<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="dashboard"/>
  <title>IPeer</title>
</head>

<body>
<div id="controller-list" role="navigation">
  <h2>Download Apps</h2>
  <ul>
    <li><a class="mdl-color-text--green-700" href="${createLink(mapping: 'downloadMentor', id: versionInstance.id, absolute: true)}">mentor-${versionInstance}.apk</a></li>
    <li><a class="mdl-color-text--green-700" href="${createLink(mapping: 'downloadMentee', id: versionInstance.id, absolute: true)}">veteran-${versionInstance}.apk</a></li>
  </ul>
</div>
</body>
</html>