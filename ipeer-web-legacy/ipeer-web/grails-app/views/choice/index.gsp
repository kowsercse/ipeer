
<%@ page import="ipeer.entity.Choice" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'choice.label', default: 'Choice')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-desktop  mdl-cell--8-col-tablet  mdl-cell--6-col-phone">
  <h1><g:message code="default.list.label" args="[entityName]"/></h1>
  <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
  </g:if>

  <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
    <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric">
        <g:message code="choice.id.label" default="#"/>
      </th>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="label"
                        title="${message(code: 'choice.label.label', default: 'Label')}"/>
      
      <g:sortableColumn namespace="admin" action="index" class="mdl-data-table__cell--non-numeric" property="value"
                        title="${message(code: 'choice.value.label', default: 'Value')}"/>
      
      <th class="mdl-data-table__cell--non-numeric">Action</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${choiceInstanceList}" status="i" var="choiceInstance">
      <tr>
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${choiceInstance.id}">${choiceInstance.id}</g:link>
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          ${fieldValue(bean: choiceInstance, field: "label")}
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
        
          
          ${fieldValue(bean: choiceInstance, field: "value")}
          
        </td>
        
        <td class="mdl-data-table__cell--non-numeric">
          <g:link namespace="admin" action="show" id="${choiceInstance.id}"><i class="mdl-color-text--grey material-icons">description</i></g:link>
          <g:link namespace="admin" action="edit" id="${choiceInstance.id}"><i class="mdl-color-text--grey material-icons">edit</i></g:link>
        </td>
      </tr>
    </g:each>
    </tbody>
  </table>

  <div class="mdl-card__actions pagination">
    <g:link action="create" namespace="admin"
            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--green-700 mdl-color-text--white">
      <g:message code="default.new.label" args="[entityName]"/>
    </g:link>
    <g:paginate namespace="admin" controller="choice" action="index" total="${choiceInstanceCount ?: 0}"/>
  </div>
</div>
</body>
</html>
