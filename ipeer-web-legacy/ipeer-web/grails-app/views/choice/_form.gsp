<%@ page import="ipeer.entity.Choice" %>




<div class="mdl-grid ${hasErrors(bean: choiceInstance, field: 'label', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="label">
    <g:message code="choice.label.label" default="Label"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:textField name="label" required="" value="${choiceInstance?.label}"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: choiceInstance, field: 'value', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="value">
    <g:message code="choice.value.label" default="Value"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:field name="value" type="number" value="${choiceInstance.value}" required=""/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: choiceInstance, field: 'questions', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="questions">
    <g:message code="choice.questions.label" default="Questions"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col">
</div>
</div>

