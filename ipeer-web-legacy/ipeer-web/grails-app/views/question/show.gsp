
<%@ page import="ipeer.entity.Question" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="dashboard">
  <g:set var="entityName" value="${message(code: 'question.label', default: 'Question')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--6-col mdl-cell--4-col-phone">
  <div class="mdl-card__title mdl-shadow--2dp">
    <g:message code="default.show.label" args="[entityName]"/>
  </div>

  <div class="mdl-card__supporting-text">
    <g:if test="${flash.message}">
      <div class="message" role="status">${flash.message}</div>
    </g:if>

    
    <g:if test="${questionInstance?.title}">
      <div class="mdl-grid">
        <div id="title-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="question.title.label" default="Title"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="title-label">
          
          <g:fieldValue bean="${questionInstance}" field="title"/>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${questionInstance?.choices}">
      <div class="mdl-grid">
        <div id="choices-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="question.choices.label" default="Choices"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="choices-label">
          
          <g:each in="${questionInstance.choices}" var="c">
            <g:link controller="choice" namespace="admin" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link>
          </g:each>
          
        </div>
      </div>
    </g:if>
    
    <g:if test="${questionInstance?.surveys}">
      <div class="mdl-grid">
        <div id="surveys-label" class="mdl-cell mdl-cell--4-col">
          <g:message code="question.surveys.label" default="Surveys"/>
        </div>
        <div class="mdl-cell mdl-cell--4-col" aria-labelledby="surveys-label">
          
          <g:each in="${questionInstance.surveys}" var="s">
            <g:link controller="survey" namespace="admin" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link>
          </g:each>
          
        </div>
      </div>
    </g:if>
    
  </div>


  <g:form class="mdl-card__actions mdl-card--border" action="delete" resource="${questionInstance}" namespace="admin" method="DELETE">
      <g:link action="edit" namespace="admin" resource="${questionInstance}"
              class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
        <g:message code="default.button.edit.label" default="Edit"/>
      </g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link>
      <g:link class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" namespace="admin" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link>
  </g:form>
</div>

</body>
</html>
