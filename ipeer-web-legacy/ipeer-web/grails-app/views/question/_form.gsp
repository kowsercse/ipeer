<%@ page import="ipeer.entity.Question" %>




<div class="mdl-grid ${hasErrors(bean: questionInstance, field: 'title', 'error')} required">
  <div class="mdl-cell mdl-cell--3-col">  <label for="title">
    <g:message code="question.title.label" default="Title"/>
    <span class="required-indicator">*</span>
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:textField name="title" required="" value="${questionInstance?.title}"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: questionInstance, field: 'choices', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="choices">
    <g:message code="question.choices.label" default="Choices"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col"><g:select name="choices" from="${ipeer.entity.Choice.list()}" multiple="multiple" optionKey="id" size="5" value="${questionInstance?.choices*.id}" class="many-to-many"/>
</div>
</div>


<div class="mdl-grid ${hasErrors(bean: questionInstance, field: 'surveys', 'error')} ">
  <div class="mdl-cell mdl-cell--3-col">  <label for="surveys">
    <g:message code="question.surveys.label" default="Surveys"/>
    
  </label>
  </div>
  <div class="mdl-cell mdl-cell--3-col">
</div>
</div>

