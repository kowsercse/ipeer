package ipeer.entity

class Role {

	static final ROLE_MENTOR = "ROLE_MENTOR"
	static final ROLE_VETERAN = "ROLE_VETERAN"
	static final ROLE_SUPER_ADMIN = "ROLE_SUPER_ADMIN"

	String authority

	static mapping = {
    version false
		cache true
	}

	static constraints = {
		authority blank: false, unique: true
	}

	@Override
	String toString() {
		authority
	}

}
