package ipeer.entity

class Issue {

  String errorLog;
  String summary
  String detail
  User createdBy
  Boolean autoReported
  Date dateCreated
  Date lastUpdated

  static mapping = {
    version false
  }

  static constraints = {
    errorLog nullable: true
  }
}
