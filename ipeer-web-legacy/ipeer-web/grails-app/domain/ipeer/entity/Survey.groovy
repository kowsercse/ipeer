package ipeer.entity

class Survey {

  String title
  Boolean active

  static hasMany = [questions: Question]

  static mapping = {
    version false
  }

  static constraints = {
    title nullable: false
  }

  @Override
  String toString() {
    title
  }

}
