package ipeer.entity

class Veteran {

  Mentor mentor
  Date startDate
  User user

  static hasMany = [submissionSummaries: SubmissionSummary]

  static mapping = {
    version false
    sort 'mentor'
  }

  static constraints = {
    user nullable: true, unique: true
    mentor nullable: true
    startDate nullable: false
  }

  @Override
  String toString() {
    id
  }

}
