package ipeer.entity

class User {

	transient springSecurityService

	String username
	String password
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
	String resetToken
	Date tokenGenerationTime
	Date dateCreated
	Date lastUpdated

  static hasOne = [veteran: Veteran, mentor: Mentor]

	static transients = ['springSecurityService']

	static constraints = {
		username blank: false, unique: true
		password blank: true
		resetToken nullable: true, unique: true
		tokenGenerationTime nullable: true
    veteran nullable: true, unique: true
    mentor nullable: true, unique: true
	}

	static mapping = {
    version false
    password column: '`password`'
	}

	@Override
	def String toString() {
		username
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role }
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}
}
