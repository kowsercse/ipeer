package ipeer.entity

class Schedule {

  Date start
  Date end
  Survey survey

  static mapping = {
    version false
  }

  static constraints = {
    start nullable: false
    end nullable: false
    survey nullable: false
  }

  boolean equals(o) {
    if (this.is(o)) return true
    if (getClass() != o.class) return false

    Schedule schedule = (Schedule) o

    if (id != schedule.id) return false

    return true
  }

  int hashCode() {
    return (id != null ? id.hashCode() : 0)
  }

  @Override
  public String toString() {
    return start.format("dd MMM, yy") + " - " + end.format("dd MMM, yy");
  }
}
