package ipeer.entity

class SubmittedAnswer {

  Question question
  Choice choice
  SubmissionSummary submissionSummary;

  static mapping = {
    version false
  }

  static constraints = {
    question nullable: false
    choice nullable: false
    submissionSummary nullable: false, unique: 'question'
  }

}
