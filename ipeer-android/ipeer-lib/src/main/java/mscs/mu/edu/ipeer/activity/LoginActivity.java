package mscs.mu.edu.ipeer.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import mscs.mu.edu.ipeer.domain.Endpoint;
import mscs.mu.edu.ipeer.lib.R;
import mscs.mu.edu.ipeer.net.*;
import mscs.mu.edu.ipeer.service.Application;
import mscs.mu.edu.ipeer.service.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static mscs.mu.edu.ipeer.activity.SplashScreenActivity.AUTH_HEADER;

public class LoginActivity extends BaseActivity {
  private static final String USERNAME = "username";
  private final Logger logger = LoggerFactory.getLogger(getClass());

  private TextView usernameTextField;
  private TextView passwordTextField;
  private SharedPreferences preferences;

  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);

    usernameTextField = (TextView) findViewById(R.id.username_text_field);
    passwordTextField = (TextView) findViewById(R.id.password_text_field);
    preferences = getSharedPreferences("mscs.mu.edu.ubicomp.ipeer.config", MODE_PRIVATE);

    final String savedUsername = preferences.getString(USERNAME, null);
    usernameTextField.setText(TextUtils.isEmpty(savedUsername) ? "" : savedUsername);
    passwordTextField.requestFocus();

    findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {
        String username = usernameTextField.getText().toString();
        String password = passwordTextField.getText().toString();
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
          login(username, password);
        }
      }
    });

    findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {
        finish();
      }
    });
  }

  private void login(final String username, final String password) {
    final Application application = (Application) getApplication();
    final Client client = application.getClient();
    client.setAuthenticationHeader("Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP));
    client.getEndpoint(Config.getInstance().getBaseUrl(), new Callback<Endpoint>() {
      @Override
      public void onCompleted(Endpoint endpoint) {
        application.setEndpoint(endpoint);
        preferences.edit().putString(USERNAME, username).apply();
        preferences.edit().putString("version-uri", endpoint.getVersionLink().getHref()).apply();
        preferences.edit().putString(AUTH_HEADER, null).apply();
        Intent intent = new Intent(LoginActivity.this, Config.getInstance().getMainIntent());
        finish();
        startActivity(intent);
      }
    }, new FailureCallback() {
      @Override
      public void onFail(final int statusCode, final String message) {
        Toast.makeText(LoginActivity.this, "Login failed: " + message, Toast.LENGTH_LONG).show();

        passwordTextField.setText("");
        usernameTextField.requestFocus();
        preferences.edit().putString(USERNAME, "").apply();
      }
    });
  }

}
