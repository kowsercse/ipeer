package mscs.mu.edu.ipeer.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import mscs.mu.edu.ipeer.lib.R;

public abstract class BaseActivity extends AppCompatActivity {

  public final boolean onCreateOptionsMenu(final Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public final boolean onOptionsItemSelected(final MenuItem item) {
    if(item.getItemId() == R.id.about) {
      final Intent intent = new Intent(this, AboutActivity.class);
      startActivity(intent);
    }
    return true;
  }

}
