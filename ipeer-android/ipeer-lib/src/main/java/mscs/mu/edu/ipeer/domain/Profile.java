package mscs.mu.edu.ipeer.domain;

public class Profile extends JsonDomain {

  private String username;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Link getMentorLink() {
    return getLink(LinkRelations.MENTOR);
  }

  public Link getVeteranLink() {
    return getLink(LinkRelations.VETERAN);
  }

}
