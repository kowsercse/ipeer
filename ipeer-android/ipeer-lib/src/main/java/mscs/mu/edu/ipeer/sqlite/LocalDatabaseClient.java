package mscs.mu.edu.ipeer.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import mscs.mu.edu.ipeer.domain.Veteran;

public class LocalDatabaseClient extends SQLiteOpenHelper {
  private static final int DATABASE_VERSION = 1;
  private static final String DATABASE_NAME = "iPeer.db";

  public LocalDatabaseClient(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    String sql = "CREATE TABLE " + LocalVeteran.TABLE_VETERAN + "("
        + LocalVeteran.ID + " TEXT PRIMARY KEY,"
        + LocalVeteran.RED_CAP_USER_ID + " TEXT,"
        + LocalVeteran.FIRST_NAME + " TEXT,"
        + LocalVeteran.LAST_NAME + " TEXT,"
        + LocalVeteran.PHONE + " TEXT,"
        + LocalVeteran.IMAGE + " TEXT,"
        + LocalVeteran.EMAIL + " TEXT)";
    db.execSQL(sql);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
  }

  public LocalVeteran getLocalVeteran(final Veteran veteran) {
    Uri veteranUri = Uri.parse(veteran.getSelf().getHref());
    LocalVeteran localVeteran = getLocalVeteran(veteranUri.getLastPathSegment());
    if (localVeteran != null) {
      localVeteran.setVeteran(veteran);
    }

    return localVeteran;
  }

  public LocalVeteran getLocalVeteran(final String localVeteranId) {
    Cursor cursor = getReadableDatabase().query(
        LocalVeteran.TABLE_VETERAN,
        LocalVeteran.COLUMNS,
        LocalVeteran.ID + " = ?",
        new String[]{localVeteranId},
        null, null, null, null
    );
    if (cursor != null && cursor.moveToFirst() && cursor.getCount() != 0) {
      return getLocalVeteran(cursor);
    }

    return null;
  }

  public void addVeteran(LocalVeteran veteran) {
    if(getLocalVeteran(veteran.getId()) != null) {
      deleteLocalVeteran(veteran.getId());
    }
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = prepareContentValues(veteran);
    db.insert(LocalVeteran.TABLE_VETERAN, null, values);
    db.close();
  }

  public void updateVeteran(LocalVeteran localVeteran) {
    ContentValues values = prepareContentValues(localVeteran);
    final String[] whereArgs = {String.valueOf(localVeteran.getId())};
    final String whereClause = LocalVeteran.ID + " = ?";
    getWritableDatabase().update(LocalVeteran.TABLE_VETERAN, values, whereClause, whereArgs);
  }

  public void deleteLocalVeteran(final String localVeteranId) {
    getWritableDatabase().delete(LocalVeteran.TABLE_VETERAN, LocalVeteran.ID + "=" + localVeteranId, null);
  }

  private LocalVeteran getLocalVeteran(final Cursor cursor) {
    LocalVeteran localVeteran = new LocalVeteran(cursor.getString(cursor.getColumnIndex(LocalVeteran.ID)));
    localVeteran.setRedCapUserId(cursor.getString(cursor.getColumnIndex(LocalVeteran.RED_CAP_USER_ID)));
    localVeteran.setFirstName(cursor.getString(cursor.getColumnIndex(LocalVeteran.FIRST_NAME)));
    localVeteran.setLastName(cursor.getString(cursor.getColumnIndex(LocalVeteran.LAST_NAME)));
    localVeteran.setPhone(cursor.getString(cursor.getColumnIndex(LocalVeteran.PHONE)));
    localVeteran.setEmail(cursor.getString(cursor.getColumnIndex(LocalVeteran.EMAIL)));
    return localVeteran;
  }

  private ContentValues prepareContentValues(final LocalVeteran veteran) {
    ContentValues values = new ContentValues();
    values.put(LocalVeteran.ID, veteran.getId());
    values.put(LocalVeteran.RED_CAP_USER_ID, veteran.getRedCapUserId());
    values.put(LocalVeteran.FIRST_NAME, veteran.getFirstName());
    values.put(LocalVeteran.LAST_NAME, veteran.getLastName());
    values.put(LocalVeteran.PHONE, veteran.getPhone());
    values.put(LocalVeteran.EMAIL, veteran.getEmail());
    return values;
  }
}
