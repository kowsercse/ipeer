package mscs.mu.edu.ipeer.domain.adapter;

import mscs.mu.edu.ipeer.domain.*;

import java.util.List;

public class SubmittedSurvey {
  private final Schedule schedule;
  private final SubmissionSummary submissionSummary;
  private List<SubmittedAnswer> answers;
  private Survey survey;

  public SubmittedSurvey(final Schedule schedule, final SubmissionSummary submissionSummary) {
    this.schedule = schedule;
    this.submissionSummary = submissionSummary;
  }

  public Schedule getSchedule() {
    return schedule;
  }

  public SubmissionSummary getSubmissionSummary() {
    return submissionSummary;
  }

  public List<SubmittedAnswer> getAnswers() {
    return answers;
  }

  public void setAnswers(List<SubmittedAnswer> answers) {
    this.answers = answers;
  }

  public void setSurvey(Survey survey) {
    this.survey = survey;
  }

  public Survey getSurvey() {
    return survey;
  }

  public float getScore() {
    return submissionSummary == null ? 0.0f : (float) submissionSummary.getScore() / 5;
  }
}
