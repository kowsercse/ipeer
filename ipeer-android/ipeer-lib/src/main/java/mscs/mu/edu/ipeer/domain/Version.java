package mscs.mu.edu.ipeer.domain;

import java.util.Date;

public class Version extends JsonDomain {
  private String major;
  private String minor;
  private String buildNumber;
  private boolean enabled;
  private Date releaseDate;

  private String getMajor() {
    return major;
  }

  private void setMajor(final String major) {
    this.major = major;
  }

  private String getMinor() {
    return minor;
  }

  private void setMinor(final String minor) {
    this.minor = minor;
  }

  private String getBuildNumber() {
    return buildNumber;
  }

  private void setBuildNumber(final String buildNumber) {
    this.buildNumber = buildNumber;
  }

  private boolean isEnabled() {
    return enabled;
  }

  private void setEnabled(final boolean enabled) {
    this.enabled = enabled;
  }

  private Date getReleaseDate() {
    return releaseDate;
  }

  private void setReleaseDate(final Date releaseDate) {
    this.releaseDate = releaseDate;
  }

  public Link getMentorAppLink() {
    return getLink(LinkRelations.MENTOR_APP);
  }

  public Link getMenteeAppLink() {
    return getLink(LinkRelations.MENTEE_APP);
  }

  @Override
  public String toString() {
    return String.format("%s.%s.%s", major, minor, buildNumber);
  }
}
