package mscs.mu.edu.ipeer.domain;

import java.util.Date;

public class SubmissionSummary extends JsonDomain {

  private int score;
  private Date submissionTime;
  private int progressDirection;

  public int getScore() {
    return score;
  }

  public void setScore(final int score) {
    this.score = score;
  }

  public Date getSubmissionTime() {
    return submissionTime;
  }

  public void setSubmissionTime(Date submissionTime) {
    this.submissionTime = submissionTime;
  }

  public int getProgressDirection() {
    return progressDirection;
  }

  public void setProgressDirection(final int progressDirection) {
    this.progressDirection = progressDirection;
  }

  public Link getScheduleLink() {
    return getLink(LinkRelations.SCHEDULE);
  }

  public Link getVeteranLink() {
    return getLink(LinkRelations.VETERAN);
  }

  public Link getSubmittedAnswersLink() {
    return getLink(LinkRelations.SUBMITTED_ANSWER);
  }

}
