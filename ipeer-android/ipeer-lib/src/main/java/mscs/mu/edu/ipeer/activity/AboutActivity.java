package mscs.mu.edu.ipeer.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import mscs.mu.edu.ipeer.lib.R;
import mscs.mu.edu.ipeer.service.Application;
import mscs.mu.edu.ipeer.service.Config;

public class AboutActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_about);
    ((TextView)findViewById(R.id.version_text_view)).setText(Config.getInstance().getVersion());
    ((TextView)findViewById(R.id.documentation_uri)).setText(Config.getInstance().getDocumentationUrl().toASCIIString());
    ((TextView)findViewById(R.id.home_uri)).setText(Config.getInstance().getHomeUri().toASCIIString());
  }
}
