package mscs.mu.edu.ipeer.net;

import mscs.mu.edu.ipeer.domain.*;

import java.io.InputStream;
import java.net.URI;
import java.util.List;

public interface Client {

  void setAuthenticationHeader(final String authenticationHeader);

  void getEndpoint(final URI endpointUri, final Callback<Endpoint> callback, final FailureCallback failureCallback);

  void getProfile(final Link profileLink, final Callback<Profile> callback);

  void getMentor(final Link mentorLink, final Callback<Mentor> callback);

  void getVeterans(final Link veteransLink, final Callback<List<Veteran>> callback);

  void getVeteran(final Link veteranLink, final Callback<Veteran> callback);

  void addVeteran(final Link link, final Veteran veteran, Callback<Veteran> callback, final FailureCallback failureCallback);

  void getSchedules(final Link schedulesLink, final Callback<List<Schedule>> callback);

  void getSchedule(final Link scheduleLink, final Callback<Schedule> callback);

  void getSubmissionSummaries(final Link submissionSummariesLink, final Callback<List<SubmissionSummary>> callback, final FailureCallback failureCallback);

  void getQuestions(final Link questionsLink, final Callback<List<Question>> callback);

  void getChoices(final Link choicesLink, final Callback<List<Choice>> callback);

  void getSubmittedAnswers(Link submittedAnswersLink, Callback<List<SubmittedAnswer>> callback);

  void getSurveys(Link surveysLink, Callback<List<Survey>> callback);

  void getSurvey(final Link surveyLink, final Callback<Survey> callback);

  void postSubmittedSurvey(final Link link, SubmittedSurvey submittedSurvey, final Callback<SubmissionSummary> callback, final FailureCallback failureCallback);

  void getResetToken(final Link resetLink, final Callback<String> callback, final FailureCallback failureCallback);

  void getVersion(Link versionLink, Callback<Version> callback);

  void getApk(Link apkLink, Callback<InputStream> callback);

  void getAuthToken(Link authTokenLink, Callback<AuthenticationToken> callback, FailureCallback failureCallback);

  void getRefreshToken(Link refreshTokenLink, Callback<RefreshToken> callback);

  void getRefreshToken(Link refreshTokenLink, Callback<RefreshToken> callback, FailureCallback failureCallback);
}
