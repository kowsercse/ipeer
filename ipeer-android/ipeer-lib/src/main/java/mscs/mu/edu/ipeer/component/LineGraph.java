package mscs.mu.edu.ipeer.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.*;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import mscs.mu.edu.ipeer.domain.Schedule;
import mscs.mu.edu.ipeer.domain.SubmissionSummary;
import mscs.mu.edu.ipeer.domain.adapter.SubmittedSurvey;
import mscs.mu.edu.ipeer.lib.R;

import java.util.ArrayList;
import java.util.List;

public class LineGraph extends ViewGroup {
  private List<SubmittedSurvey> submittedSurveys = new ArrayList<>();
  private Paint linePaint;
  private Paint circlePaint;

  private int lineColor;
  private float lineWidth = 2.0f;
  private float circleRadius = 5.0f;

  private Rect bound;
  private final int internalPadding = 10;
  private final int totalStep = 30;
  private Callback callback;

  public void setCallback(final Callback callback) {
    this.callback = callback;
  }

  public LineGraph(final Context context) {
    super(context);
    lineColor = getResources().getColor(getResources().getColor(R.color.olive1));
    init();
  }

  public LineGraph(final Context context, final AttributeSet attrs) {
    super(context, attrs);
    initFromAttributes(context, attrs);
    init();
  }

  private void init() {
    setWillNotDraw(false);
    bound = new Rect(0, 0, getSuggestedMinimumWidth(), getSuggestedMinimumHeight());
    linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    linePaint.setColor(lineColor);
    linePaint.setStrokeWidth(lineWidth);

    circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    circlePaint.setColor(lineColor);
    circlePaint.setStrokeWidth(circleRadius);

    if (this.isInEditMode()) {
      SubmissionSummary submissionSummary = new SubmissionSummary();
      submissionSummary.setScore(0);
      add(new SubmittedSurvey(new Schedule(), submissionSummary));
      for (int i = 0; i < 6; i++) {
        submissionSummary = new SubmissionSummary();
        submissionSummary.setScore(8 + i * 2 * (i % 2 == 0 ? 1 : -1));
        add(new SubmittedSurvey(new Schedule(), submissionSummary));
      }
    }
  }

  @Override
  protected void onLayout(final boolean changed, final int l, final int t, final int r, final int b) {
  }

  @Override
  protected void onDraw(final Canvas canvas) {
    super.onDraw(canvas);

    if (getChildCount() > 1) {
      final int xInc = (int) ((bound.width() - 2 * (internalPadding + circleRadius)) / 5);
      final int yInc = (int) ((bound.height() - 2 * (internalPadding + circleRadius)) / totalStep);

      final float[] points = new float[(getChildCount() - 1) * 4];
      points[0] = getPaddingLeft() + internalPadding + circleRadius;
      points[1] = bound.height() - (getPaddingBottom() + internalPadding + yInc * Math.max(0, submittedSurveys.get(0).getScore() * 10) + circleRadius);
      points[2] = points[0] + xInc;
      points[3] = bound.height() - (getPaddingBottom() + internalPadding + yInc * Math.max(0, submittedSurveys.get(1).getScore() * 10) + circleRadius);

      for (int i = 1; i < getChildCount() - 1; i++) {
        points[i * 4] = points[(i - 1) * 4 + 2];
        points[i * 4 + 1] = points[(i - 1) * 4 + 3];
        points[i * 4 + 2] = points[i * 4] + xInc;
        points[i * 4 + 3] = bound.height() - (getPaddingBottom() + internalPadding + yInc * Math.max(0, submittedSurveys.get(i + 1).getScore() * 10) + circleRadius);
      }
      canvas.drawLines(points, linePaint);
    }
  }

  @Override
  protected int getSuggestedMinimumWidth() {
    return (int) (getSuggestedMinimumHeight() * 1.62);
  }

  @Override
  protected int getSuggestedMinimumHeight() {
    return 90;
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    int heightMode = MeasureSpec.getMode(heightMeasureSpec);
    int heightSize = MeasureSpec.getSize(heightMeasureSpec);

    int minWidth = getPaddingLeft() + getPaddingRight() + getSuggestedMinimumWidth();
    int measuredWidth = Math.max(minWidth, MeasureSpec.getSize(widthMeasureSpec));

    int minHeight = getPaddingBottom() + getPaddingTop() + getSuggestedMinimumHeight();
    final int measuredHeight;

    if(heightMode == MeasureSpec.EXACTLY) {
      measuredHeight = heightSize;
    }
    else if(heightMode == MeasureSpec.AT_MOST) {
      measuredHeight = Math.min(heightSize, minHeight);
    }
    else {
      measuredHeight = Math.max(MeasureSpec.getSize(heightMeasureSpec), minHeight);
    }

    setChildPosition();
    setMeasuredDimension(measuredWidth, measuredHeight);
  }

  @Override
  protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    super.onSizeChanged(w, h, oldw, oldh);

    bound = new Rect(0, 0, w - getPaddingLeft() + getPaddingRight(), h - getPaddingTop() + getPaddingBottom());
    setChildPosition();
  }

  private void setChildPosition() {
    if (getChildCount() > 0) {
      final int xInc = (int) ((bound.width() - (internalPadding + circleRadius) * 2) / 5);
      final int yInc = (int) ((bound.height() - (internalPadding + circleRadius) * 2) / totalStep);

      int left = getPaddingLeft() + internalPadding;
      int right = (int) (internalPadding + 2 * circleRadius);
      int bottom = bound.height() - (getPaddingBottom() + internalPadding + (int) (yInc * Math.max(0.0f, submittedSurveys.get(0).getScore() * 10)));
      int top = (int) (bottom - 2 * circleRadius);
      getChildAt(0).layout(left, top, right, bottom);

      for (int i = 1; i < getChildCount(); i++) {
        left += xInc;
        right += xInc;
        bottom = bound.height() - (getPaddingBottom() + internalPadding + (int) (yInc * Math.max(0.0f, submittedSurveys.get(i).getScore() * 10)));
        top = (int) (bottom - 2 * circleRadius);
        getChildAt(i).layout(left, top, right, bottom);
      }
    }
  }

  public void add(final SubmittedSurvey submittedSurvey) {
    if (submittedSurveys.size() == 6) {
      return;
    }
    submittedSurveys.add(submittedSurvey);
    final SubmittedSurveyView surveyView = new SubmittedSurveyView(getContext());
    addView(surveyView);
    surveyView.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(final View v) {
        if (callback != null) {
          callback.surveySelected(submittedSurvey);
        }
      }
    });
  }

  public void addAll(final List<SubmittedSurvey> submittedSurveys) {
    if (submittedSurveys != null) {
      for (final SubmittedSurvey submittedSurvey : submittedSurveys) {
        add(submittedSurvey);
      }
    }
  }

  private void initFromAttributes(final Context context, final AttributeSet attrs) {
    TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.LineGraph, 0, 0);
    try {
      lineColor = typedArray.getColor(R.styleable.LineGraph_lineColor, getResources().getColor(R.color.olive1));
      lineWidth = typedArray.getDimension(R.styleable.LineGraph_lineWidth, lineWidth);
      circleRadius = typedArray.getDimension(R.styleable.LineGraph_circleRadius, circleRadius);
    } finally {
      typedArray.recycle();
    }
  }

  public void clear() {
    removeAllViews();
    submittedSurveys.clear();
  }

  private class SubmittedSurveyView extends View {
    public SubmittedSurveyView(final Context context) {
      super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
      canvas.drawCircle(circleRadius, circleRadius, circleRadius, circlePaint);
    }
  }

  public interface Callback {
    void surveySelected(SubmittedSurvey submittedSurvey);
  }

}
