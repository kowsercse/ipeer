package mscs.mu.edu.ipeer.domain;

public class SubmittedAnswer extends JsonDomain {

  public Link getQuestionLink() {
    return getLink(LinkRelations.QUESTION);
  }

  public Link getChoiceLink() {
    return getLink(LinkRelations.CHOICE);
  }

}
