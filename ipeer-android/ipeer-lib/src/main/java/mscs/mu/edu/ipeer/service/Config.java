package mscs.mu.edu.ipeer.service;

import android.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URI;
import java.util.*;

public class Config {
  public static final String MENTOR = "mentor";
  public static final String MENTEE = "veteran";

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private Class<?> mainIntent;
  private String prefix;

  private static Config INSTANCE = new Config();
  private final String buildVersion;
  private final String minorVersion;
  private final String majorVersion;
  private final URI documentationUrl;
  private final URI baseUrl;
  private final String version;
  private final URI homeUri;

  public static Config getInstance() {
    return INSTANCE;
  }

  private Config() {
    final Properties properties = loadConfigurations();

    majorVersion = properties.getProperty("ipeer.config.version.major");
    minorVersion = properties.getProperty("ipeer.config.version.minor");
    buildVersion = properties.getProperty("ipeer.config.version.build");
    final String home = properties.getProperty("ipeer.config.url.home");
    homeUri = URI.create(home);
    baseUrl = URI.create(home + "/webservice");
    documentationUrl = URI.create(home + "/documentation");
    version = String.format("%s.%s.%s", majorVersion, minorVersion, buildVersion);

    initializeAppInformation();
  }

  public final URI getBaseUrl() {
    return baseUrl;
  }

  public URI getHomeUri() {
    return homeUri;
  }

  public final URI getDocumentationUrl() {
    return documentationUrl;
  }

  public final String getMajorVersion() {
    return majorVersion;
  }

  public final String getMinorVersion() {
    return minorVersion;
  }

  public final String getBuildVersion() {
    return buildVersion;
  }

  public String getVersion() {
    return version;
  }

  public Class<?> getMainIntent() {
    return mainIntent;
  }

  public String getPrefix() {
    return prefix;
  }

  private Properties loadConfigurations() {
    final Properties properties = new Properties();
    final List<String> configurations = Arrays.asList("config", "prod", "test", "dev");
    for (final String configuration : configurations) {
      String resourceName = File.separator + configuration + ".properties";
      try {
        final InputStream inputStream = this.getClass().getResourceAsStream(resourceName);
        if (inputStream != null) {
          properties.load(inputStream);
          inputStream.close();
        }
      } catch (IOException e) {
        Log.d(getClass().getName(), "Failed to read configuration: " + configuration, e);
      }
    }

    return properties;
  }

  private void initializeAppInformation() {
    try {
      mainIntent = Class.forName("mscs.mu.edu.ipeer.mentor.activity.MainActivity");
      prefix = MENTOR;
    } catch (ClassNotFoundException e) {
      logger.debug("Mentor main activity not found.");
    }
    try {
      mainIntent = Class.forName("mscs.mu.edu.ipeer.mentee.activity.MainActivity");
      prefix = MENTEE;
    } catch (ClassNotFoundException e) {
      logger.debug("Mentee main activity not found.");
    }

    if(mainIntent == null) {
      logger.debug("The application is not installed properly");
    }
  }

}
