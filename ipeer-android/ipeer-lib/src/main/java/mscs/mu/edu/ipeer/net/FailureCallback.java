package mscs.mu.edu.ipeer.net;

public interface FailureCallback {
  void onFail(final int statusCode, final String message);
}
