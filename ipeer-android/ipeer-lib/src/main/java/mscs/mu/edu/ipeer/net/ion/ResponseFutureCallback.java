package mscs.mu.edu.ipeer.net.ion;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;
import mscs.mu.edu.ipeer.net.Callback;
import mscs.mu.edu.ipeer.net.FailureCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

class ResponseFutureCallback<T> implements FutureCallback<Response<T>> {
  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final Callback<T> callback;
  private final URI uri;
  private final FailureCallback failureCallback;

  ResponseFutureCallback(final URI uri, final Callback<T> callback) {
    this(uri, callback, null);
  }

  ResponseFutureCallback(final URI uri, final Callback<T> callback, final FailureCallback failureCallback) {
    this.callback = callback;
    this.uri = uri;
    this.failureCallback = failureCallback;
  }

  @Override
  public void onCompleted(final Exception exception, final Response<T> response) {
    if (exception == null && response.getHeaders().code() < 400) {
      pauseSinceWebAppIsNotAbleToHandleTooManyRequest();
      callback.onCompleted(response.getResult());
    } else {
      logger.warn("Failed to process the request for: " + uri.toASCIIString() + " " + (response != null ? response.getHeaders().code() : 0), exception);
      if (failureCallback != null) {
        failureCallback.onFail(response != null ? response.getHeaders().code() : 0, exception != null ? exception.getMessage() : "Failed to process the request");
      }
    }
  }

  private void pauseSinceWebAppIsNotAbleToHandleTooManyRequest() {
    try {
      Thread.sleep(200);
    } catch (InterruptedException e) {
      logger.warn("Failed to pause the application after successful network request");
    }
  }
}
