package mscs.mu.edu.ipeer.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author kowsercse@gmail.com
 */
public class TokenUtil {

  private static final String PREFIX = "QRF-";
  private static final Pattern TOKEN_PATTERN = Pattern.compile("QRF-([A-Za-z0-9\\-]+)");

  public static String encodeToken(final String token) {
    return PREFIX + token;
  }

  public static String decodeToken(final String encodedToken) {
    final Matcher matcher = TOKEN_PATTERN.matcher(encodedToken);
    return matcher.matches() ? matcher.group(1) : null;
  }

}
