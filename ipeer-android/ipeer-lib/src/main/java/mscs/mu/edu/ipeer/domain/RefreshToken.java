package mscs.mu.edu.ipeer.domain;

public class RefreshToken extends JsonDomain {

  private String token;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Link getAuthenticationTokenLink() {
    return getLink(LinkRelations.AUTH_TOKEN);
  }

}
