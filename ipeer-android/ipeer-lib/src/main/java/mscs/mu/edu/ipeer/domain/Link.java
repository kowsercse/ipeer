package mscs.mu.edu.ipeer.domain;

public class Link {
  private String rel;
  private String href;

  public Link() {
  }

  public Link(final String href) {
    this(LinkRelations.SELF, href);
  }

  public Link(final String rel, final String href) {
    this.rel = rel;
    this.href = href;
  }

  public String getRel() {
    return rel;
  }

  public void setRel(String rel) {
    this.rel = rel;
  }

  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Link link = (Link) o;

    return href.equals(link.href);
  }

  @Override
  public int hashCode() {
    return href.hashCode();
  }

  @Override
  public String toString() {
    return (rel == null ? "" : rel + ": ") + href;
  }

}
