package mscs.mu.edu.ipeer.util;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.*;
import android.content.pm.PackageManager;
import android.telephony.SmsManager;
import android.widget.Toast;

import java.util.ArrayList;

public class AndroidUtils {

  public static void sendSMS(String phoneNumber, String message, Context context) {
    if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
      return;
    }
    ArrayList<PendingIntent> sentPendingIntents = new ArrayList<>();
    ArrayList<PendingIntent> deliveredPendingIntents = new ArrayList<>();
    PendingIntent smsSentListenerIntent = PendingIntent.getBroadcast(context, 0, new Intent(context, SmsSentReceiver.class), 0);
    PendingIntent smsSendListenerIntent = PendingIntent.getBroadcast(context, 0, new Intent(context, SmsDeliveredReceiver.class), 0);

    SmsManager sms = SmsManager.getDefault();
    ArrayList<String> devidedMessages = sms.divideMessage(message);
    for (int i = 0; i < devidedMessages.size(); i++) {
      sentPendingIntents.add(i, smsSentListenerIntent);
      deliveredPendingIntents.add(i, smsSendListenerIntent);
    }
    sms.sendMultipartTextMessage(phoneNumber, null, devidedMessages, sentPendingIntents, deliveredPendingIntents);
  }

  public class SmsDeliveredReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      switch (getResultCode()) {
        case Activity.RESULT_OK:
          Toast.makeText(context, "SMS delivered", Toast.LENGTH_SHORT).show();
          break;
        case Activity.RESULT_CANCELED:
          Toast.makeText(context, "SMS not delivered", Toast.LENGTH_SHORT).show();
          break;
      }
    }
  }

  public class SmsSentReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      switch (getResultCode()) {
        case Activity.RESULT_OK:
          Toast.makeText(context, "SMS Sent", Toast.LENGTH_SHORT).show();
          break;
        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
          Toast.makeText(context, "SMS generic failure", Toast.LENGTH_SHORT).show();
          break;
        case SmsManager.RESULT_ERROR_NO_SERVICE:
          Toast.makeText(context, "SMS no service", Toast.LENGTH_SHORT).show();
          break;
        case SmsManager.RESULT_ERROR_NULL_PDU:
          Toast.makeText(context, "SMS null PDU", Toast.LENGTH_SHORT).show();
          break;
        case SmsManager.RESULT_ERROR_RADIO_OFF:
          Toast.makeText(context, "SMS radio off", Toast.LENGTH_SHORT).show();
          break;
      }
    }
  }
}
