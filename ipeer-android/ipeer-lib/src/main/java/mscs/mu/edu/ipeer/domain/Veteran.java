package mscs.mu.edu.ipeer.domain;

import java.util.Date;

public class Veteran extends JsonDomain {

  private Date startDate;

  private String redCapUserId;

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(final Date startDate) {
    this.startDate = startDate;
  }

  public String getRedCapUserId() {
    return redCapUserId;
  }

  public void setRedCapUserId(String redCapUserId) {
    this.redCapUserId = redCapUserId;
  }

  public Link getMentorLink() {
    return getLink(LinkRelations.MENTOR);
  }

  public Link getSchedulesLink() {
    return getLink(LinkRelations.SCHEDULE);
  }

  public Link getSubmissionSummariesLink() {
    return getLink(LinkRelations.SUBMISSION_SUMMARY);
  }

  public Link getResetTokenLink() {
    return getLink(LinkRelations.RESET_TOKEN);
  }

  public Link getRefreshTokenLink() {
    return getLink(LinkRelations.REFRESH_TOKEN);
  }

  public Link getProfileLink() {
    return getLink(LinkRelations.PROFILE);
  }

  @Override
  public String toString() {
    return getSelf().toString();
  }

}
