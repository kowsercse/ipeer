package mscs.mu.edu.ipeer.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.*;

import java.util.Calendar;
import java.util.Random;

public class BootBroadcastReceiver extends BroadcastReceiver {

  @Override
  public void onReceive(final Context context, final Intent intent) {
    registerApkDownloader(context);
  }

  public static void registerApkDownloader(final Context context) {
    Intent apkDownloaderIntent = new Intent(context, ApkDownloader.class);
    final PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, apkDownloaderIntent, PendingIntent.FLAG_CANCEL_CURRENT);

    long triggerTimeInMillis = getTriggerTimeInMillis();
    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, triggerTimeInMillis, AlarmManager.INTERVAL_DAY, pendingIntent);
  }

  private static long getTriggerTimeInMillis() {
    final int minute = new Random().nextInt(60);

    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, minute);
    return calendar.getTimeInMillis();
  }

}
