package mscs.mu.edu.ipeer.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtility {

  public static String getShortDate(Date date) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM, dd", Locale.US);
    return dateFormat.format(date);
  }

  public static String getSimpleDate(Date date) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd", Locale.US);
    return dateFormat.format(date);
  }

  public static String getWeekDay(Date date) {
    SimpleDateFormat weekDayFormat = new SimpleDateFormat("EEEE", Locale.US);
    return weekDayFormat.format(date);
  }

  public static String getFullDateTime(Date date) {
    SimpleDateFormat weekDayFormat = new SimpleDateFormat("EEE, MMM d, yyyy h:mm a", Locale.US);
    return weekDayFormat.format(date);
  }

}
