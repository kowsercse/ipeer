package mscs.mu.edu.ipeer.domain;

import java.util.List;

public class JsonDomain {

  private List<Link> links;

  public Link getSelf() {
    return getLink(LinkRelations.SELF);
  }

  public Link getLink(String relation) {
    for (Link link : links) {
      if (link.getRel().equalsIgnoreCase(relation)) {
        return link;
      }
    }

    return null;
  }

  public List<Link> getLinks() {
    return links;
  }

  public void setLinks(List<Link> links) {
    this.links = links;
  }

  @Override
  public String toString() {
    return getSelf().toString();
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    final JsonDomain that = (JsonDomain) o;

    return getSelf().equals(that.getSelf());
  }

  @Override
  public int hashCode() {
    return getSelf().hashCode();
  }
}
