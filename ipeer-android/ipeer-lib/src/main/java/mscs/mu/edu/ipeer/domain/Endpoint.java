package mscs.mu.edu.ipeer.domain;

public class Endpoint extends JsonDomain {

  public Link getProfileLink() {
    return getLink(LinkRelations.PROFILE);
  }

  public Link getSurveysLink() {
    return getLink(LinkRelations.SURVEY);
  }

  public Link getQuestionsLink() {
    return getLink(LinkRelations.QUESTION);
  }

  public Link getChoicesLink() {
    return getLink(LinkRelations.CHOICE);
  }

  public Link getVersionLink() {
    return getLink(LinkRelations.VERSION);
  }
}
