package mscs.mu.edu.ipeer.domain;

public class Survey extends JsonDomain {

  private String title;
  private boolean active;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public Link getQuestionsLink() {
    return getLink(LinkRelations.QUESTION);
  }

  @Override
  public String toString() {
    return title;
  }

}
