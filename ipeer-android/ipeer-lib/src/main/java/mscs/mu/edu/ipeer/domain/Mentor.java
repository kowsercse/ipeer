package mscs.mu.edu.ipeer.domain;

public class Mentor extends JsonDomain {
  private String firstName;
  private String lastName;
  private String phone;

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Link getVeteransLink() {
    return getLink(LinkRelations.VETERAN);
  }

}
