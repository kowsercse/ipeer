package mscs.mu.edu.ipeer.net.ion;

import com.google.gson.reflect.TypeToken;
import com.koushikdutta.ion.Ion;
import mscs.mu.edu.ipeer.domain.*;
import mscs.mu.edu.ipeer.net.*;
import mscs.mu.edu.ipeer.service.Application;

import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.net.URI;
import java.util.List;

public class IonClient implements Client {

  private final Ion ion;
  private String authenticationHeader;
  private Application application;

  @Override
  public void setAuthenticationHeader(final String authenticationHeader) {
    this.authenticationHeader = authenticationHeader;
  }

  public IonClient(Application application) {
    ion = Ion.getDefault(application);
    this.application = application;
  }

  @Override
  public void getEndpoint(final URI endpointUri, final Callback<Endpoint> callback, final FailureCallback failureCallback) {
    execute(endpointUri, callback, failureCallback);
  }

  @Override
  public void getProfile(Link profileLink, Callback<Profile> callback) {
    execute(profileLink, callback);
  }

  @Override
  public void getMentor(Link mentorLink, Callback<Mentor> callback) {
    execute(mentorLink, callback);
  }

  @Override
  public void getVeterans(Link veteransLink, Callback<List<Veteran>> callback) {
    execute(veteransLink, callback);
  }

  @Override
  public void getVeteran(Link veteranLink, Callback<Veteran> callback) {
    execute(veteranLink, callback);
  }

  @Override
  public void addVeteran(final Link veteransCollectionLink, final Veteran veteran, final Callback<Veteran> callback, final FailureCallback failureCallback) {
    executePost(veteransCollectionLink, veteran, callback, failureCallback);
  }

  @Override
  public void getSchedules(Link schedulesLink, Callback<List<Schedule>> callback) {
    execute(schedulesLink, callback);
  }

  @Override
  public void getSchedule(final Link scheduleLink, final Callback<Schedule> callback) {
    execute(scheduleLink, callback);
  }

  @Override
  public void getSubmissionSummaries(Link submissionSummariesLink, Callback<List<SubmissionSummary>> callback, final FailureCallback failureCallback) {
    execute(submissionSummariesLink, callback, failureCallback);
  }

  @Override
  public void getQuestions(final Link questionsLink, final Callback<List<Question>> callback) {
    execute(questionsLink, callback);
  }

  @Override
  public void getChoices(final Link choicesLink, final Callback<List<Choice>> callback) {
    execute(choicesLink, callback);
  }

  @Override
  public void getSubmittedAnswers(Link submittedAnswersLink, Callback<List<SubmittedAnswer>> callback) {
    execute(submittedAnswersLink, callback);
  }

  @Override
  public void getRefreshToken(Link refreshTokenLink, Callback<RefreshToken> callback) {
    execute(refreshTokenLink, callback);
  }

  @Override
  public void getRefreshToken(Link refreshTokenLink, Callback<RefreshToken> callback, FailureCallback failureCallback) {
    execute(refreshTokenLink, callback, failureCallback);
  }

  @Override
  public void getSurveys(Link surveysLink, Callback<List<Survey>> callback) {
    execute(surveysLink, callback);
  }

  @Override
  public void getVersion(final Link versionLink, final Callback<Version> callback) {
    execute(versionLink, callback);
  }

  @Override
  public void getSurvey(final Link surveyLink, final Callback<Survey> callback) {
    if (application.getSurveys().containsKey(surveyLink)) {
      callback.onCompleted(application.getSurveys().get(surveyLink));
    } else {
      execute(surveyLink, callback);
    }
  }

  @Override
  public void getApk(final Link apkLink, final Callback<InputStream> callback) {
    ion.build(ion.getContext())
        .load(Method.GET.toString(), apkLink.getHref())
        .setHeader("Authorization", authenticationHeader)
        .asInputStream()
        .withResponse()
        .setCallback(new ResponseFutureCallback<>(URI.create(apkLink.getHref()), callback));
  }

  @Override
  public void getAuthToken(final Link authTokenLink, final Callback<AuthenticationToken> callback, final FailureCallback failureCallback) {
    execute(authTokenLink, callback, failureCallback);
  }

  @Override
  public void postSubmittedSurvey(final Link link, final SubmittedSurvey submittedSurvey, final Callback<SubmissionSummary> callback, final FailureCallback failureCallback) {
    ion.build(ion.getContext())
        .load(Method.POST.toString(), link.getHref())
        .setHeader("Authorization", authenticationHeader)
        .setJsonPojoBody(submittedSurvey)
        .as(SubmissionSummary.class)
        .withResponse()
        .setCallback(new ResponseFutureCallback<>(URI.create(link.getHref()), callback, failureCallback));
  }

  @Override
  public void getResetToken(final Link resetLink, final Callback<String> callback, final FailureCallback failureCallback) {
    ion.build(ion.getContext())
        .load(Method.GET.toString(), resetLink.getHref())
        .setHeader("Authorization", authenticationHeader)
        .asString()
        .withResponse()
        .setCallback(new ResponseFutureCallback<>(URI.create(resetLink.getHref()), callback, failureCallback));
  }

  private <T> void execute(Link link, Callback<T> callback) {
    execute(link, callback, null);
  }

  private <T> void execute(Link link, Callback<T> callback, final FailureCallback failureCallback) {
    execute(URI.create(link.getHref()), callback, failureCallback);
  }

  private <T> void execute(final URI uri, final Callback<T> callback, final FailureCallback failureCallback) {
    TypeToken<T> typeToken = extractTypeToken(callback);
    ion.build(ion.getContext())
        .load(Method.GET.toString(), uri.toASCIIString())
        .setHeader("Authorization", authenticationHeader)
        .as(typeToken)
        .withResponse()
        .setCallback(new ResponseFutureCallback<>(uri, callback, failureCallback));
  }

  private <T> void executePost(final Link link, Object body, final Callback<T> callback, final FailureCallback failureCallback) {
    TypeToken<T> typeToken = extractTypeToken(callback);
    ion.build(ion.getContext())
        .load(Method.POST.toString(), link.getHref())
        .setHeader("Authorization", authenticationHeader)
        .setJsonPojoBody(body)
        .as(typeToken)
        .withResponse()
        .setCallback(new ResponseFutureCallback<>(URI.create(link.getHref()), callback, failureCallback));
  }

  private <T> TypeToken<T> extractTypeToken(final Callback<T> callback) {
    final Class<? extends Callback> clazz = callback.getClass();
    final ParameterizedType genericSuperclass = (ParameterizedType) (clazz.getGenericInterfaces().length != 0
        ? clazz.getGenericInterfaces()[0]
        : clazz.getGenericSuperclass());
    @SuppressWarnings("unchecked")
    TypeToken<T> typeToken = (TypeToken<T>) TypeToken.get(genericSuperclass.getActualTypeArguments()[0]);
    return typeToken;
  }

}
