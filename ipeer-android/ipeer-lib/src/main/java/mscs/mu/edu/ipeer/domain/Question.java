package mscs.mu.edu.ipeer.domain;

public class Question extends JsonDomain {

  private String title;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Link getChoicesLink() {
    return getLink(LinkRelations.CHOICE);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Question question = (Question) o;

    return !(getSelf() != null ? !getSelf().equals(question.getSelf()) : question.getSelf() != null);
  }

  @Override
  public int hashCode() {
    return getSelf() != null ? getSelf().hashCode() : 0;
  }

  @Override
  public String toString() {
    return title;
  }

}
