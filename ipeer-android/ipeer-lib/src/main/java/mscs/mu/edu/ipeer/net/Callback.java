package mscs.mu.edu.ipeer.net;

public interface Callback<R> {

  void onCompleted(final R result);

}
