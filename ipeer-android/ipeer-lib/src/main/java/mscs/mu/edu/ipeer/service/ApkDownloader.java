package mscs.mu.edu.ipeer.service;

import android.annotation.TargetApi;
import android.app.*;
import android.content.*;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import mscs.mu.edu.ipeer.domain.*;
import mscs.mu.edu.ipeer.lib.R;
import mscs.mu.edu.ipeer.net.Callback;
import mscs.mu.edu.ipeer.net.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class ApkDownloader extends BroadcastReceiver {

  private static final String EXTENSION = "apk";
  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public void onReceive(final Context context, final Intent intent) {
    final Application application = (Application) context.getApplicationContext();
    final Client client = application.getClient();
    final Endpoint endpoint = application.getEndpoint();
    final SharedPreferences sharedPreferences = application.getSharedPreferences("mscs.mu.edu.ubicomp.ipeer.config", Context.MODE_PRIVATE);

    final String versionUri = sharedPreferences.getString("version-uri", endpoint != null ? endpoint.getVersionLink().getHref() : null);
    if(versionUri != null) {
      client.getVersion(new Link(versionUri), new Callback<Version>() {
        @Override
        public void onCompleted(final Version version) {
          if (!Config.getInstance().getVersion().equals(version.toString())) {
            final String prefix = Config.getInstance().getPrefix();
            final File file = getOutputFile(version, prefix);
            if (!file.exists()) {
              final Link apkLink = prefix.equals(Config.MENTOR) ? version.getMentorAppLink() : version.getMenteeAppLink();
              client.getApk(apkLink, new Callback<InputStream>() {
                @Override
                public void onCompleted(final InputStream inputStream) {
                  try {
                    writeDownloadedFile(file, inputStream);
                    showInstallNotification(context, file);
                  } catch (IOException e) {
                    logger.debug("Failed to save apk: " + file.getAbsolutePath());
                  }
                }
              });
            } else {
              showInstallNotification(context, file);
            }
          }
        }
      });
    }
  }

  @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
  private void showInstallNotification(final Context context, final File apk) {
    final Intent intent = new Intent(Intent.ACTION_VIEW)
        .setDataAndType(Uri.fromFile(apk), "application/vnd.android.package-archive")
        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

    final int icon = Config.getInstance().getPrefix().equals(Config.MENTOR) ? R.drawable.mentor : R.drawable.mentee;
    final PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    final Notification.Builder builder = new Notification.Builder(context)
        .setSmallIcon(icon)
        .setContentTitle("New update downloaded")
        .setContentText(apk.getName() + " is ready to be installed")
        .setAutoCancel(false)
        .setContentIntent(pendingIntent);

    @SuppressWarnings("deprecation")
    final Notification notification = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN ?
        builder.build() : builder.getNotification();
    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    notificationManager.notify(0, notification);
  }

  private File getOutputFile(final Version version, final String prefix) {
    String fileName = String.format("%s-%s.%s", prefix, version, EXTENSION);
    return new File(Environment.getExternalStorageDirectory(), fileName);
  }

  private void writeDownloadedFile(final File output, final InputStream inputStream) throws IOException {
    FileOutputStream fileOutputStream = null;
    try {
      fileOutputStream = new FileOutputStream(output.getPath());
      byte data[] = new byte[4096];
      int count;
      while ((count = inputStream.read(data)) != -1) {
        fileOutputStream.write(data, 0, count);
        fileOutputStream.flush();
      }
    }
    catch (final IOException ex) {
      logger.debug("Error while saving apk: " + output.getAbsolutePath(), ex);
    }
    finally {
      if(fileOutputStream != null) {
        fileOutputStream.close();
      }
      if(inputStream != null) {
        inputStream.close();
      }
    }
  }

}
