package mscs.mu.edu.ipeer.domain;

/**
 * @author kowsercse@gmail.com
 */
public class AuthenticationToken {

  private String tokenType;
  private String accessToken;

  public String getTokenType() {
    return tokenType;
  }

  public void setTokenType(final String tokenType) {
    this.tokenType = tokenType;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(final String accessToken) {
    this.accessToken = accessToken;
  }

  public String getAuthenticationHeader() {
    return tokenType + " " + accessToken;
  }
}
