package mscs.mu.edu.ipeer.activity;

import android.Manifest;
import android.app.Activity;
import android.content.*;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import mscs.mu.edu.ipeer.domain.*;
import mscs.mu.edu.ipeer.lib.R;
import mscs.mu.edu.ipeer.net.*;
import mscs.mu.edu.ipeer.service.Application;
import mscs.mu.edu.ipeer.service.Config;
import mscs.mu.edu.ipeer.util.TokenUtil;

import java.util.concurrent.TimeUnit;

public class SplashScreenActivity extends Activity {
  public static final String AUTH_HEADER = "token";
  private static final int REQUEST_CODE = 100;

  private Client client;
  private SharedPreferences preferences;
  private Application application;

  @Override
  public void onCreate(Bundle icicle) {
    super.onCreate(icicle);
    setContentView(R.layout.activity_splash_screen);
    application = (Application) getApplication();
    client = application.getClient();
    preferences = getSharedPreferences("mscs.mu.edu.ubicomp.ipeer.config", MODE_PRIVATE);

    login();
  }

  private void login() {
    final FailureCallback failureCallback = new FailureCallback() {
      @Override
      public void onFail(final int statusCode, final String message) {
        new Handler().postDelayed(new Runnable() {
          @Override
          public void run() {
            Intent mainIntent = new Intent(SplashScreenActivity.this, LoginActivity.class);
            startActivity(mainIntent);
            finish();
          }
        }, TimeUnit.MILLISECONDS.toMillis(500));
      }
    };

    loginUsingToken(preferences.getString(AUTH_HEADER, null), new FailureCallback() {
      @Override
      public void onFail(final int statusCode, final String message) {
        client.setAuthenticationHeader(null);
        final Link authTokenLink = getAuthenticationTokenLink();
        client.getAuthToken(authTokenLink, new Callback<AuthenticationToken>() {
          @Override
          public void onCompleted(final AuthenticationToken token) {
            loginUsingToken(token.getAuthenticationHeader(), failureCallback);
          }
        }, failureCallback);
      }
    });
  }

  private Link getAuthenticationTokenLink() {
    final String refreshToken = readRefreshTokenFromTextMessage();
    final String tokenUrl = Config.getInstance().getBaseUrl() + "/profile/token/" + refreshToken;
    return new Link(tokenUrl);
  }

  private void loginUsingToken(final String token, final FailureCallback failureCallback) {
    client.setAuthenticationHeader(token);
    client.getEndpoint(Config.getInstance().getBaseUrl(), new Callback<Endpoint>() {
      @Override
      public void onCompleted(Endpoint endpoint) {
        preferences.edit().putString(AUTH_HEADER, token).apply();
        application.setEndpoint(endpoint);
        Intent intent = new Intent(SplashScreenActivity.this, Config.getInstance().getMainIntent());
        finish();
        startActivity(intent);
      }
    }, failureCallback);
  }

  private String readRefreshTokenFromTextMessage() {
    if (!isReadSmsPermission()) {
      ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, REQUEST_CODE);
    }

    if (!isReadSmsPermission()) {
      return null;
    }

    Uri uri = Uri.parse("content://sms/inbox");
    final ContentResolver contentResolver = this.getContentResolver();
    final Cursor cursor = contentResolver.query(uri, null, null, null, null);

    if (cursor != null) {
      startManagingCursor(cursor);
      cursor.moveToLast();

      int total = 10;
      while (cursor.moveToPrevious() && --total > 0) {
        final String message = cursor.getString(cursor.getColumnIndexOrThrow("body"));
        final String token = TokenUtil.decodeToken(message);
        if(token != null) {
          return token;
        }
      }
    }

    return null;
  }

  private boolean isReadSmsPermission() {
    return ContextCompat.checkSelfPermission(getBaseContext(), "android.permission.READ_SMS") == PackageManager.PERMISSION_GRANTED;
  }

}
