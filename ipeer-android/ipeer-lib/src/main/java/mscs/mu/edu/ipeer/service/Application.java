package mscs.mu.edu.ipeer.service;

import mscs.mu.edu.ipeer.domain.*;
import mscs.mu.edu.ipeer.domain.adapter.SubmittedSurvey;
import mscs.mu.edu.ipeer.net.Client;
import mscs.mu.edu.ipeer.net.ion.IonClient;
import mscs.mu.edu.ipeer.sqlite.LocalDatabaseClient;

import java.util.*;

public class Application extends android.app.Application {

  private Client client;
  private LocalDatabaseClient localDatabaseClient;

  private Endpoint endpoint;
  private final Map<Link, Survey> surveys = new HashMap<>();
  private final Map<Link, Question> questions = new HashMap<>();
  private final Map<Link, Choice> choices = new HashMap<>();
  private final Map<Veteran, List<SubmittedSurvey>> veteransSurveySubmissions = new HashMap<>();
  private final Map<Veteran, SubmittedSurvey> veteransLatestSubmissions = new HashMap<>();

  @Override
  public void onCreate() {
    super.onCreate();
    client = new IonClient(this);
    localDatabaseClient = new LocalDatabaseClient(getApplicationContext());
    BootBroadcastReceiver.registerApkDownloader(this);
  }

  public Client getClient() {
    return client;
  }

  public LocalDatabaseClient getLocalDatabaseClient() {
    return localDatabaseClient;
  }

  public void setEndpoint(Endpoint endpoint) {
    this.endpoint = endpoint;
  }

  public Endpoint getEndpoint() {
    return endpoint;
  }

  public Map<Link, Survey> getSurveys() {
    return surveys;
  }

  public Map<Link, Question> getQuestions() {
    return questions;
  }

  public Map<Link, Choice> getChoices() {
    return choices;
  }

  public Map<Veteran, List<SubmittedSurvey>> getVeteransSurveySubmissions() {
    return veteransSurveySubmissions;
  }

  public Map<Veteran, SubmittedSurvey> getVeteransLatestSubmissions() {
    return veteransLatestSubmissions;
  }

}
