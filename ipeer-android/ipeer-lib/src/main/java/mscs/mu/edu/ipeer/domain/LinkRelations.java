package mscs.mu.edu.ipeer.domain;

public class LinkRelations {
  public static final String SELF = "self";
  public static final String PROFILE = "profile";
  public static final String VETERAN = "veteran";
  public static final String MENTOR = "mentor";
  public static final String SURVEY = "survey";
  public static final String QUESTION = "question";
  public static final String CHOICE = "choice";
  public static final String SCHEDULE = "schedule";
  public static final String SUBMITTED_ANSWER = "submittedAnswer";
  public static final String SUBMISSION_SUMMARY = "submissionSummary";
  public static final String RESET_TOKEN = "reset-token";
  public static final String MENTOR_APP = "mentor-app";
  public static final String MENTEE_APP = "mentee-app";
  public static final String VERSION = "version";
  public static final String AUTH_TOKEN = "authentication-token";
  public static final String REFRESH_TOKEN = "refresh-token";
}
