package mscs.mu.edu.ipeer.sqlite;


import android.net.Uri;
import android.text.TextUtils;
import mscs.mu.edu.ipeer.domain.SubmissionSummary;
import mscs.mu.edu.ipeer.domain.Veteran;

import java.util.List;

public class LocalVeteran {

  public static final String ID = "id";
  public static final String RED_CAP_USER_ID = "red_cap_user_id";
  public static final String FIRST_NAME = "first_name";
  public static final String LAST_NAME = "last_name";
  public static final String PHONE = "phone";
  public static final String EMAIL = "email";
  public static final String IMAGE = "image";

  public static final String[] COLUMNS = {ID, FIRST_NAME, LAST_NAME, PHONE, EMAIL, RED_CAP_USER_ID, IMAGE};
  public static final String TABLE_VETERAN = "local_veteran";

  private String id;
  private String firstName = "";
  private String lastName = "";
  private String phone = "";
  private String email = "";
  private String redCapUserId;
  private Veteran veteran;
  private List<SubmissionSummary> submissionSummaries;

  public LocalVeteran() {
  }

  public LocalVeteran(final Veteran veteran) {
    this.veteran = veteran;
    this.id = Uri.parse(veteran.getSelf().getHref()).getLastPathSegment();
    this.redCapUserId = veteran.getRedCapUserId();
  }

  public LocalVeteran(final String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getRedCapUserId() {
    return redCapUserId;
  }

  public void setRedCapUserId(String redCapUserId) {
    this.redCapUserId = redCapUserId;
  }

  public Veteran getVeteran() {
    return veteran;
  }

  public void setVeteran(Veteran veteran) {
    this.veteran = veteran;
  }

  public List<SubmissionSummary> getSubmissionSummaries() {
    return submissionSummaries;
  }

  public void setSubmissionSummaries(List<SubmissionSummary> submissionSummaries) {
    this.submissionSummaries = submissionSummaries;
  }

  public String getFullName() {
    return String.format("%s %s",
        TextUtils.isEmpty(firstName) ? "" : firstName,
        TextUtils.isEmpty(lastName) ? "" : lastName
    );
  }

  public String getCapitalizedName() {
    if (!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName)) {
      return "" + Character.toUpperCase(firstName.charAt(0)) + Character.toUpperCase(lastName.charAt(0));
    }

    return id;
  }

  public String getIdForUI() {
    return redCapUserId == null || redCapUserId.isEmpty() ? id : redCapUserId;
  }
}
