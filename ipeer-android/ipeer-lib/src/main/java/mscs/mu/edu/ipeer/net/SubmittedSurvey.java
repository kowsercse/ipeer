package mscs.mu.edu.ipeer.net;

import android.net.Uri;
import mscs.mu.edu.ipeer.domain.Schedule;

import java.util.ArrayList;
import java.util.List;

public class SubmittedSurvey {
  private int schedule;
  private List<SelectedAnswer> submittedAnswers;

  public SubmittedSurvey(final Schedule schedule) {
    this.schedule = Integer.parseInt(Uri.parse(schedule.getSelf().getHref()).getLastPathSegment());
    this.submittedAnswers = new ArrayList<>();
  }

  public int getSchedule() {
    return schedule;
  }

  public void setSchedule(int schedule) {
    this.schedule = schedule;
  }

  public List<SelectedAnswer> getSubmittedAnswers() {
    return submittedAnswers;
  }

  public void setSubmittedAnswers(List<SelectedAnswer> submittedAnswers) {
    this.submittedAnswers = submittedAnswers;
  }
}
