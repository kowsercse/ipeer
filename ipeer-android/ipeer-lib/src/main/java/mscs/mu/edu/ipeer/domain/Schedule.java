package mscs.mu.edu.ipeer.domain;

import java.util.Date;

public class Schedule extends JsonDomain {

  private Date start;
  private Date end;

  public Date getStart() {
    return start;
  }

  public void setStart(Date start) {
    this.start = start;
  }

  public Date getEnd() {
    return end;
  }

  public void setEnd(Date end) {
    this.end = end;
  }

  public boolean isActiveNow() {
    return !isScheduledFuture() && !isScheduledPast();
  }

  public boolean isScheduledFuture() {
    return start.after(new Date());
  }

  public boolean isScheduledPast() {
    return end.before(new Date());
  }

  public Link getSurveyLink() {
    return getLink(LinkRelations.SURVEY);
  }

}
