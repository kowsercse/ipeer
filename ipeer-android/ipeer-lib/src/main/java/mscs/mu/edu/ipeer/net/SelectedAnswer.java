package mscs.mu.edu.ipeer.net;

import android.net.Uri;
import mscs.mu.edu.ipeer.domain.Choice;
import mscs.mu.edu.ipeer.domain.Question;

public class SelectedAnswer {
  private int question;
  private int choice;

  public SelectedAnswer(final Question question, final Choice choice) {
    this.question = Integer.parseInt(Uri.parse(question.getSelf().getHref()).getLastPathSegment());
    this.choice = Integer.parseInt(Uri.parse(choice.getSelf().getHref()).getLastPathSegment());
  }

  public int getQuestion() {
    return question;
  }

  public void setQuestion(int question) {
    this.question = question;
  }

  public int getChoice() {
    return choice;
  }

  public void setChoice(int choice) {
    this.choice = choice;
  }

}
