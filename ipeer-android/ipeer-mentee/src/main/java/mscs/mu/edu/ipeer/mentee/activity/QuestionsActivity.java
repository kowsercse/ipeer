package mscs.mu.edu.ipeer.mentee.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.*;
import android.widget.*;
import mscs.mu.edu.ipeer.domain.*;
import mscs.mu.edu.ipeer.mentee.R;
import mscs.mu.edu.ipeer.net.*;
import mscs.mu.edu.ipeer.net.SelectedAnswer;
import mscs.mu.edu.ipeer.service.Application;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class QuestionsActivity extends AppCompatActivity {
  public static final String SCHEDULE_URI_KEY = "SCHEDULE_URI_KEY";
  public static final String SUBMISSION_SUMMARY_URI_KEY = "SUBMISSION_SUMMARY_URI_KEY";

  private static final Comparator<Choice> comparator = new Comparator<Choice>() {
    @Override
    public int compare(Choice lhs, Choice rhs) {
      return rhs.getValue().compareTo(lhs.getValue());
    }
  };

  private int currentQuestionIndex = -1;
  private boolean isIgnoreBackOrNext = false;
  private List<QuestionAnswerHolder> questionAnswers = new ArrayList<>();
  private Schedule schedule;

  private ChoicesArrayAdapter choicesArrayAdapter;
  private TextView questionTitleTextView;
  private Button nextButton;
  private Button backButton;

  private int olive2;
  private int olive1;

  private Handler choiceSelectionHandler = new Handler();
  private Runnable goNextRunnable = new Runnable() {
    @Override
    public void run() {
      isIgnoreBackOrNext = false;
      goNext();
    }
  };
  private Client client;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_questions);

    initModel();
    initListeners();
    ((ListView) findViewById(R.id.choices_list_view)).setAdapter(choicesArrayAdapter);

    downloadResources();
  }

  private void initModel() {
    olive1 = getResources().getColor(R.color.olive1);
    olive2 = getResources().getColor(R.color.olive2);

    client = ((Application) getApplication()).getClient();
    choicesArrayAdapter = new ChoicesArrayAdapter();
    questionTitleTextView = ((TextView) findViewById(R.id.question_title));
    backButton = (Button) findViewById(R.id.question_back);
    nextButton = (Button)findViewById(R.id.question_next);
  }

  private void initListeners() {
    backButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        goPrevious();
      }
    });
    nextButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        goNext();
      }
    });
  }

  private void downloadResources() {
    String scheduleUriString = getIntent().getStringExtra(SCHEDULE_URI_KEY);
    client.getSchedule(new Link(scheduleUriString), new Callback<Schedule>() {
      @Override
      public void onCompleted(final Schedule schedule) {
        QuestionsActivity.this.schedule = schedule;
        client.getSurvey(schedule.getSurveyLink(), new Callback<Survey>() {
          @Override
          public void onCompleted(final Survey survey) {
            ((TextView) findViewById(R.id.survey_title)).setText(survey.getTitle());
            client.getQuestions(survey.getQuestionsLink(), new Callback<List<Question>>() {
              @Override
              public void onCompleted(final List<Question> questions) {
                for (final Question question : questions) {
                  client.getChoices(question.getChoicesLink(), new Callback<List<Choice>>() {
                    @Override
                    public void onCompleted(List<Choice> choices) {
                      questionAnswers.add(new QuestionAnswerHolder(question, choices));
                      if (questionAnswers.size() == questions.size()) {
                        currentQuestionIndex = 0;
                        reload();
                      }
                    }
                  });
                }
              }
            });
          }
        });
      }
    });
  }

  private void goPrevious() {
    if (!isIgnoreBackOrNext) {
      currentQuestionIndex--;
      reload();
    }
  }

  private void goNext() {
    if (!isIgnoreBackOrNext) {
      if (currentQuestionIndex + 1 == questionAnswers.size()) {
        submitSurvey();
      } else {
        currentQuestionIndex++;
        reload();
      }
    }
  }

  private void reload() {
    backButton.setEnabled(!(currentQuestionIndex == 0 || questionAnswers.size() == 1));
    nextButton.setText((currentQuestionIndex + 1 == questionAnswers.size() || questionAnswers.size() == 1) ? R.string.submit : R.string.next);
    QuestionAnswerHolder questionAnswerHolder = questionAnswers.get(currentQuestionIndex);
    questionTitleTextView.setText(questionAnswerHolder.question.getTitle());
    choicesArrayAdapter.clear();
    choicesArrayAdapter.addAll(questionAnswerHolder.choices);
  }

  private void submitSurvey() {
    final SubmittedSurvey submittedSurvey = new SubmittedSurvey(schedule);
    for (QuestionAnswerHolder questionAnswer : questionAnswers) {
      submittedSurvey.getSubmittedAnswers().add(new SelectedAnswer(questionAnswer.question, questionAnswer.selectedChoice));
    }
    final Link link = new Link(LinkRelations.SUBMISSION_SUMMARY, getIntent().getStringExtra(SUBMISSION_SUMMARY_URI_KEY));
    client.postSubmittedSurvey(link, submittedSurvey, new Callback<SubmissionSummary>() {
      @Override
      public void onCompleted(SubmissionSummary result) {
        Toast.makeText(QuestionsActivity.this, "Survey submitted successfully", Toast.LENGTH_LONG).show();
        setResult(RESULT_OK);
        finish();
      }
    }, new FailureCallback() {
      @Override
      public void onFail(final int statusCode, final String message) {
        Toast.makeText(QuestionsActivity.this, "Failed to submit the survey", Toast.LENGTH_LONG).show();
      }
    });
  }

  private class ChoicesArrayAdapter extends ArrayAdapter<Choice> {
    private final LayoutInflater inflater;

    public ChoicesArrayAdapter() {
      super(QuestionsActivity.this, R.layout.layout_question);
      inflater = (LayoutInflater) QuestionsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      if (convertView == null) {
        convertView = inflater.inflate(R.layout.layout_question, parent, false);
        ViewHolder viewHolder = new ViewHolder(convertView);
        convertView.setTag(viewHolder);
      }

      ViewHolder viewHolder = (ViewHolder) convertView.getTag();
      final Choice choice = getItem(position);
      viewHolder.choiceButton.setText(choice.getLabel());
      final QuestionAnswerHolder questionAnswerHolder = questionAnswers.get(currentQuestionIndex);
      viewHolder.choiceButton.setBackgroundColor(choice.equals(questionAnswerHolder.selectedChoice) ? olive2 : olive1);
      viewHolder.choiceButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, choice.equals(questionAnswerHolder.selectedChoice) ? R.drawable.ok : 0, 0);
      viewHolder.choiceButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (!isIgnoreBackOrNext) {
            isIgnoreBackOrNext = true;
            questionAnswerHolder.selectedChoice = choice;
            choicesArrayAdapter.clear();
            choicesArrayAdapter.addAll(questionAnswerHolder.choices);
            choiceSelectionHandler.postDelayed(goNextRunnable, TimeUnit.MILLISECONDS.toMillis(500));
          }
        }
      });

      return convertView;
    }
  }

  private static class ViewHolder {
    private final Button choiceButton;

    public ViewHolder(View view) {
      choiceButton = (Button) view.findViewById(R.id.choice_selected_button);
    }
  }

  private static class QuestionAnswerHolder {
    private final Question question;
    private final List<Choice> choices;
    private Choice selectedChoice;

    public QuestionAnswerHolder(final Question question, final List<Choice> choices) {
      Collections.sort(choices, comparator);
      this.question = question;
      this.choices = choices;
      selectedChoice = choices.get(0);
    }
  }

}
