package mscs.mu.edu.ipeer.mentee.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import mscs.mu.edu.ipeer.activity.BaseActivity;
import mscs.mu.edu.ipeer.domain.*;
import mscs.mu.edu.ipeer.domain.adapter.SubmittedSurvey;
import mscs.mu.edu.ipeer.mentee.R;
import mscs.mu.edu.ipeer.mentee.component.ScheduleArrayAdapter;
import mscs.mu.edu.ipeer.net.*;
import mscs.mu.edu.ipeer.service.Application;

import java.util.*;

import static mscs.mu.edu.ipeer.activity.SplashScreenActivity.AUTH_HEADER;

public class MainActivity extends BaseActivity {

  public static final int SUBMIT_SURVEY = 1;

  private ScheduleArrayAdapter scheduleArrayAdapter;
  private View checkInButton;

  private List<Schedule> schedules;
  private Veteran veteran;
  private Client client;
  private SharedPreferences preferences;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    final Application application = (Application) getApplicationContext();
    client = application.getClient();
    preferences = getSharedPreferences("mscs.mu.edu.ubicomp.ipeer.config", MODE_PRIVATE);

    client.getProfile(application.getEndpoint().getProfileLink(), new Callback<Profile>() {
      @Override
      public void onCompleted(Profile profile) {
        if (profile.getVeteranLink() == null) {
          showInvalidUserAlert();
          return;
        }
        client.getVeteran(profile.getVeteranLink(), new Callback<Veteran>() {
          @Override
          public void onCompleted(final Veteran veteran) {
            MainActivity.this.veteran = veteran;
            checkInButton = findViewById(R.id.check_in_button);
            scheduleArrayAdapter = new ScheduleArrayAdapter(MainActivity.this, veteran.getSubmissionSummariesLink().getHref());
            ((ListView) findViewById(R.id.schedule_list)).setAdapter(scheduleArrayAdapter);

            final String authenticationHeader = preferences.getString(AUTH_HEADER, null);
            if (authenticationHeader == null) {
              client.getRefreshToken(veteran.getRefreshTokenLink(), new Callback<RefreshToken>() {
                @Override
                public void onCompleted(RefreshToken refreshToken) {
                  client.getAuthToken(refreshToken.getAuthenticationTokenLink(), new Callback<AuthenticationToken>() {
                    @Override
                    public void onCompleted(AuthenticationToken authenticationToken) {
                      preferences.edit().putString(AUTH_HEADER, authenticationToken.getAuthenticationHeader()).apply();
                    }
                  }, null);
                }
              });
            }

            client.getMentor(veteran.getMentorLink(), new Callback<Mentor>() {
              @Override
              public void onCompleted(Mentor mentor) {
                initListeners(mentor);
              }
            });

            client.getSchedules(veteran.getSchedulesLink(), new Callback<List<Schedule>>() {
              @Override
              public void onCompleted(final List<Schedule> schedules) {
                MainActivity.this.schedules = schedules;
                downloadSubmissionSummaries();
              }
            });
          }
        });
      }
    });
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == SUBMIT_SURVEY && resultCode == RESULT_OK) {
      downloadSubmissionSummaries();
    }
  }

  private void showInvalidUserAlert() {
    new AlertDialog.Builder(MainActivity.this)
        .setTitle("Logged in user is not a veteran")
        .setMessage("Veteran application is going to be stopped")
        .setCancelable(false)
        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            finish();
          }
        }).create().show();
  }

  private void downloadSubmissionSummaries() {
    client.getSubmissionSummaries(veteran.getSubmissionSummariesLink(), new Callback<List<SubmissionSummary>>() {
      @Override
      public void onCompleted(final List<SubmissionSummary> submissionSummaries) {
        List<SubmittedSurvey> submittedSurveys = getSubmittedSurveys(submissionSummaries, schedules);
        scheduleArrayAdapter.clear();
        scheduleArrayAdapter.addAll(submittedSurveys);

        final Schedule activeSchedule = getActiveSchedule(submittedSurveys);
        checkInButton.setEnabled(activeSchedule != null);
        if (activeSchedule != null) {
          checkInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent intent = new Intent(MainActivity.this, QuestionsActivity.class);
              intent.putExtra(QuestionsActivity.SCHEDULE_URI_KEY, activeSchedule.getSelf().getHref());
              intent.putExtra(QuestionsActivity.SUBMISSION_SUMMARY_URI_KEY, veteran.getSubmissionSummariesLink().getHref());
              startActivityForResult(intent, SUBMIT_SURVEY);
            }
          });
        }
      }
    }, new FailureCallback() {
      @Override
      public void onFail(final int statusCode, final String message) {
        Toast.makeText(MainActivity.this, "Failed to download submissions", Toast.LENGTH_LONG).show();
      }
    });
  }

  private Schedule getActiveSchedule(List<SubmittedSurvey> submittedSurveys) {
    for (final SubmittedSurvey submittedSurvey : submittedSurveys) {
      if (submittedSurvey.getSchedule().isActiveNow() && submittedSurvey.getSubmissionSummary() == null) {
        return submittedSurvey.getSchedule();
      }
    }

    return null;
  }

  private void initListeners(final Mentor mentor) {
    findViewById(R.id.call_mentor_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Uri uri = Uri.parse("tel:" + mentor.getPhone());
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(uri);
        MainActivity.this.startActivity(callIntent);
      }
    });

    findViewById(R.id.text_mentor_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Uri uri = Uri.parse("smsto:" + mentor.getPhone());
        Intent textIntent = new Intent(Intent.ACTION_SENDTO, uri);
        textIntent.putExtra("compose_mode", true);
        MainActivity.this.startActivity(textIntent);
      }
    });
  }


  private List<SubmittedSurvey> getSubmittedSurveys(List<SubmissionSummary> submissionSummaries, List<Schedule> schedules) {
    Map<Link, SubmissionSummary> submissionSummaryMap = new HashMap<>();
    for (SubmissionSummary submissionSummary : submissionSummaries) {
      submissionSummaryMap.put(submissionSummary.getScheduleLink(), submissionSummary);
    }
    List<SubmittedSurvey> submittedSurveys = new ArrayList<>();
    for (Schedule schedule : schedules) {
      final SubmittedSurvey submittedSurvey = new SubmittedSurvey(schedule, submissionSummaryMap.get(schedule.getSelf()));
      submittedSurveys.add(submittedSurvey);
    }
    Collections.sort(submittedSurveys, new Comparator<SubmittedSurvey>() {
      @Override
      public int compare(SubmittedSurvey lhs, SubmittedSurvey rhs) {
        return rhs.getSchedule().getStart().compareTo(lhs.getSchedule().getStart());
      }
    });

    return submittedSurveys;
  }

}
