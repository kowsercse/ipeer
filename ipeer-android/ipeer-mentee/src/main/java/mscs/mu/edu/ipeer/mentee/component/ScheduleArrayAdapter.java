package mscs.mu.edu.ipeer.mentee.component;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.*;
import android.widget.*;
import mscs.mu.edu.ipeer.domain.Schedule;
import mscs.mu.edu.ipeer.domain.adapter.SubmittedSurvey;
import mscs.mu.edu.ipeer.mentee.R;
import mscs.mu.edu.ipeer.mentee.activity.MainActivity;
import mscs.mu.edu.ipeer.mentee.activity.QuestionsActivity;
import mscs.mu.edu.ipeer.util.DateUtility;

public class ScheduleArrayAdapter extends ArrayAdapter<SubmittedSurvey> {
  private final LayoutInflater inflater;
  private int defaultColor;
  private int black;
  private final String submissionSummaryUri;

  public ScheduleArrayAdapter(Context context, final String submissionSummaryUri) {
    super(context, R.layout.layout_schedule);
    this.submissionSummaryUri = submissionSummaryUri;
    inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    defaultColor = getContext().getResources().getColor(R.color.button_color);
    black = getContext().getResources().getColor(R.color.black);
  }

  @Override
  public View getView(final int position, View convertView, final ViewGroup parent) {
    if (convertView == null) {
      convertView = inflater.inflate(R.layout.layout_schedule, parent, false);
      ViewHolder viewHolder = new ViewHolder(convertView);
      convertView.setTag(viewHolder);
    }

    final SubmittedSurvey submittedSurvey = getItem(position);
    final Schedule schedule = submittedSurvey.getSchedule();
    ViewHolder viewHolder = (ViewHolder) convertView.getTag();
    viewHolder.weekDayLabel.setText(DateUtility.getWeekDay(schedule.getStart()));
    viewHolder.dateLabel.setText(DateUtility.getShortDate(schedule.getStart()));
    decorateCheckInButton(viewHolder.checkInButton, submittedSurvey);

    return convertView;
  }

  private void decorateCheckInButton(final Button checkInButton, final SubmittedSurvey submittedSurvey) {
    final Schedule schedule = submittedSurvey.getSchedule();
    if (schedule.isActiveNow() && submittedSurvey.getSubmissionSummary() == null) {
      decorateButton(checkInButton, R.string.available_now, defaultColor, false, 0, new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          Intent intent = new Intent(getContext(), QuestionsActivity.class);
          intent.putExtra(QuestionsActivity.SCHEDULE_URI_KEY, schedule.getSelf().getHref());
          intent.putExtra(QuestionsActivity.SUBMISSION_SUMMARY_URI_KEY, submissionSummaryUri);
          ((MainActivity) getContext()).startActivityForResult(intent, MainActivity.SUBMIT_SURVEY);
        }
      });
    } else if (schedule.isScheduledFuture()) {
      decorateButton(checkInButton, R.string.coming_soon, defaultColor, false, 0, null);
    } else if(submittedSurvey.getSubmissionSummary() != null) {
      decorateButton(checkInButton, R.string.completed, black, true, R.drawable.ok_green, null);
    }
    else {
      decorateButton(checkInButton, R.string.missed, black, true, R.drawable.cross_red, null);
    }
  }

  private void decorateButton(Button checkInButton, int text, int textColor, boolean isTransparent, int icon, View.OnClickListener listener) {
    if(isTransparent) {
      checkInButton.setBackgroundColor(Color.TRANSPARENT);
    } else {
      checkInButton.setBackgroundResource(R.drawable.button_default);
    }
    checkInButton.setText(text);
    checkInButton.setTextColor(textColor);
    checkInButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, icon, 0);
    checkInButton.setEnabled(listener != null);
    checkInButton.setOnClickListener(listener);
  }

  private static class ViewHolder {
    private final Button checkInButton;
    private final TextView dateLabel;
    private final TextView weekDayLabel;

    public ViewHolder(View view) {
      weekDayLabel = (TextView) view.findViewById(R.id.schedule_week_day_label);
      dateLabel = (TextView) view.findViewById(R.id.schedule_date_label);
      checkInButton = (Button) view.findViewById(R.id.schedule_check_in_button);
    }
  }

}
