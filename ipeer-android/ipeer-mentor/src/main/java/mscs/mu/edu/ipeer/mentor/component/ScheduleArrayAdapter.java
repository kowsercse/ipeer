package mscs.mu.edu.ipeer.mentor.component;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.*;
import android.widget.*;
import mscs.mu.edu.ipeer.mentor.R;
import mscs.mu.edu.ipeer.mentor.activity.SurveyResultActivity;
import mscs.mu.edu.ipeer.domain.adapter.SubmittedSurvey;
import mscs.mu.edu.ipeer.util.DateUtility;

public class ScheduleArrayAdapter extends ArrayAdapter<SubmittedSurvey> {
  private final LayoutInflater inflater;
  private final int defaultColor;
  private String veteranUri;

  public ScheduleArrayAdapter(final Context context, final String veteranUri) {
    super(context, R.layout.layout_schedule);

    inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    this.veteranUri = veteranUri;
    defaultColor = context.getResources().getColor(R.color.button_color);
  }

  @Override
  public View getView(final int position, View convertView, final ViewGroup parent) {
    if (convertView == null) {
      convertView = inflater.inflate(R.layout.layout_schedule, parent, false);
      ViewHolder viewHolder = new ViewHolder(convertView);
      convertView.setTag(viewHolder);
    }

    final SubmittedSurvey submittedSurvey = getItem(position);
    ViewHolder viewHolder = (ViewHolder) convertView.getTag();
    viewHolder.weekDayView.setText(DateUtility.getWeekDay(submittedSurvey.getSchedule().getStart()));
    viewHolder.dateView.setText(DateUtility.getShortDate(submittedSurvey.getSchedule().getStart()));
    decorateCheckInButton(viewHolder.checkInButton, submittedSurvey);
    return convertView;
  }

  private void decorateCheckInButton(final Button checkInButton, final SubmittedSurvey submittedSurvey) {
    if (submittedSurvey.getSubmissionSummary() != null) {
      decorateButton(checkInButton, R.string.view_survey, defaultColor, false, true, new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          Intent intent = new Intent(getContext(), SurveyResultActivity.class);
          intent.putExtra(SurveyResultActivity.VETERAN_URI, veteranUri);
          intent.putExtra(SurveyResultActivity.SELECTED_SCHEDULE_URI, submittedSurvey.getSchedule().getSelf().getHref());
          getContext().startActivity(intent);
        }
      });
    } else if (submittedSurvey.getSchedule().isActiveNow() || submittedSurvey.getSchedule().isScheduledFuture()) {
      decorateButton(checkInButton, R.string.incomplete, defaultColor, false, false, null);
    } else {
      decorateButton(checkInButton, R.string.missed, R.color.black, true, true, null);
    }
  }

  private void decorateButton(Button checkInButton, int text, int textColor, boolean isTransparent, boolean enabled, View.OnClickListener listener) {
    if (isTransparent) {
      checkInButton.setBackgroundColor(Color.TRANSPARENT);
    } else {
      checkInButton.setBackgroundResource(R.drawable.button_default);
    }
    checkInButton.setText(text);
    checkInButton.setTextColor(textColor);
    checkInButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.string.missed == text ? R.drawable.cross_red : 0, 0);
    checkInButton.setEnabled(enabled);
    checkInButton.setOnClickListener(listener);
  }

  private static class ViewHolder {
    private final TextView weekDayView;
    private final TextView dateView;
    private final Button checkInButton;

    public ViewHolder(View view) {
      weekDayView = (TextView) view.findViewById(R.id.schedule_week_day_label);
      dateView = (TextView) view.findViewById(R.id.schedule_date_label);
      checkInButton = (Button) view.findViewById(R.id.schedule_check_in_button);
    }
  }

}
