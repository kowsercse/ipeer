package mscs.mu.edu.ipeer.mentor.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.*;
import mscs.mu.edu.ipeer.activity.BaseActivity;
import mscs.mu.edu.ipeer.component.LineGraph;
import mscs.mu.edu.ipeer.domain.*;
import mscs.mu.edu.ipeer.domain.adapter.SubmittedSurvey;
import mscs.mu.edu.ipeer.mentor.R;
import mscs.mu.edu.ipeer.mentor.component.ScheduleArrayAdapter;
import mscs.mu.edu.ipeer.net.*;
import mscs.mu.edu.ipeer.service.Application;
import mscs.mu.edu.ipeer.sqlite.LocalDatabaseClient;
import mscs.mu.edu.ipeer.sqlite.LocalVeteran;
import mscs.mu.edu.ipeer.util.AndroidUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class VeteranDetailActivity extends BaseActivity {
  private final Logger logger = LoggerFactory.getLogger(getClass());

  public static final String VETERAN_URI = "veteran_uri";
  public static final String VETERAN_TOKEN_URI = "veteran_token_uri";
  private static final int EDIT_VETERAN = 1;

  private LocalVeteran localVeteran;
  private Client client;
  private LocalDatabaseClient localDatabaseClient;
  private ScheduleArrayAdapter scheduleArrayAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_veteran_detail);

    initModel();
    initState();
    initListeners();

    downloadSubmittedSurvey();
  }

  private void downloadSubmittedSurvey() {
    String veteranUri = getIntent().getStringExtra(VETERAN_URI);
    client.getVeteran(new Link(LinkRelations.VETERAN, veteranUri), new Callback<Veteran>() {
      @Override
      public void onCompleted(final Veteran veteran) {
        final List<SubmittedSurvey> submittedSurveys = ((Application)getApplication()).getVeteransSurveySubmissions().get(veteran);
        scheduleArrayAdapter.clear();
        scheduleArrayAdapter.addAll(submittedSurveys);

        final LineGraph graph = (LineGraph) findViewById(R.id.veteran_survey_graph);
        graph.clear();
        graph.addAll(submittedSurveys);
      }
    });
  }

  private void initModel() {
    String veteranUri = getIntent().getStringExtra(VETERAN_URI);
    String localVeteranId = Uri.parse(veteranUri).getLastPathSegment();
    final Application application = (Application) getApplication();
    client = application.getClient();
    localDatabaseClient = application.getLocalDatabaseClient();
    localVeteran = localDatabaseClient.getLocalVeteran(localVeteranId);
    scheduleArrayAdapter = new ScheduleArrayAdapter(this, veteranUri);
  }

  private void initState() {
    ((TextView) findViewById(R.id.veteran_id_text_view)).setText("ID: " + localVeteran.getIdForUI());
    ((TextView) findViewById(R.id.veteran_name_text_view)).setText(localVeteran.getFullName());
    ((ListView)findViewById(R.id.submission_summaries_list_view)).setAdapter(scheduleArrayAdapter);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if(requestCode == EDIT_VETERAN && resultCode == RESULT_OK) {
      localVeteran = localDatabaseClient.getLocalVeteran(localVeteran.getId());
      ((TextView) findViewById(R.id.veteran_name_text_view)).setText(localVeteran.getFullName());
      setResult(RESULT_OK);
    }
  }

  private void initListeners() {
    findViewById(R.id.veteran_call_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (localVeteran != null) {
          Uri uri = Uri.parse("tel:" + localVeteran.getPhone());
          Intent callIntent = new Intent(Intent.ACTION_CALL);
          callIntent.setData(uri);
          startActivity(callIntent);
        }
      }
    });

    findViewById(R.id.veteran_send_text_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (localVeteran != null) {
          Uri uri = Uri.parse("smsto:" + localVeteran.getPhone());
          Intent textIntent = new Intent(Intent.ACTION_SENDTO, uri);
          textIntent.putExtra("compose_mode", true);
          startActivity(textIntent);
        }
        else {
          Toast.makeText(VeteranDetailActivity.this, "Please add a phone number before sending a reset token", Toast.LENGTH_LONG).show();
        }
      }
    });

    findViewById(R.id.veteran_email_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (localVeteran != null) {
          Intent intent = new Intent(Intent.ACTION_SEND);
          intent.setType("text/html");
          intent.putExtra(Intent.EXTRA_EMAIL, localVeteran.getEmail());
          startActivity(Intent.createChooser(intent, "Send Email"));
        }
        else {
          Toast.makeText(VeteranDetailActivity.this, "Please add a phone number before sending a reset token", Toast.LENGTH_LONG).show();
        }
      }
    });

    findViewById(R.id.veteran_edit_text_view).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(VeteranDetailActivity.this, VeteranEditActivity.class);
        intent.putExtra(VeteranEditActivity.VETERAN_ID, localVeteran.getId());
        startActivityForResult(intent, EDIT_VETERAN);
      }
    });

    findViewById(R.id.veteran_change_password_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (localVeteran != null) {
          Link tokenLink = new Link(LinkRelations.RESET_TOKEN, getIntent().getStringExtra(VETERAN_TOKEN_URI));
          client.getResetToken(tokenLink, new Callback<String>() {
            @Override
            public void onCompleted(String result) {
              final String message = String.format("Your username: %s\nReset your password here: %s", localVeteran.getId(), result);
              sendResetToken(message, localVeteran);
            }
          }, new FailureCallback() {
            @Override
            public void onFail(final int statusCode, final String message) {
              finish();
              Toast.makeText(VeteranDetailActivity.this, "Failed to generate reset token for " + localVeteran.getFullName(), Toast.LENGTH_LONG).show();
            }
          });
        } else {
          Toast.makeText(VeteranDetailActivity.this, "Please add a phone number before sending a reset token", Toast.LENGTH_LONG).show();
        }
      }
    });
  }

  private void sendResetToken(String result, LocalVeteran localVeteran) {
    String fullName = localVeteran.getFullName();
    String niceName = TextUtils.isEmpty(fullName) ? localVeteran.getIdForUI() : fullName;
    try {
      AndroidUtils.sendSMS(localVeteran.getPhone(), result, VeteranDetailActivity.this);
      Toast.makeText(this, "A text message is sent to " + niceName, Toast.LENGTH_LONG).show();
    } catch (Exception ex) {
      logger.debug("Failed to send text message for: " + localVeteran.getPhone(), ex);
      Toast.makeText(VeteranDetailActivity.this, "Failed to send password token message for: " + localVeteran.getPhone(), Toast.LENGTH_LONG).show();
    }
  }

  private List<SubmittedSurvey> getSubmittedSurveys(List<SubmissionSummary> submissionSummaries, List<Schedule> schedules) {
    Map<Link, SubmissionSummary> submissionSummaryMap = new HashMap<>();
    for (SubmissionSummary submissionSummary : submissionSummaries) {
      submissionSummaryMap.put(submissionSummary.getScheduleLink(), submissionSummary);
    }
    List<SubmittedSurvey> submittedSurveys = new ArrayList<>();
    for (Schedule schedule : schedules) {
      submittedSurveys.add(new SubmittedSurvey(schedule, submissionSummaryMap.get(schedule.getSelf())));
    }
    Collections.sort(submittedSurveys, new Comparator<SubmittedSurvey>() {
      @Override
      public int compare(SubmittedSurvey lhs, SubmittedSurvey rhs) {
        return rhs.getSchedule().getStart().compareTo(lhs.getSchedule().getStart());
      }
    });
    return submittedSurveys;
  }

}
