package mscs.mu.edu.ipeer.mentor.component;

import android.content.Context;
import android.content.Intent;
import android.view.*;
import android.widget.*;
import mscs.mu.edu.ipeer.component.LineGraph;
import mscs.mu.edu.ipeer.domain.*;
import mscs.mu.edu.ipeer.domain.adapter.SubmittedSurvey;
import mscs.mu.edu.ipeer.mentor.R;
import mscs.mu.edu.ipeer.mentor.activity.*;
import mscs.mu.edu.ipeer.service.Application;
import mscs.mu.edu.ipeer.sqlite.LocalVeteran;

import java.util.*;

public class VeteransArrayAdapter extends ArrayAdapter<LocalVeteran> {
  private final Map<Veteran, List<SubmittedSurvey>> veteransSurveySubmissions;
  private Map<Veteran, SubmittedSurvey> veteransLatestSubmissions;

  private final LayoutInflater inflater;
  private final int red;
  private final int gray2;

  public VeteransArrayAdapter(Context context) {
    super(context, R.layout.layout_veteran);
    veteransLatestSubmissions = ((Application) context.getApplicationContext()).getVeteransLatestSubmissions();
    veteransSurveySubmissions = ((Application) context.getApplicationContext()).getVeteransSurveySubmissions();
    inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    red = getContext().getResources().getColor(R.color.red);
    gray2 = getContext().getResources().getColor(R.color.gray2);
  }

  @Override
  public View getView(final int position, View convertView, final ViewGroup parent) {
    final LocalVeteran localVeteran = getItem(position);

    if (convertView == null) {
      convertView = inflater.inflate(R.layout.layout_veteran, parent, false);
      final ViewHolder viewHolder = new ViewHolder(convertView);
      convertView.setTag(viewHolder);
    }

    ViewHolder viewHolder = (ViewHolder) convertView.getTag();
    viewHolder.viewProfile.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View v) {
        Intent intent = new Intent(getContext(), VeteranDetailActivity.class);
        intent.putExtra(VeteranDetailActivity.VETERAN_URI, localVeteran.getVeteran().getSelf().getHref());
        intent.putExtra(VeteranDetailActivity.VETERAN_TOKEN_URI, localVeteran.getVeteran().getResetTokenLink().getHref());
        ((MainActivity) getContext()).startActivityForResult(intent, MainActivity.EDIT_VETERAN);
      }
    });
    viewHolder.nameView.setText(localVeteran.getCapitalizedName());
    showLatestStatus(localVeteran, viewHolder);
    final List<SubmittedSurvey> submittedSurveys = getPreviousSurveys(localVeteran);
    viewHolder.surveyGraph.clear();
    viewHolder.surveyGraph.addAll(submittedSurveys);
    viewHolder.surveyGraph.setCallback(new LineGraph.Callback() {
      @Override
      public void surveySelected(final SubmittedSurvey submittedSurvey) {
        Intent intent = new Intent(getContext(), SurveyResultActivity.class);
        intent.putExtra(SurveyResultActivity.VETERAN_URI, localVeteran.getVeteran().getSelf().getHref());
        intent.putExtra(SurveyResultActivity.SELECTED_SCHEDULE_URI, submittedSurvey.getSchedule().getSelf().getHref());
        getContext().startActivity(intent);
      }
    });

    return convertView;
  }

  private void showLatestStatus(final LocalVeteran localVeteran, final ViewHolder viewHolder) {
    final SubmittedSurvey submittedSurvey = veteransLatestSubmissions.get(localVeteran.getVeteran());
    final SubmissionSummary latestSubmission = submittedSurvey != null ? submittedSurvey.getSubmissionSummary() : null;
    if(latestSubmission == null) {
      updateProgressIcon(viewHolder, red, R.drawable.negetive, "Survey missed");
    }
    else if(latestSubmission.getProgressDirection() > 0) {
      updateProgressIcon(viewHolder, gray2, R.drawable.up, "Survey completed");
    }
    else if(latestSubmission.getProgressDirection() < 0) {
      updateProgressIcon(viewHolder, gray2, R.drawable.down, "Survey completed");
    }
    else {
      updateProgressIcon(viewHolder, gray2, R.drawable.equal, "Survey completed");
    }
  }

  private void updateProgressIcon(ViewHolder viewHolder, int red, int imageResource, String message) {
    viewHolder.progressView.setBackgroundColor(red);
    viewHolder.progressView.setImageResource(imageResource);
    viewHolder.statusView.setText(message);
  }

  private List<SubmittedSurvey> getPreviousSurveys(final LocalVeteran localVeteran) {
    final List<SubmittedSurvey> submittedSurveys = veteransSurveySubmissions.get(localVeteran.getVeteran());
    if(submittedSurveys == null || submittedSurveys.size() == 0) {
      return Collections.emptyList();
    }
    for (int i = 0; i < submittedSurveys.size(); i++) {
      final Schedule schedule = submittedSurveys.get(i).getSchedule();
      if(schedule.isActiveNow() || schedule.isScheduledPast()) {
        return submittedSurveys.subList(i, submittedSurveys.size());
      }
    }

    return Collections.emptyList();
  }

  private static class ViewHolder {
    private final TextView nameView;
    private final ImageButton progressView;
    private final LineGraph surveyGraph;
    private final TextView statusView;
    private final Button viewProfile;

    public ViewHolder(View view) {
      nameView = (TextView) view.findViewById(R.id.veteran_capitalize_name);
      progressView = (ImageButton) view.findViewById(R.id.veteran_progress_icon);
      surveyGraph = (LineGraph) view.findViewById(R.id.survey_graph);
      statusView = (TextView) view.findViewById(R.id.veteran_latest_status);
      viewProfile = (Button) view.findViewById(R.id.veteran_view_profile);
    }
  }

}
