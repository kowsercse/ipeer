package mscs.mu.edu.ipeer.mentor.activity;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import mscs.mu.edu.ipeer.activity.BaseActivity;
import mscs.mu.edu.ipeer.domain.*;
import mscs.mu.edu.ipeer.domain.adapter.SubmittedSurvey;
import mscs.mu.edu.ipeer.mentor.R;
import mscs.mu.edu.ipeer.mentor.component.SubmittedAnswerArrayAdapter;
import mscs.mu.edu.ipeer.net.*;
import mscs.mu.edu.ipeer.service.Application;
import mscs.mu.edu.ipeer.sqlite.LocalDatabaseClient;
import mscs.mu.edu.ipeer.sqlite.LocalVeteran;
import mscs.mu.edu.ipeer.util.DateUtility;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class SurveyResultActivity extends BaseActivity {

  public static final String SELECTED_SCHEDULE_URI = "selected_schedule";
  public static final String VETERAN_URI = "veteran_uri";

  private Client client;
  private LocalVeteran localVeteran;
  private int currentIndex = -1;
  private View nextButton;
  private View backButton;
  private List<SubmittedSurvey> submittedSurveys;
  private SubmittedAnswerArrayAdapter answerArrayAdapter;
  private TextView surveyTitleView;
  private TextView submissionDateView;
  private TextView scheduleDateView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_survey_result);

    initModel();
    initComponents();
    initListeners();
    downloadSubmittedSurvey();
  }

  private void initComponents() {
    backButton = findViewById(R.id.back_button);
    nextButton = findViewById(R.id.next_button);
    surveyTitleView = (TextView) findViewById(R.id.survey_title);
    submissionDateView = (TextView) findViewById(R.id.submission_date);
    scheduleDateView = (TextView) findViewById(R.id.schedule_date);
    ((TextView) findViewById(R.id.veteran_id_text_view)).setText("ID: " + localVeteran.getIdForUI());
    ((TextView) findViewById(R.id.veteran_full_name)).setText(localVeteran.getFullName());
  }

  private void initListeners() {
    backButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        currentIndex--;
        reload();
      }
    });
    nextButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        currentIndex++;
        reload();
      }
    });
  }

  private void reload() {
    backButton.setEnabled(!(currentIndex == 0 || submittedSurveys.size() == 1));
    nextButton.setEnabled(!(currentIndex + 1 == submittedSurveys.size() || submittedSurveys.size() == 1));
    SubmittedSurvey submittedSurvey = submittedSurveys.get(currentIndex);
    surveyTitleView.setText(submittedSurvey.getSurvey().getTitle());
    scheduleDateView.setText(DateUtility.getSimpleDate(submittedSurvey.getSchedule().getStart()) + " - " + DateUtility.getSimpleDate(submittedSurvey.getSchedule().getEnd()));
    answerArrayAdapter.clear();
    if(submittedSurvey.getSubmissionSummary() != null) {
      submissionDateView.setText("Submitted on: " + DateUtility.getFullDateTime(submittedSurvey.getSubmissionSummary().getSubmissionTime()));
      answerArrayAdapter.addAll(submittedSurvey.getAnswers());
    }
    else {
      submissionDateView.setText("");
    }
  }

  private void downloadSubmittedSurvey() {
    String veteranUri = getIntent().getStringExtra(VETERAN_URI);
    client.getVeteran(new Link(LinkRelations.VETERAN, veteranUri), new Callback<Veteran>() {
      @Override
      public void onCompleted(final Veteran veteran) {
        submittedSurveys = ((Application)getApplication()).getVeteransSurveySubmissions().get(veteran);
        initializeCurrentIndex();
        downloadAnswers();
      }
    });
  }

  private void downloadAnswers() {
    final AtomicInteger totalProcessed = new AtomicInteger(0);
    for (final SubmittedSurvey submittedSurvey : submittedSurveys) {
      SubmissionSummary submissionSummary = submittedSurvey.getSubmissionSummary();
      if (submissionSummary != null) {
        client.getSubmittedAnswers(submissionSummary.getSubmittedAnswersLink(), new Callback<List<SubmittedAnswer>>() {
          @Override
          public void onCompleted(List<SubmittedAnswer> submittedAnswers) {
            submittedSurvey.setAnswers(submittedAnswers);
            updateUiIfPossible(totalProcessed);
          }
        });
      } else {
        updateUiIfPossible(totalProcessed);
      }
    }
  }

  private void initModel() {
    String veteranUri = getIntent().getStringExtra(VETERAN_URI);
    Application application = (Application) getApplication();
    client = application.getClient();
    LocalDatabaseClient localDatabaseClient = application.getLocalDatabaseClient();
    localVeteran = localDatabaseClient.getLocalVeteran(Uri.parse(veteranUri).getLastPathSegment());
    answerArrayAdapter = new SubmittedAnswerArrayAdapter(this);
    ((ListView)findViewById(R.id.submitted_answers_list_view)).setAdapter(answerArrayAdapter);
  }

  private void initializeCurrentIndex() {
    String selectedScheduleUri = getIntent().getStringExtra(SELECTED_SCHEDULE_URI);
    for (SubmittedSurvey submittedSurvey : submittedSurveys) {
      currentIndex++;
      if(submittedSurvey.getSchedule().getSelf().getHref().equals(selectedScheduleUri)) {
        break;
      }
    }
  }

  private void updateUiIfPossible(AtomicInteger totalProcessed) {
    if(totalProcessed.incrementAndGet() == submittedSurveys.size()) {
      reload();
    }
  }

}
