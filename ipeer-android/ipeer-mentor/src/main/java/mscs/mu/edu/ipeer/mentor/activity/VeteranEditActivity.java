package mscs.mu.edu.ipeer.mentor.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.View;
import android.widget.*;
import mscs.mu.edu.ipeer.mentor.R;
import mscs.mu.edu.ipeer.service.Application;
import mscs.mu.edu.ipeer.sqlite.LocalDatabaseClient;
import mscs.mu.edu.ipeer.sqlite.LocalVeteran;

public class VeteranEditActivity extends AppCompatActivity {
  public static final String VETERAN_ID = "veteran_id";

  private EditText redCapUserIdEditText;
  private EditText firstNameEditText;
  private EditText lastNameEditText;
  private EditText emailEditText;
  private EditText phoneEditText;
  private LocalVeteran localVeteran;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_veteran_edit);

    Application application = (Application) getApplication();
    String localVeteranId = getIntent().getStringExtra(VETERAN_ID);
    final LocalDatabaseClient localDatabaseClient = application.getLocalDatabaseClient();
    localVeteran = localDatabaseClient.getLocalVeteran(localVeteranId);

    ((TextView) findViewById(R.id.veteran_id_text_view)).setText("ID: " + localVeteran.getId());
    redCapUserIdEditText = (EditText) findViewById(R.id.veteran_red_cap_user_id_edit_text);
    firstNameEditText = (EditText) findViewById(R.id.veteran_first_name_edit_text);
    lastNameEditText = (EditText) findViewById(R.id.veteran_last_name_edit_text);
    emailEditText = (EditText) findViewById(R.id.veteran_email_edit_text);
    phoneEditText = (EditText) findViewById(R.id.veteran_phone_number_edit_text);

    redCapUserIdEditText.setText(localVeteran.getRedCapUserId());
    firstNameEditText.setText(localVeteran.getFirstName());
    lastNameEditText.setText(localVeteran.getLastName());
    emailEditText.setText(localVeteran.getEmail());
    phoneEditText.setText(localVeteran.getPhone());

    phoneEditText.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
    findViewById(R.id.veteran_update_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        LocalVeteran editedLocalVeteran = createEditedVeteran();
        localDatabaseClient.updateVeteran(editedLocalVeteran);
        setResult(RESULT_OK);
        finish();
        Toast.makeText(VeteranEditActivity.this, "Veteran information is updated", Toast.LENGTH_LONG).show();
      }
    });
    findViewById(R.id.veteran_edit_cancel_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });
  }

  private LocalVeteran createEditedVeteran() {
    final LocalVeteran editedVeteran = new LocalVeteran(localVeteran.getId());
    editedVeteran.setRedCapUserId(String.valueOf(redCapUserIdEditText.getText()));
    editedVeteran.setFirstName(String.valueOf(firstNameEditText.getText()));
    editedVeteran.setLastName(String.valueOf(lastNameEditText.getText()));
    editedVeteran.setPhone(String.valueOf(phoneEditText.getText()));
    editedVeteran.setEmail(String.valueOf(emailEditText.getText()));
    return editedVeteran;
  }

}
