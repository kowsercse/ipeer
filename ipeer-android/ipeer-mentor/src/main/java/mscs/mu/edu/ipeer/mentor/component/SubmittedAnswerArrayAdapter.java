package mscs.mu.edu.ipeer.mentor.component;

import android.content.Context;
import android.view.*;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import mscs.mu.edu.ipeer.domain.*;
import mscs.mu.edu.ipeer.mentor.R;
import mscs.mu.edu.ipeer.service.Application;

import java.util.Map;


public class SubmittedAnswerArrayAdapter extends ArrayAdapter<SubmittedAnswer> {
  private final LayoutInflater inflater;
  private final Map<Link, Choice> choices;
  private final Map<Link, Question> questions;

  public SubmittedAnswerArrayAdapter(final Context context) {
    super(context, R.layout.layout_submitted_answer);
    inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    Application application = (Application) context.getApplicationContext();
    choices = application.getChoices();
    questions = application.getQuestions();
  }

  @Override
  public View getView(final int position, View convertView, final ViewGroup parent) {
    if (convertView == null) {
      convertView = inflater.inflate(R.layout.layout_submitted_answer, parent, false);
      ViewHolder viewHolder = new ViewHolder(convertView);
      convertView.setTag(viewHolder);
    }

    final SubmittedAnswer submittedAnswer = getItem(position);
    ViewHolder viewHolder = (ViewHolder) convertView.getTag();
    viewHolder.questionTitle.setText(questions.get(submittedAnswer.getQuestionLink()).getTitle());
    viewHolder.choiceLabel.setText(choices.get(submittedAnswer.getChoiceLink()).getLabel());
    return convertView;
  }

  private static class ViewHolder {
    private final TextView questionTitle;
    private final TextView choiceLabel;

    public ViewHolder(View view) {
      questionTitle = (TextView) view.findViewById(R.id.question_title_text_view);
      choiceLabel = (TextView) view.findViewById(R.id.choice_label_text_view);
    }
  }

}
