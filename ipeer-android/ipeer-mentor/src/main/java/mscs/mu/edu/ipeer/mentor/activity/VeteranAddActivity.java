package mscs.mu.edu.ipeer.mentor.activity;

import android.app.*;
import android.net.*;
import android.os.*;
import android.telephony.*;
import android.view.*;
import android.widget.*;
import mscs.mu.edu.ipeer.domain.*;
import mscs.mu.edu.ipeer.mentor.R;
import mscs.mu.edu.ipeer.net.Callback;
import mscs.mu.edu.ipeer.net.Client;
import mscs.mu.edu.ipeer.net.FailureCallback;
import mscs.mu.edu.ipeer.service.Application;
import mscs.mu.edu.ipeer.sqlite.LocalDatabaseClient;
import mscs.mu.edu.ipeer.sqlite.LocalVeteran;
import mscs.mu.edu.ipeer.util.AndroidUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VeteranAddActivity extends Activity {

  public static final String VETERANS_URI = "veterans_uri";
  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_veteran_add);

    Application application = (Application) getApplication();
    final Client client = application.getClient();
    final LocalDatabaseClient localDatabaseClient = application.getLocalDatabaseClient();

    ((TextView) findViewById(R.id.veteran_phone_name_edit_text)).addTextChangedListener(new PhoneNumberFormattingTextWatcher());
    findViewById(R.id.veteran_add_cancel_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });
    findViewById(R.id.veteran_add_ok_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        final String veteransUri = getIntent().getStringExtra(VETERANS_URI);
        Link veteransLink = new Link(LinkRelations.VETERAN, veteransUri);

        final LocalVeteran localVeteran = createLocalVeteran();
        Veteran submittedVeteran= new Veteran();
        submittedVeteran.setRedCapUserId(localVeteran.getRedCapUserId());
        client.addVeteran(veteransLink, submittedVeteran, new Callback<Veteran>() {
          @Override
          public void onCompleted(Veteran veteran) {
            localVeteran.setId(Uri.parse(veteran.getSelf().getHref()).getLastPathSegment());
            localVeteran.setVeteran(veteran);
            localDatabaseClient.addVeteran(localVeteran);

            setResult(RESULT_OK);
            sendRefreshToken(veteran, client, localVeteran);
            sendResetToken(veteran, client, localVeteran);
          }
        }, new FailureCallback() {
          @Override
          public void onFail(final int statusCode, final String message) {
            Toast.makeText(VeteranAddActivity.this, "Failed to add veteran: " + message, Toast.LENGTH_LONG).show();
          }
        });
      }
    });
  }

  private void sendRefreshToken(final Veteran veteran, final Client client, final LocalVeteran localVeteran) {
    client.getRefreshToken(veteran.getRefreshTokenLink(), new Callback<RefreshToken>() {
      @Override
      public void onCompleted(RefreshToken refreshToken) {
        try {
          Toast.makeText(VeteranAddActivity.this, "Login information is sent to " + localVeteran.getFullName(), Toast.LENGTH_LONG).show();
          AndroidUtils.sendSMS(localVeteran.getPhone(), "QRF-" + refreshToken.getToken(), VeteranAddActivity.this);
        } catch (Exception ex) {
          logger.debug("Failed to send text message for: " + localVeteran.getPhone(), ex);
          Toast.makeText(VeteranAddActivity.this, "Failed to send text message for: " + localVeteran.getPhone(), Toast.LENGTH_LONG).show();
        }
        finish();
      }
    }, new FailureCallback() {
      @Override
      public void onFail(final int statusCode, final String message) {
        Toast.makeText(VeteranAddActivity.this, "Failed to send generate reset token for " + localVeteran.getFullName(), Toast.LENGTH_LONG).show();
        finish();
      }
    });
  }

  private void sendResetToken(final Veteran veteran, final Client client, final LocalVeteran localVeteran) {
    client.getResetToken(veteran.getResetTokenLink(), new Callback<String>() {
      @Override
      public void onCompleted(String result) {
        try {
          Toast.makeText(VeteranAddActivity.this, "A text message is sent to " + localVeteran.getFullName(), Toast.LENGTH_LONG).show();
          AndroidUtils.sendSMS(localVeteran.getPhone(), prepareMessageBody(localVeteran.getId(), result), VeteranAddActivity.this);
        } catch (Exception ex) {
          logger.debug("Failed to send text message for: " + localVeteran.getPhone(), ex);
          Toast.makeText(VeteranAddActivity.this, "Failed to send text message for: " + localVeteran.getPhone(), Toast.LENGTH_LONG).show();
        }
        finish();
      }
    }, new FailureCallback() {
      @Override
      public void onFail(final int statusCode, final String message) {
        Toast.makeText(VeteranAddActivity.this, "Failed to send generate reset token for " + localVeteran.getFullName(), Toast.LENGTH_LONG).show();
        finish();
      }
    });
  }

  private String prepareMessageBody(final String username, final String url) {
    return String.format("Your username: %s\nReset your password here: %s", username, url);
  }

  private LocalVeteran createLocalVeteran() {
    final LocalVeteran localVeteran = new LocalVeteran();
    localVeteran.setRedCapUserId(String.valueOf(((TextView) findViewById(R.id.veteran_red_cap_user_id_edit_text)).getText()));
    localVeteran.setFirstName(String.valueOf(((TextView) findViewById(R.id.veteran_first_name_edit_text)).getText()));
    localVeteran.setLastName(String.valueOf(((TextView) findViewById(R.id.veteran_last_name_edit_text)).getText()));
    localVeteran.setEmail(String.valueOf(((TextView) findViewById(R.id.veteran_email_edit_text)).getText()));
    localVeteran.setPhone(String.valueOf(((TextView) findViewById(R.id.veteran_phone_name_edit_text)).getText()));
    return localVeteran;
  }

}
