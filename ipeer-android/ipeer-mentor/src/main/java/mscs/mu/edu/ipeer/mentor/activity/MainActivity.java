package mscs.mu.edu.ipeer.mentor.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import mscs.mu.edu.ipeer.activity.BaseActivity;
import mscs.mu.edu.ipeer.domain.*;
import mscs.mu.edu.ipeer.domain.adapter.SubmittedSurvey;
import mscs.mu.edu.ipeer.mentor.R;
import mscs.mu.edu.ipeer.mentor.component.VeteransArrayAdapter;
import mscs.mu.edu.ipeer.net.*;
import mscs.mu.edu.ipeer.service.Application;
import mscs.mu.edu.ipeer.sqlite.LocalDatabaseClient;
import mscs.mu.edu.ipeer.sqlite.LocalVeteran;

import java.util.*;


public class MainActivity extends BaseActivity {
  public static final int EDIT_VETERAN = 2;
  private static final int ADD_VETERAN = 1;

  private Application application;
  private Client client;
  private LocalDatabaseClient localDatabaseClient;
  private ArrayAdapter<LocalVeteran> veteransArrayAdapter;

  private Mentor mentor;
  private List<Veteran> veterans;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    application = (Application) getApplication();
    localDatabaseClient = application.getLocalDatabaseClient();
    client = application.getClient();

    findViewById(R.id.veteran_add_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this, VeteranAddActivity.class);
        intent.putExtra(VeteranAddActivity.VETERANS_URI, mentor.getVeteransLink().getHref());
        startActivityForResult(intent, ADD_VETERAN);
      }
    });

    veteransArrayAdapter = new VeteransArrayAdapter(this);
    ((ListView) findViewById(R.id.veteran_list)).setAdapter(veteransArrayAdapter);

    downloadSurveyInformation(application);
    client.getProfile(application.getEndpoint().getProfileLink(), new Callback<Profile>() {
      @Override
      public void onCompleted(final Profile profile) {
        if (profile.getMentorLink() == null) {
          new AlertDialog.Builder(MainActivity.this)
              .setTitle("Logged in user is not a mentor")
              .setMessage("Mentor application is going to be stopped")
              .setCancelable(false)
              .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  finish();
                }
              }).create().show();
          return;
        }
        client.getMentor(profile.getMentorLink(), new Callback<Mentor>() {
          @Override
          public void onCompleted(final Mentor mentor) {
            MainActivity.this.mentor = mentor;
            downloadVeterans();
          }
        });
      }
    });
  }

  private void downloadSurveyInformation(final Application application) {
    client.getSurveys(application.getEndpoint().getSurveysLink(), new Callback<List<Survey>>() {
      @Override
      public void onCompleted(List<Survey> surveys) {
        for (Survey survey : surveys) {
          application.getSurveys().put(survey.getSelf(), survey);
        }
      }
    });
    client.getQuestions(application.getEndpoint().getQuestionsLink(), new Callback<List<Question>>() {
      @Override
      public void onCompleted(List<Question> questions) {
        for (Question question : questions) {
          application.getQuestions().put(question.getSelf(), question);
        }
      }
    });
    client.getChoices(application.getEndpoint().getChoicesLink(), new Callback<List<Choice>>() {
      @Override
      public void onCompleted(List<Choice> choices) {
        for (Choice choice : choices) {
          application.getChoices().put(choice.getSelf(), choice);
        }
      }
    });
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if(requestCode == ADD_VETERAN && resultCode == RESULT_OK) {
      downloadVeterans();
    }
    else if(requestCode == EDIT_VETERAN && resultCode == RESULT_OK) {
      reloadLocalVeterans();
    }
  }

  private void downloadVeterans() {
    client.getVeterans(mentor.getVeteransLink(), new Callback<List<Veteran>>() {
      @Override
      public void onCompleted(final List<Veteran> veterans) {
        MainActivity.this.veterans = veterans;
        for (final Veteran veteran : veterans) {
          client.getSchedules(veteran.getSchedulesLink(), new Callback<List<Schedule>>() {
            @Override
            public void onCompleted(final List<Schedule> schedules) {
              client.getSubmissionSummaries(veteran.getSubmissionSummariesLink(), new Callback<List<SubmissionSummary>>() {
                @Override
                public void onCompleted(List<SubmissionSummary> submissionSummaries) {
                  updateSubmittedSurvey(submissionSummaries, schedules, veteran, veterans);
                }
              }, new FailureCallback() {
                @Override
                public void onFail(final int statusCode, final String message) {
                  updateSubmittedSurvey(Collections.<SubmissionSummary>emptyList(), schedules, veteran, veterans);
                }
              });
            }
          });
        }
        reloadLocalVeterans();
      }
    });
  }

  private void updateSubmittedSurvey(final List<SubmissionSummary> submissionSummaries, final List<Schedule> schedules, final Veteran veteran, final List<Veteran> veterans) {
    final List<SubmittedSurvey> submittedSurveys = getSubmittedSurveys(submissionSummaries, schedules);
    application.getVeteransSurveySubmissions().put(veteran, submittedSurveys);
    application.getVeteransLatestSubmissions().put(veteran, getLatestSubmission(submittedSurveys));
    if (application.getVeteransSurveySubmissions().size() == veterans.size()) {
      reloadLocalVeterans();
    }
  }

  private void reloadLocalVeterans() {
    List<LocalVeteran> localVeterans = getLocalVeterans();
    veteransArrayAdapter.clear();
    veteransArrayAdapter.addAll(localVeterans);
  }

  private List<LocalVeteran> getLocalVeterans() {
    List<LocalVeteran> localVeterans = new ArrayList<>();
    for (Veteran veteran : veterans) {
      LocalVeteran localVeteran = localDatabaseClient.getLocalVeteran(veteran);
      if (localVeteran == null) {
        localVeteran = new LocalVeteran(veteran);
        localDatabaseClient.addVeteran(localVeteran);
      }
      localVeterans.add(localVeteran);
    }

    Collections.sort(localVeterans, new Comparator<LocalVeteran>() {
      @Override
      public int compare(LocalVeteran lhs, LocalVeteran rhs) {
        return lhs.getVeteran().getStartDate().compareTo(rhs.getVeteran().getStartDate());
      }
    });
    return localVeterans;
  }

  private List<SubmittedSurvey> getSubmittedSurveys(List<SubmissionSummary> submissionSummaries, List<Schedule> schedules) {
    Map<Link, SubmissionSummary> submissionSummaryMap = new HashMap<>();
    for (SubmissionSummary submissionSummary : submissionSummaries) {
      submissionSummaryMap.put(submissionSummary.getScheduleLink(), submissionSummary);
    }
    List<SubmittedSurvey> submittedSurveys = new ArrayList<>();
    for (Schedule schedule : schedules) {
      final SubmittedSurvey submittedSurvey = new SubmittedSurvey(schedule, submissionSummaryMap.get(schedule.getSelf()));
      client.getSurvey(schedule.getSurveyLink(), new Callback<Survey>() {
        @Override
        public void onCompleted(Survey survey) {
          submittedSurvey.setSurvey(survey);
        }
      });
      submittedSurveys.add(submittedSurvey);
    }
    Collections.sort(submittedSurveys, new Comparator<SubmittedSurvey>() {
      @Override
      public int compare(SubmittedSurvey lhs, SubmittedSurvey rhs) {
        return rhs.getSchedule().getStart().compareTo(lhs.getSchedule().getStart());
      }
    });

    return submittedSurveys;
  }

  private SubmittedSurvey getLatestSubmission(List<SubmittedSurvey> submittedSurveys) {
    final Date now = new Date();
    for (SubmittedSurvey submittedSurvey : submittedSurveys) {
      final Schedule schedule = submittedSurvey.getSchedule();
      if(schedule.isActiveNow() || schedule.getStart().compareTo(now) <= 0) {
        return submittedSurvey;
      }
    }
    return null;
  }
}
