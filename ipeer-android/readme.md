# ipeer-android
Android Application for iPeer project. This application produces
 - Mentor Android Application
 - Veteran Android Application
 
## Installation
 - Download source code using `git clone https://bitbucket.org/kowsercse/ipeer.git`
 - Go to project directory `cd ipeer/ipeer-android`
 - Copy `.ipeer-android` to the `~/` directory
 - Edit deployment configuration file in the `~/.ipeer-android` directory  
    - `dev.properties` for development
    - `test.properties` for production
    - `prod.properties` for test
 - Cd to `ipeer/ipeer-android`
 - Create `ipeer-android/ipeer-lib/src/main/resources/dev.properties` using `./gradlew release` command

## Release Application

 - To release for environment:
    - **dev:** `./gradlew release -Penv=dev` or `./gradlew release`
    - **test:** `./gradlew release -Penv=test`
    - **prod:** `./gradlew release -Penv=prod`

## Run Application

 - Use Android Studio or Eclipse editor support to deploy application using a emulator or phone.
 - Download from server for for manual installation.
 